//
// Created by thejackimonster on 24.11.19.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <lge/audio/CaptureStream.hpp>
#include <lge/audio/Context.hpp>
#include <lge/audio/Device.hpp>
#include <lge/audio/StreamSource.hpp>
#include <lge/graphics/Device.hpp>
#include <lge/graphics/frame/Buffer.hpp>
#include <lge/graphics/ShaderProgram.hpp>
#include <lge/graphics/video/Texture2D.hpp>
#include <lge/sprite/SpriteBatch.hpp>
#include <lge/time/time.hxx>
#include <lge/window/Window.hpp>

int main(int argc, char** argv) {
	lge::Window window("lge", 1280, 720);
	
	window.makeContext();
	
	lge::GraphicsDevice device;
	lge::AudioDevice audioDevice;
	
	lge::AudioContext audioContext (audioDevice);
	
	if (audioContext.makeCurrent()) {
		audioContext.process();
	}
	
	if (device.init()) {
		lge::vector<lge::string> devices = lge::AudioCaptureDevice::listDevices();
		
		lge::string devName = (devices.size() > 3? devices[3] : devices[0]);
		
		std::cout << devName << std::endl;
		
		lge::AudioCaptureStream captureStream (devName, 48000, lge::AudioFormat::STEREO_16, 60);
		lge::AudioStreamSource streamSource;
		
		lge::ShaderProgram program0;
		
		program0.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/sprite.vert");
		program0.addShader(lge::ShaderType::GEOMETRY_SHADER, "resources/shaders/sprite.geom");
		program0.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/sprite.frag");
		
		lge::ShaderAttributes& attr0 = program0.getAttributes();
		
		attr0.bindLocation("Bounds", 0);
		attr0.bindLocation("TextureBounds", 1);
		attr0.bindLocation("Color", 2);
		attr0.bindLocation("Depth", 3);
		
		lge::ShaderFragments& frag0 = program0.getFragments();
		
		frag0.bindLocation("FragmentColor", 0);
		
		program0.link();
		
		lge::uniform<lge::mat3> u_transform = program0.getUniform<lge::mat3>("u_transform");
		lge::uniform<lge::TextureHandle> u_diffuse = program0.getUniform<lge::TextureHandle>("u_diffuse");
		lge::uniform_buffer_block<lge::vec2> u_screen_block = program0.getUniformBlock<lge::vec2>("u_screen");
		
		if (program0.validate()) {
			program0.use();
		}
		
		device.checkErrors(true);
		
		device.setParam(lge::DeviceParameter::CULL_FACE, true);
		device.setParam(lge::DeviceParameter::DEPTH_TEST, false);
		
		device.setCullFacing(false, true);
		
		lge::VideoTexture video;
		
		if (video.load("resources/assets/video.mp4")) {
			video.setWrapping(false, false);
			video.setMinFilter(true, false);
		}
		
		device.checkErrors(true);
		
		lge::uniform_buffer<lge::vec2> u_screen (lge::vec2(window.getWidth(), window.getHeight()));
		
		u_screen.bind(0);
		u_screen_block.connect(u_screen);
		
		lge::SpriteBatch sb;
		
		sb.setup(attr0);
		
		lge::DefaultFrameBuffer& fb = lge::FrameBuffer::getDefault();
		
		fb.attachTarget("FragmentColor", lge::FrameBufferTarget::BACK_LEFT);
		
		if (fb.checkStatus(false)) {
			fb.setup(frag0);
		}
		
		window.e_Resize += [&](int width, int height) {
			u_screen = lge::vec2(width, height);
			
			glViewport(0, 0, width, height);
		};
		
		window.e_Key += [&](int key, int scancode, int action, int mods) {
			if ((key == GLFW_KEY_F) && (action == GLFW_PRESS)) {
				window.setFullscreen(!window.isFullscreen());
			}
		};
		
		// These bind()-calls have to happen after creation of all textures (seems to mess up other methods) <- TODO
		video.bind(0);
		
		device.checkErrors(true);
		
		lge::Sprite s;
		
		s.setPosition(lge::vec2(-1, -1));
		s.setSize(lge::vec2(2, 2));
		s.setTextureBounds(lge::vec4(0, 0, 1, 1));
		s.setColor(lge::vec4(1, 1, 1, 1));
		s.setDepth(0.0f);
		
		sb.enableSprite(s);
		
		streamSource.getSource().setGain(0.0f);
		
		u_transform = lge::identity< 3 >();
		u_diffuse = video;
		
		program0.use();
		
		while (!window.shouldClose()) {
			float dt = lge::deltaTime();
			float ct = lge::currentTime();
			
			if (static_cast<int>(ct + dt) > static_cast<int>(ct)) {
				window.setTitle("FPS: " + std::to_string(static_cast<int>(1.0f / dt)));
			}
			
			captureStream.update(true);
			
			streamSource.push(captureStream);
			streamSource.update(true);
			
			video.update(dt);
			video.reload();
			
			fb.clearDepth(1.0f);
			fb.clearColor(lge::vec4(0, 0, 0, 1));
			
			fb.setup(frag0);
			fb.use();
			
			sb.updateSprite(s);
			
			sb.draw();
			
			window.swap();
			
			lge::Window::pollEvents();
		}
		
		sb.disableSprite(s);
		
		captureStream.update(false);
	}
	
	return 0;
}
