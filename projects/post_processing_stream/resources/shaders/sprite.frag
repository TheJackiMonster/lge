#version 430 core

in vec2 y_Relative;
in vec2 y_Size;
in vec2 y_Texture;
in vec4 y_Color;

uniform sampler2D u_diffuse;

layout (std140) uniform u_screen {
    float u_width;
    float u_height;
};

out vec4 FragmentColor;

vec2 nextCorner(vec2 position) {
    return vec2(position.x < 0? -1 : +1, position.y < 0? -1 : +1);
}

void main() {
    vec2 position = y_Relative * y_Size;
    vec2 relative_corner = nextCorner(position);

    float radius = 0.1f;

    vec2 ratio = vec2(u_width, u_height) / min(u_width, u_height);

    vec2 corner = relative_corner * y_Size - relative_corner * radius / ratio;

    vec2 distance = (corner - position) * ratio;
    vec2 direction = normalize(corner * ratio);
    vec2 value = distance * relative_corner;

    if ((value.x < -radius) || (value.y < -radius) ||
        ((value.x < 0) && (value.y < 0) &&
        (length(distance) > radius))) {
        discard;
    }

    FragmentColor = texture(u_diffuse, y_Texture) * y_Color;
}
