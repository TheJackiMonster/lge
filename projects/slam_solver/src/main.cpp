//
// Created by thejackimonster on 05.07.23.
//
// Copyright (c) 2023 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <climits>
#include <cmath>
#include <cstddef>
#include <lge/lang/primitives.hxx>
#include <lge/lang/string.hxx>
#include <lge/math/mat.hxx>
#include <lge/math/trigonometry.hxx>
#include <lge/math/util.hxx>
#include <lge/math/vec.hxx>
#include <lge/memory/vector.hxx>
#include <lge/system/filestream.hxx>
#include <lge/system/iostream.hxx>
#include <lge/system/stringstream.hxx>
#include <limits>
#include <string>

typedef double precision_t;
typedef lge::vec<3, 1, precision_t> vec3;
typedef lge::mat<3,3, 3, precision_t> mat3x3;
typedef lge::mat<3,6, 6, precision_t> mat3x6;

lge::vector<vec3> load_odometry(const char* path) {
    lge::ifstream file (path);
    lge::string line;

    lge::vector<vec3> odometry;

    while (std::getline(file, line)) {
        lge::stringstream ss (line);
        lge::string elem;

        vec3 entry;

        for (lge::uint i = 0; i < 3; i++) {
            if (std::getline(ss, elem, ',')) {
                entry[i] = std::stof(elem);
            }
        }

        odometry.push_back(entry);
    }

    return odometry;
}

vec3 applyTransform(const vec3& current, const vec3& delta) {
    // x_(n+1) = x_n + cos(z_n) * dx - sin(z_n) * dy
    // y_(n+1) = y_n + sin(z_n) * dx + cos(z_n) * dy
    // z_(n+1) = z_n + dz

    vec3 result;

    result[0] = (
        current[0] + 
        lge::cos<precision_t>(current[2]) * delta[0] - 
        lge::sin<precision_t>(current[2]) * delta[1]
    );

    result[0] = (
        current[0] + 
        lge::sin<precision_t>(current[2]) * delta[0] + 
        lge::cos<precision_t>(current[2]) * delta[1]
    );

    result[0] = (
        current[2] + 
        delta[2]
    );

    return result;
}

precision_t mod(precision_t x, precision_t y) {
    return x - (lge::vlong)(x/y) * y;
}

precision_t difference(const vec3& current, const vec3& next) {
    const vec3 dif (
        current[0] - next[0],
        current[1] - next[1],
        mod(current[2] - next[2], M_PI)
    );

    return (lge::flip(dif) * dif)(0, 0);
}

mat3x6 jakobiAt(const vec3& current, const vec3& delta) {
    // 0 = x_n + cos(z_n) * dx - sin(z_n) * dy - x_(n+1)
    // 0 = y_n + sin(z_n) * dx + cos(z_n) * dy - y_(n+1)
    // 0 = z_n + dz - z_(n+1)

    // x_n, y_n, z_n, x_(n+1), y_(n+1), z_(n+1)

    // 1, 0, -sin(z_n) * dx - cos(z_n) * dy, -1, 0, 0
    // 0, 1, cos(z_n) * dx - sin(z_n) * dy, 0, -1, 0
    // 0, 0, 1, 0, 0, -1

    mat3x6 result (
        1, 0, 0, -1,  0,  0,
        0, 1, 0,  0, -1,  0,
        0, 0, 1,  0,  0, -1
    );

    result(0, 2) = (
        -lge::sin<precision_t>(current[2]) * delta[0] - 
        lge::cos<precision_t>(current[2]) * delta[1]
    );

    result(1, 2) = (
        lge::cos<precision_t>(current[2]) * delta[0] + 
        -lge::sin<precision_t>(current[2]) * delta[1]
    );

    return result;
}

typedef lge::vec<2, 1, uint> match_t;

struct model_t {
    mat3x3 cov;
};

struct factor_t {
    uint nodes [2];
    const vec3* odometry;
    const model_t* model;
};

lge::vector<vec3> optimize(const lge::vector<factor_t>& factors, const lge::vector<vec3>& current, precision_t alpha) {
    lge::vector<vec3> next;
    next.reserve(current.size());

    for (const auto& c : current) {
        next.push_back(c);
    }

    for (const auto& f : factors) {
        vec3 x (0, 0, 0);
        vec3 y (0, 0, 0);

        if (f.nodes[0] < current.size()) {
            x = current[ f.nodes[0] ];
        }

        if (f.nodes[1] < current.size()) {
            y = current[ f.nodes[1] ];
        }

        mat3x3 cov = lge::identity<3>();

        if (f.model != nullptr) {
            cov = f.model->cov;
        }

        const vec3& odom = *(f.odometry);
        const mat3x6 J = jakobiAt(x, odom);
        const mat3x3 info = lge::inverse(cov);
        const auto dx = -alpha * lge::transpose(J) * info * (x - y);

        if (f.nodes[0] < next.size()) {
            next[ f.nodes[0] ] += dx.template column<3>(0, 0);
        }

        if (f.nodes[1] < next.size()) {
            next[ f.nodes[1] ] += dx.template column<3>(0, 3);
        }
    }

    return next;
}

precision_t get_error(const lge::vector<vec3>& current, const lge::vector<vec3>& next) {
    if (current.size() != next.size()) {
        lge::cerr << "Mismatch: " << current.size() << " != " << next.size() << lge::endl;
        return std::numeric_limits<precision_t>::infinity();
    }

    precision_t result = 0;
    for (uint i = 0; i < current.size(); i++) {
        result += difference(current[i], next[i]);
    }

    return result;
}

int main(int argc, const char** argv) {
    const auto odometry_gt = load_odometry("odometry_gt.txt");
    const auto odometry = load_odometry("odometry_noisy_underrotated.txt");

    model_t noise_model_prior;
    noise_model_prior.cov = mat3x3(
        0.1f, 0.0f, 0.0f,
        0.0f, 0.1f, 0.0f,
        0.0f, 0.0f, 0.01f
    );

    model_t noise_model_odo;
    noise_model_odo.cov = mat3x3(
        5.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.02f
    );

    model_t noise_model_lc;
    noise_model_lc.cov = mat3x3(
        10.0f, 0.0f, 0.0f,
        0.0f, 2.0f, 0.0f,
        0.0f, 0.0f, 0.04f
    );

    lge::vector<vec3> groundTruth;
    groundTruth.reserve(odometry_gt.size());

    vec3 current (0, 0, 0);
    for (uint i = 0; i < odometry_gt.size(); i++) {
        const auto& odom = odometry_gt[i];
        current = applyTransform(current, odom);

        groundTruth.push_back(current);
    }

    lge::vector<vec3> initialEstimate;
    initialEstimate.reserve(odometry.size());

    lge::vector<factor_t> factors;
    factors.reserve(odometry.size());

    current = vec3(0, 0, 0);
    for (uint i = 0; i < odometry.size(); i++) {
        const auto& odom = odometry[i];
        current = applyTransform(current, odom);

        factor_t factor;
        factor.nodes[0] = i > 0? (i - 1) : UINT_MAX;
        factor.nodes[1] = i;
        factor.odometry = &odom;
        factor.model = i > 0? &noise_model_odo : &noise_model_prior;

        initialEstimate.push_back(current);
        factors.push_back(factor);
    }

    vec3 lc_odom (0, 0, 0);
    lge::vector<match_t> matches;
    matches.emplace_back(0, 62);

    for (const auto& match : matches) {
        factor_t factor;
        factor.nodes[0] = match[0];
        factor.nodes[1] = match[1];
        factor.odometry = &lc_odom;
        factor.model = &noise_model_lc;
        factors.push_back(factor);
    }

    const uint max_iterations = 100;
    const precision_t min_error_change = 1e-5;

    precision_t error = get_error(initialEstimate, groundTruth);
    lge::vector<vec3> nodes = initialEstimate;
    for (uint i = 0; i < max_iterations; i++) {
        const precision_t current_error = error;

        nodes = optimize(factors, nodes, 0.001);
        error = get_error(nodes, groundTruth);

        const auto d_error = lge::abs<precision_t>(current_error - error);

        lge::cout << "STEP [ " << i << " ]: " << error << lge::endl;

        const precision_t d = difference(nodes[0], nodes[62]);

        lge::cout << "-> D: " << d << lge::endl;

        if (d_error < min_error_change) {
            break;
        }
    }

    return 0;
}
