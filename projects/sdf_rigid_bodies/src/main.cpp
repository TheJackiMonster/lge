//
// Created by thejackimonster on 29.11.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <lge/camera/Camera.hpp>
#include <lge/graphics/Device.hpp>
#include <lge/graphics/ShaderProgram.hpp>
#include <lge/graphics/Texture3D.hpp>
#include <lge/model/Model.hpp>
#include <lge/physics/PhysicEngine.hpp>
#include <lge/window/Window.hpp>

#include <lge/time/time.hxx>

#include <lge/graphics/frame/Buffer.hpp>
#include <lge/graphics/glsl.hxx>

struct t_transform {
	lge::std140::mat4_T m_view;
	lge::std140::mat4_T m_projection;
};

int main(int argc, char** argv) {
	lge::Window window("lge", 1280, 720);
	
	window.makeContext();
	window.setSwapInterval(0);

	lge::GraphicsDevice device;
	
	if (device.init()) {
		lge::PhysicEngine engine;
		
		lge::ShaderProgram program0;
		
		program0.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/sdf.vert");
		program0.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/sdf.frag");
		
		lge::ShaderAttributes& attr0 = program0.getAttributes();
		
		attr0.bindLocation("Position", 0);
		
		lge::ShaderFragments& frag0 = program0.getFragments();
		
		frag0.bindLocation("FragmentColor", 0);
		
		program0.link();
		
		lge::uniform< lge::uint > u_bounds_index0 = program0.getUniform< lge::uint >("u_bounds_index");
		lge::uniform< lge::Texture3DHandle > u_sdf0 = program0.getUniform< lge::Texture3DHandle >("u_sdf");
		
		lge::uniform_buffer_block< t_transform > u_transform_block0 = program0.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block0 = program0.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program0.validate()) {
			program0.use();
		}
		
		lge::ShaderProgram program1;
		
		program1.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/bounds.vert");
		program1.addShader(lge::ShaderType::GEOMETRY_SHADER, "resources/shaders/bounds.geom");
		program1.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/minimal.frag");
		
		lge::ShaderAttributes& attr1 = program1.getAttributes();
		lge::ShaderFragments& frag1 = program1.getFragments();
		
		frag1.bindLocation("FragmentColor", 0);
		
		program1.link();
		
		lge::uniform_buffer_block< t_transform > u_transform_block1 = program1.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block1 = program1.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program1.validate()) {
			program1.use();
		}
		
		lge::ShaderProgram program2;
		
		program2.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/bounds.vert");
		program2.addShader(lge::ShaderType::GEOMETRY_SHADER, "resources/shaders/motion.geom");
		program2.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/minimal.frag");
		
		lge::ShaderAttributes& attr2 = program2.getAttributes();
		lge::ShaderFragments& frag2 = program2.getFragments();
		
		frag2.bindLocation("FragmentColor", 0);
		
		program2.link();
		
		lge::uniform_buffer_block< t_transform > u_transform_block2 = program2.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block2 = program2.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program2.validate()) {
			program2.use();
		}
		
		lge::ShaderProgram program3;
		
		program3.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/draw_sdf.vert");
		program3.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/draw_sdf.frag");
		
		lge::ShaderAttributes& attr3 = program3.getAttributes();
		lge::ShaderFragments& frag3 = program3.getFragments();
		
		frag3.bindLocation("FragmentColor", 0);
		
		program3.link();
		
		lge::uniform< lge::uint > u_bounds_index3 = program3.getUniform< lge::uint >("u_bounds_index");
		lge::uniform< lge::Texture3DHandle > u_sdf3 = program3.getUniform< lge::Texture3DHandle >("u_sdf");
		
		lge::uniform_buffer_block< t_transform > u_transform_block3 = program3.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block3 = program3.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program3.validate()) {
			program3.use();
		}
		
		lge::ShaderProgram program4;
		
		program4.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/draw_distance.vert");
		program4.addShader(lge::ShaderType::GEOMETRY_SHADER, "resources/shaders/draw_distance.geom");
		program4.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/draw_distance.frag");
		
		lge::ShaderAttributes& attr4 = program4.getAttributes();
		lge::ShaderFragments& frag4 = program4.getFragments();
		
		frag4.bindLocation("FragmentColor", 0);
		
		program4.link();
		
		lge::uniform< lge::uint > u_bounds_index4 = program4.getUniform< lge::uint >("u_bounds_index");
		lge::uniform< lge::uint > u_other_index4 = program4.getUniform< lge::uint >("u_other_index");
		lge::uniform< lge::Texture3DHandle > u_sdf4 = program4.getUniform< lge::Texture3DHandle >("u_sdf");
		
		lge::uniform_buffer_block< t_transform > u_transform_block4 = program4.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block4 = program4.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program4.validate()) {
			program4.use();
		}
		
		lge::ShaderProgram program7;
		
		program7.addShader(lge::ShaderType::VERTEX_SHADER, "resources/shaders/minimal.vert");
		program7.addShader(lge::ShaderType::FRAGMENT_SHADER, "resources/shaders/minimal.frag");
		
		lge::ShaderAttributes& attr7 = program7.getAttributes();
		
		attr7.bindLocation("Position", 0);
		
		lge::ShaderFragments& frag7 = program7.getFragments();
		
		frag7.bindLocation("FragmentColor", 0);
		
		program7.link();
		
		lge::uniform< lge::uint > u_bounds_index7 = program7.getUniform< lge::uint >("u_bounds_index");
		
		lge::uniform_buffer_block< t_transform > u_transform_block7 = program7.getUniformBlock< t_transform >(
				"u_transform_block");
		lge::shader_storage_buffer_block< lge::t_bounds > u_bounds_block7 = program7.getStorageBlock< lge::t_bounds >(
				"u_bounds_block");
		
		if (program7.validate()) {
			program7.use();
		}
		
		device.checkErrors(true);
		
		device.setParam(lge::DeviceParameter::BLEND, true);
		device.setParam(lge::DeviceParameter::CULL_FACE, true);
		device.setParam(lge::DeviceParameter::DEPTH_TEST, true);
		device.setParam(lge::DeviceParameter::MULTISAMPLE, true);
		
		device.setCullFacing(false, true);
		device.setDefaultBlending();
		
		lge::vec3 pos = lge::vec3(0, -2, -10);
		
		lge::Camera camera;
		
		// Camera::{ position: [ -9.19822 -22.5737 -9.8909 ], direction: [ 0.99411 -0.108299 -0.00403786 ] }
		
		camera.look(lge::vec3(-9.2, -22.6, -9.9), lge::vec3(0.99, -0.11, -0.00));
		
		lge::mat4 P = lge::perspective(45.0f, 16.0f / 9, 0.1f, 10000.0f);
		
		lge::uniform_buffer< t_transform > u_transform;
		
		t_transform transform;

		transform.m_view.mat = lge::transpose(camera.getView());
		transform.m_projection.mat = lge::transpose(P);
		
		u_transform = transform;
		
		u_transform_block0.connect(u_transform);
		u_transform_block1.connect(u_transform);
		u_transform_block2.connect(u_transform);
		u_transform_block3.connect(u_transform);
		u_transform_block4.connect(u_transform);
		u_transform_block7.connect(u_transform);

        device.checkErrors(true);
		
        auto& bounds = engine.getBounds();
        
		u_bounds_block0.connect(bounds);
		u_bounds_block1.connect(bounds);
		u_bounds_block2.connect(bounds);
		u_bounds_block3.connect(bounds);
		u_bounds_block4.connect(bounds);
		u_bounds_block7.connect(bounds);

        device.checkErrors(true);
        
        lge::SignedDistanceField sdf0;
		lge::SignedDistanceField sdf1;
		lge::SignedDistanceField sdf2;
		
		lge::Model model0;
		lge::Model model1;
		lge::Model model2;
		
		if (model0.load("resources/models/sphere.fbx")) {
			model0.setup(attr0);
		}
		
		if (model1.load("resources/models/Armadillo_flat.fbx")) {
			model1.setup(attr0);
		}
		
		if (model2.load("resources/models/cube.fbx")) {
			model2.setup(attr0);
		}
		
		if (!sdf0.load("model0.sdf")) {
			if (sdf0.setup(12)) {
				sdf0.generate(device, model0);
				sdf0.save("model0.sdf");
			}
		} else {
			sdf0.setModel(model0);
		}
		
		if (!sdf1.load("model1.sdf")) {
			if (sdf1.setup(8)) {
				sdf1.generate(device, model1);
				sdf1.save("model1.sdf");
			}
		} else {
			sdf1.setModel(model1);
		}
		
		if (!sdf2.load("model2.sdf")) {
			if (sdf2.setup(12)) {
				sdf2.generate(device, model2);
				sdf2.save("model2.sdf");
			}
		} else {
			sdf2.setModel(model2);
		}
		
		u_sdf0 = sdf0.getTexture();
		u_sdf3 = sdf0.getTexture();
		u_sdf4 = sdf1.getTexture();
		
		device.checkErrors(true);
		
		lge::DefaultFrameBuffer& fb = lge::FrameBuffer::getDefault();
		
		fb.attachTarget("FragmentColor", lge::FrameBufferTarget::BACK_LEFT);
		
		if (fb.checkStatus(false)) {
			fb.setup(frag0);
		}
		
		window.e_Resize += [&](int width, int height) {
			glViewport(0, 0, width, height);
		};
		
		sdf0.generateInertia(device);
		sdf1.generateInertia(device);
		sdf2.generateInertia(device);
		
		lge::PrecisionMode precisionMode = lge::PrecisionMode::REALTIME;
		
		bool active = false;
		
		if (argc > 1) {
			int cn = std::stoi(std::string(argv[1]));
			
			while (cn > 0) {
				precisionMode = lge::PrecisionMode_::next(precisionMode);
				cn--;
			}
			
			active = true;
		}

		bool KEY_W = false;
        bool KEY_A = false;
        bool KEY_S = false;
        bool KEY_D = false;
        
        uint DEBUG = 0;
        bool SHIFT = false;
        bool DROP = false;
        
        uint targetId = 0;
		uint bodies_count = 1;
  
		window.e_Key += [&](int key, int scancode, int action, int mods) {
			if ((key == GLFW_KEY_H) && (action == GLFW_PRESS)) {
				active = !active;
				
				lge::cout << "Physics-State: " << (active? "PLAYING" : "PAUSED") << lge::endl;
			}
			
			if ((key == GLFW_KEY_J) && (action == GLFW_PRESS)) {
				precisionMode = lge::PrecisionMode_::next(precisionMode);
				
				lge::cout << "Precision-Mode: " << lge::PrecisionMode_::getName(precisionMode) << " (ACTIVE)" << lge::endl;
			}
			
			if ((key == GLFW_KEY_G) && (action == GLFW_PRESS)) {
				DEBUG = (DEBUG + 1) % 4;
				
				lge::cout << "Debug-State: " << DEBUG << lge::endl;
			}
			
			if (key == GLFW_KEY_LEFT_SHIFT) {
				SHIFT = (action != GLFW_RELEASE);
			}

			if (key == GLFW_KEY_W) {
                KEY_W = (action != GLFW_RELEASE);
			}

            if (key == GLFW_KEY_A) {
                KEY_A = (action != GLFW_RELEASE);
            }

            if (key == GLFW_KEY_S) {
                KEY_S = (action != GLFW_RELEASE);
            }

            if (key == GLFW_KEY_D) {
                KEY_D = (action != GLFW_RELEASE);
            }
            
            if ((key == GLFW_KEY_UP) && (action == GLFW_PRESS)) {
				targetId = (targetId + 1) % bodies_count;
            }
			
			if ((key == GLFW_KEY_DOWN) && (action == GLFW_PRESS)) {
				targetId = (targetId + bodies_count - 1) % bodies_count;
			}
			
			if ((key == GLFW_KEY_Q) && (action == GLFW_PRESS)) {
				DROP = true;
			}
		};

		bool DRAG = false;
		bool DRAG_START = false;

		window.e_Mouse += [&](int button, int action, int mods) {
            if (button == 0) {
                DRAG = (action != GLFW_RELEASE);
                DRAG_START = (action == GLFW_PRESS);
            }
		};

        lge::dvec2 drag_stop (0);

        window.e_MouseMove += [&](double x, double y) {
            auto pos = lge::dvec2(x, y);

            if (DRAG_START) {
                drag_stop = pos;
                DRAG_START = false;
            }

            if (DRAG) {
                auto delta = (drag_stop - pos);
	
				camera.turn(
						static_cast<float>(delta[1]) * 0.01f,
						static_cast<float>(delta[0]) * 0.01f
				);
				
                drag_stop = pos;
            }
        };
		
		device.checkErrors(true);

		lge::VertexArray vao;
		
		vao.setup(attr1);
		
		float t = 0.0f;
		
		engine.setGravity(0.981f);

		// Velocity limit: (lge::numeric_limits<float>::max() / 8)
		
		lge::RigidBody& world = engine.createRigidBody(sdf2);
		
		world.setOrientation(lge::rotation(0.0f, lge::vec3(0, 0, 1)));
		world.bounds().moveTo(pos + lge::vec3(8, -45, 2));
		world.bounds().resizeBy(80.0f);
		
		//world.setAngularFrequency(lge::vec3(0, 0.1f, 0));
		
		float inf = lge::numeric_limits< float >::infinity();
		
		world.setMass(inf);
		world.setInertiaAutomatic();
		
		world.material().setRoughness(1.0f);
		
		lge::vec3 worldPos = world.bounds().getCenter();
		
		float sphereF = 2.0f / 5.0f;
		float cubeF = 1.0f / 6.0f;
		
		/*lge::RigidBody& body = engine.createRigidBody(sdf0);
		
		//body->setVelocity(lge::vec3(1.1f, 0, 0));
		body.setAngularFrequency(lge::vec3(0, 0, 1.2f));
		
		body.material().setBounciness(0.6f);
		body.material().setRoughness(0.8f);
		body.bounds().moveTo(pos + lge::vec3(0, 0, 0));
		body.bounds().resizeBy(2.0f);
		
		float bodyValue = body.bounds().getSize();
		
		body.setMass(2.0f);
		body.setInertiaAutomatic();*/
		
		const int amount = 54;
		
		for (int i = 0; i < amount; i++) {
			lge::RigidBody& sphere = engine.createRigidBody(sdf1);
			
			
			float rad = lge::radians(360.0f * i / amount);
			
			sphere.bounds().moveTo(pos + lge::vec3(lge::sin< float >(rad) * amount / 3, 10, lge::cos< float >(rad) * amount / 3));
			
			
			//sphere.bounds().moveTo(pos + lge::vec3(0, i - 18, 0));
			sphere.setVelocity(lge::vec3(0, -1, 0));
			
			sphere.material().setRoughness(0.1f);
			sphere.material().setBounciness(i * 1.0f / (amount - 1));
		}
		
		lge::cout << "Physics-State: " << (active? "PLAYING" : "PAUSED") << lge::endl;
		lge::cout << "Precision-Mode: " << lge::PrecisionMode_::getName(precisionMode) << " (ACTIVE)" << lge::endl;
		lge::cout << "Debug-State: " << DEBUG << lge::endl;
		
		float gt = 0.0f;
		float bt = 0.0f;
		
		int min_fps = 1000;
		int max_fps = 0;
		
		while (!window.shouldClose()) {
			float dt = lge::deltaTime();
			float ct = lge::currentTime();

			if (static_cast<int>(ct + dt) > static_cast<int>(ct)) {
                window.setTitle("FPS: " + std::to_string(static_cast<int>(1.0f / dt)));
			}
			
			const float camMovementSpace = (SHIFT? 15.0f : 1.5f) * dt; // velocity * time
			
			if (!active) {
				dt = 0.0f;
			}
			
			t += dt;
			
			lge::vec3 camMovement (0);
			
			if (KEY_W) {
				camMovement += camera.getDirection() * camMovementSpace;
			}
			
			if (KEY_A) {
				camMovement -= camera.getSide() * camMovementSpace;
			}
			
			if (KEY_S) {
				camMovement -= camera.getDirection() * camMovementSpace;
			}
			
			if (KEY_D) {
				camMovement += camera.getSide() * camMovementSpace;
			}
			
			camera.setPosition(camera.getPosition() + camMovement);
			
			transform.m_view.mat = lge::transpose(camera.getView());
			transform.m_projection.mat = lge::transpose(P);
			
			if (DROP) {
				lge::RigidBody& sphere = engine.createRigidBody(sdf1);
				
				lge::cout << reinterpret_cast<size_t>(&sphere) << lge::endl;
				
				sphere.bounds().moveTo(camera.getPosition() + camera.getDirection() * 2);
				sphere.setVelocity(lge::vec3(0, -1, 0));
				
				sphere.material().setRoughness(0.2f);
				
				DROP = false;
			}
			
			fb.clearDepth(1.0f);
			fb.clearColor(lge::vec4(1, 1, 1, 1));
			
			device.memoryBarrier(GL_FRAMEBUFFER_BARRIER_BIT);
			
			bool flag_out = false;
			
			if (dt > 0) {
				lge::cout << "Time: " << gt << "; Frametime: " << bt << "; ";
				flag_out = true;
			}
			
			bt += dt;
			
			dt = engine.update(device, dt, precisionMode);
			
			gt += dt;
			
			if (flag_out) {
				int fps = (int) (1.0f / dt);
				
				if (fps < min_fps) {
					min_fps = fps;
				}
				
				if (fps > max_fps) {
					max_fps = fps;
				}
				
				lge::cout << "Deltatime: " << dt << "; ";
				lge::cout << "FPS: " << min_fps << "/" << fps << "/" << max_fps << ";" << lge::endl;
			}
			
			device.checkErrors(true);

            u_transform = transform;
			
			switch (DEBUG) {
				case 1:
					fb.setup(frag7);
					break;
				case 2:
					fb.setup(frag3);
					break;
				case 3:
					fb.setup(frag4);
					break;
				default:
					fb.setup(frag0);
					break;
			}
			
			fb.use();
			
			device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_UNIFORM_BARRIER_BIT);
			
			switch (DEBUG) {
				case 1:
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					
					program7.use();
					break;
				case 2:
					glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
					
					program3.use();
					break;
				case 3:
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					
					program4.use();
					break;
				default:
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					
					program0.use();
					break;
			}
			
			lge::vector< lge::RigidBody >& bodies = engine.getRigidBodies();
			
			bodies_count = bodies.size();

            for (lge::uint i = 0; i < bodies.size(); i++) {
                const lge::Model* model = bodies[i].getSDF().getModel();
	
				switch (DEBUG) {
					case 1: {
						u_bounds_index7 = i;
						
						if (model != nullptr) {
							model->draw();
						}
						
						break;
					}
					case 2: {
						u_bounds_index3 = i;
						
						const auto& texture = bodies[i].getSDF().getTexture();
						
						u_sdf3 = texture;
						
						vao.draw(lge::DrawType::POINTS, (
								texture.getWidth() *
								texture.getHeight() *
								texture.getDepth()
						), 0);
						
						break;
					}
					case 3: {
						u_bounds_index4 = i;
						u_other_index4 = targetId;
						
						u_sdf4 = bodies[targetId].getSDF().getTexture();
						
						if (model != nullptr) {
							model->draw();
						}
						
						break;
					}
					default: {
						u_bounds_index0 = i;
						
						u_sdf0 = bodies[i].getSDF().getTexture();
						
						if (model != nullptr) {
							model->draw();
						}
						
						break;
					}
				}
            }
			
			if ((DEBUG >= 1) && (DEBUG <= 3)) {
				fb.setup(frag1);
				fb.use();
				
				device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_UNIFORM_BARRIER_BIT);
				
				program1.use();
				
				vao.draw(lge::DrawType::POINTS, bodies.size(), 0);
				
				fb.setup(frag2);
				fb.use();
				
				device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_UNIFORM_BARRIER_BIT);
				
				program2.use();
				
				vao.draw(lge::DrawType::POINTS, bodies.size(), 0);
			}
			
			window.swap();
			
			lge::Window::pollEvents();
		}
	}
	
	return 0;
}
