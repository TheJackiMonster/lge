#version 430 core

in vec3 Position;

uniform uint u_bounds_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

flat out int y_BoundsIndex;

void main() {
    mat3 R = u_bounds[u_bounds_index].m_orientation;

    vec3 pos = R * Position;

    vec3 p = u_bounds[u_bounds_index].m_cube.xyz;
    float s = u_bounds[u_bounds_index].m_cube.w;

    pos = pos * s + p;

    y_BoundsIndex = int(u_bounds_index);

    gl_Position = u_projection * u_view * vec4(pos, 1);
}
