#version 430 core

layout(triangles) in;
layout(line_strip, max_vertices=2) out;

in vec3 x_Position[];

uniform uint u_other_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

uniform sampler3D u_sdf;

out float y_Distance;

float distance_from(vec3 position) {
    vec3 pos = (position - 0.5f);

    float outer_distance = length(max(abs(pos) - 0.5f, vec3(0)));
    float inner_distance = texture(u_sdf, position).r;

    return (outer_distance + inner_distance);
}

vec4 get_from_sdf(vec3 position) {
    /*
     * Using 1-voxel layer around the sdf!
     */
    ivec3 size = textureSize(u_sdf, 0);
    vec3 offset = 1.0f / size;

    vec3 pos = offset + (position + 0.5f) * (1.0f - 2.0f * offset);

    vec3 ox = vec3(offset.x, 0, 0);
    vec3 oy = vec3(0, offset.y, 0);
    vec3 oz = vec3(0, 0, offset.z);

    return vec4(normalize(vec3(
        distance_from(pos + ox) - distance_from(pos - ox),
        distance_from(pos + oy) - distance_from(pos - oy),
        distance_from(pos + oz) - distance_from(pos - oz)
    )), distance_from(pos));
}

bool is_valid(vec3 v) {
    return !(any(isnan(v)) || any(isinf(v)));
}

void main() {
    vec3 pos = (
        x_Position[0] +
        x_Position[1] +
        x_Position[2]
    ) / 3;

    t_bounds other = u_bounds[u_other_index];

    mat3 R = other.m_orientation;
    vec3 p = other.m_cube.xyz;
    float s = other.m_cube.w;

    vec3 abs_pos0 = pos;
    vec3 rel_pos1 = pos - p;

    pos = transpose(R) * rel_pos1 / s;

    vec4 sdf_value = get_from_sdf(pos);

    vec3 normal = sdf_value.xyz;
    float distance = sdf_value.w * other.m_cube.w;

    if (is_valid(normal)) {
        normal = R * normal;

        rel_pos1 -= (distance * normal);

        vec3 abs_pos1 = rel_pos1 + p;

        y_Distance = distance;

        gl_Position = u_projection * u_view * vec4(abs_pos0, 1);

        EmitVertex();

        y_Distance = 0.0f;

        gl_Position = u_projection * u_view * vec4(abs_pos1, 1);

        EmitVertex();

        EndPrimitive();
    }
}
