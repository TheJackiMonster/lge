#version 430 core

in vec3 x_Position;

uniform sampler3D u_sdf;

out vec4 FragmentColor;

void main() {
    float distance = texture(u_sdf, x_Position).r;
    float value = max(0.5 - distance, 0);

    if (value <= 0) {
        discard;
    }

    FragmentColor = vec4(vec3(
        - min(distance, 0),
        abs(distance),
        0.5 * sign(distance)
    ), distance < 0? 1 : value * value);
}
