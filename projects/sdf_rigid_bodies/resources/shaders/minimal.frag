#version 430 core

flat in int y_BoundsIndex;

out vec4 FragmentColor;

void main() {
    FragmentColor = vec4(1, 1, 1, 1);

    switch (y_BoundsIndex % 6) {
        case 0: {
            FragmentColor = vec4(0, 0, 1, 1);
            break;
        }

        case 1: {
            FragmentColor = vec4(0, 1, 1, 1);
            break;
        }

        case 2: {
            FragmentColor = vec4(0, 1, 0, 1);
            break;
        }

        case 3: {
            FragmentColor = vec4(1, 1, 0, 1);
            break;
        }

        case 4: {
            FragmentColor = vec4(1, 0, 0, 1);
            break;
        }

        case 5: {
            FragmentColor = vec4(1, 0, 1, 1);
            break;
        }

        default: {
            FragmentColor = vec4(1, 1, 1, 1);
            break;
        }
    }
}