#version 430 core

in vec3 Position;

uniform uint u_bounds_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

out vec3 x_Position;

void main() {
    t_bounds bounds = u_bounds[u_bounds_index];

    mat3 R = bounds.m_orientation;
    vec3 p = bounds.m_cube.xyz;
    float s = bounds.m_cube.w;

    vec3 pos = R * Position * s + p;

    x_Position = pos;

    gl_Position = u_projection * u_view * vec4(pos, 1);
}
