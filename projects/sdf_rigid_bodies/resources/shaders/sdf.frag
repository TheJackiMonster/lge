#version 430 core

in vec3 x_Position;
in vec3 x_Normal;
in vec3 x_Color;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

out vec4 FragmentColor;

void main() {
    vec3 n = normalize(x_Normal);
    vec3 vn = normalize((u_view * vec4(n, 0)).xyz);

    float s = max(dot(vn, x_Position), 0) * 0.8;
    float spec = pow(s, 8) * 0.25;

    float alpha = 1 - vn.z * vn.z + spec / 4;

    FragmentColor = vec4((s + 0.2) * x_Color + vec3(spec), 1.0);
}
