#version 430 core

in vec3 Position;
in vec3 Normal;

uniform uint u_bounds_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

uniform sampler3D u_sdf;

out vec3 x_Position;
out vec3 x_Normal;
out vec3 x_Color;

vec3 relativePosition() {
    /*
     * Using 1-voxel layer around the sdf!
     */
    ivec3 size = textureSize(u_sdf, 0);
    vec3 offset = 1.0f / size;

    return offset + (Position + 0.5f) * (1.0f - 2.0f * offset);
}

void main() {
    mat3 R = u_bounds[u_bounds_index].m_orientation;

    vec3 pos = R * Position;

    vec3 p = u_bounds[u_bounds_index].m_cube.xyz;
    float s = u_bounds[u_bounds_index].m_cube.w;

    int color_id = (u_bounds_index > 0? int(u_bounds_index) % 6 : -1);

    switch (color_id) {
        case 0: {
            x_Color = vec3(0, 0, 1);
            break;
        }

        case 1: {
            x_Color = vec3(0, 1, 1);
            break;
        }

        case 2: {
            x_Color = vec3(0, 1, 0);
            break;
        }

        case 3: {
            x_Color = vec3(1, 1, 0);
            break;
        }

        case 4: {
            x_Color = vec3(1, 0, 0);
            break;
        }

        case 5: {
            x_Color = vec3(1, 0, 1);
            break;
        }

        default: {
            x_Color = vec3(0.7, 0.75, 0.8);
            break;
        }
    }

    x_Normal = R * Normal;

    pos = pos * s + p;

    x_Position = relativePosition();

    gl_Position = u_projection * u_view * vec4(pos, 1);
}
