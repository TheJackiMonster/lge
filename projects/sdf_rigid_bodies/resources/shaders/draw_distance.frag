#version 430 core

in float y_Distance;

uniform uint u_bounds_index;
uniform uint u_other_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

out vec4 FragmentColor;

void main() {
    if (u_bounds_index == u_other_index) {
        FragmentColor = vec4(1, 1, 1, 1);
    } else {
        t_bounds other = u_bounds[u_other_index];

        float rel_distance = y_Distance / other.m_cube.w;

        FragmentColor = vec4(1 - rel_distance, rel_distance, 0, 0.1);
    }
}