#version 430

layout(points) in;
layout(line_strip, max_vertices=20) out;

in int x_BoundsIndex[];

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

flat out int y_BoundsIndex;

void vertex(int index, mat4 MVP, vec3 position) {
    y_BoundsIndex = index;
    gl_Position = MVP * vec4(position, 1);
    EmitVertex();
}

void arrow(int index, mat4 MVP, vec3 position, vec3 direction) {
    vec3 n = normalize(direction);

    vec3 off_x = cross(normalize(position), n);
    vec3 off_y = cross(normalize(off_x), n);

    off_x = cross(off_y, n);

    off_x *= length(direction) * 0.02;
    off_y *= length(direction) * 0.02;

    vec3 p0 = position + direction;
    vec3 p1 = position + direction * 0.9 + off_y;
    vec3 p2 = position + direction * 0.9 + 0.866 * off_x - 0.5 * off_y;
    vec3 p3 = position + direction * 0.9 - 0.866 * off_x - 0.5 * off_y;

    vertex(index, MVP, p1);
    vertex(index, MVP, p2);
    vertex(index, MVP, p3);
    vertex(index, MVP, p1);

    EndPrimitive();

    vertex(index, MVP, p1);
    vertex(index, MVP, p0);
    vertex(index, MVP, p2);

    EndPrimitive();

    vertex(index, MVP, p3);
    vertex(index, MVP, p0);
    vertex(index, MVP, position);

    EndPrimitive();
}

void main() {
    int index = int(x_BoundsIndex[0]);

    vec3 p = u_bounds[index].m_cube.xyz;
    float s = u_bounds[index].m_cube.w;

    mat4 MVP = u_projection * u_view;

    arrow(2, MVP, p + normalize(u_bounds[index].m_velocity) * s / 2, u_bounds[index].m_velocity);
    arrow(4, MVP, p + normalize(u_bounds[index].m_angular_frequency) * s / 2, u_bounds[index].m_angular_frequency * s / 2);
}
