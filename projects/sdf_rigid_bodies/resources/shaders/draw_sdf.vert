#version 430 core

uniform uint u_bounds_index;

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

uniform sampler3D u_sdf;

out vec3 x_Position;

void main() {
    int id = gl_VertexID;

    ivec3 size = textureSize(u_sdf, 0);

    ivec3 index = ivec3(
        id % size.x,
        (id / size.x) % size.y,
        (id / size.x / size.y) % size.z
    );

    vec3 offset = 1.0f / size;

    vec3 p = u_bounds[u_bounds_index].m_cube.xyz;
    float s = u_bounds[u_bounds_index].m_cube.w;

    vec3 pos = vec3(0.5 + index) * offset;

    x_Position = pos;

    mat3 R = u_bounds[u_bounds_index].m_orientation;

    pos = (R * (pos - 0.5)) * s / (1 - 2 * offset) + p;

    gl_Position = u_projection * u_view * vec4(pos, 1);
}
