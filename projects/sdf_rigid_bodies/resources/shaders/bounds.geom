#version 430 core

layout(points) in;
layout(line_strip, max_vertices=24) out;

in int x_BoundsIndex[];

layout (std140) uniform u_transform_block {
    mat4 u_view;
    mat4 u_projection;
};

struct t_bounds {
    float m_mass;
    float m_bounciness;
    float m_roughness;
    vec3 m_velocity;
    vec3 m_angular_frequency;
    vec4 m_cube;
    mat3 m_orientation;
    mat3 m_inertia;
};

layout (std430) restrict readonly buffer u_bounds_block {
    t_bounds u_bounds [];
};

flat out int y_BoundsIndex;

void corner(int index, mat4 MVP, bvec3 a) {
    y_BoundsIndex = index;

    gl_Position = MVP * vec4(mix(vec3(-0.5f), vec3(+0.5f), a), 1);

    EmitVertex();
}

void main() {
    int index = int(x_BoundsIndex[0]);

    vec3 p = u_bounds[index].m_cube.xyz;
    float s = u_bounds[index].m_cube.w;

    mat4 M = mat4(
        s, 0, 0, 0,
        0, s, 0, 0,
        0, 0, s, 0,
        p.x, p.y, p.z, 1
    );

    mat4 MVP = u_projection * u_view * M;

    corner(index, MVP, bvec3(false, false, false));
    corner(index, MVP, bvec3(true, false, false));
    EndPrimitive();

    corner(index, MVP, bvec3(false, false, false));
    corner(index, MVP, bvec3(false, true, false));
    EndPrimitive();

    corner(index, MVP, bvec3(false, false, false));
    corner(index, MVP, bvec3(false, false, true));
    EndPrimitive();

    corner(index, MVP, bvec3(false, true, false));
    corner(index, MVP, bvec3(true, true, false));
    EndPrimitive();

    corner(index, MVP, bvec3(false, false, true));
    corner(index, MVP, bvec3(false, true, true));
    EndPrimitive();

    corner(index, MVP, bvec3(true, false, false));
    corner(index, MVP, bvec3(true, false, true));
    EndPrimitive();

    corner(index, MVP, bvec3(false, false, true));
    corner(index, MVP, bvec3(true, false, true));
    EndPrimitive();

    corner(index, MVP, bvec3(true, false, false));
    corner(index, MVP, bvec3(true, true, false));
    EndPrimitive();

    corner(index, MVP, bvec3(false, true, false));
    corner(index, MVP, bvec3(false, true, true));
    EndPrimitive();

    corner(index, MVP, bvec3(false, true, true));
    corner(index, MVP, bvec3(true, true, true));
    EndPrimitive();

    corner(index, MVP, bvec3(true, false, true));
    corner(index, MVP, bvec3(true, true, true));
    EndPrimitive();

    corner(index, MVP, bvec3(true, true, false));
    corner(index, MVP, bvec3(true, true, true));
    EndPrimitive();
}
