# README #

![logo][logo]

This project is called **Literally Gaming Engine** for Linux® ( **LGE** ) even it is more an 
API than an engine compared with other so called game-engines. It is explicitly 
only supported for linux distributions.

>  The Linux trademark is owned by Linus Torvalds.

### What is this repository for? ###

This project should simplify the programming of applications (games for example) 
using:

 * graphics (2D/3D)
 * physics (2D/3D)
 * networking
 * audio [3D]
 * input
 * math

### How do I get set up? ###

Setting this up should be relatively easy to do. You can clone this repository 
using git.

* `git clone git@gitlab.com:TheJackiMonster/lge.git` or
* `git clone https://gitlab.com/TheJackiMonster/lge.git` or
* download the repository as compressed archive...

After this step you can build an application using this project with `cmake .` 
and `make` afterwards. You can create another project similar to those in the 
directory ![projects][projects]. 
The engine will be included as library.

>  *I develop this project on Archlinux and test it on the latest Ubuntu LTS version, 
but I can't gurantee it works on other distributions.*

### Contribution guidelines ###

Playing around with its current functions and features is desired. If you like 
to contribute ideas or code to it you can open issues or merge-requests.

Forking and developing on your own is also possible since it's contributed under 
this ![LICENSE][license].

### Who do I talk to? ###

At the moment it's just me to work on this project for practise and experience:

 * Tobias Frisch (alias TheJackiMonster) [ https://gitlab.com/TheJackiMonster ]

[logo]: resources/icon.svg "Logo for Literally Gaming Engine"
[projects]: projects "Example Projects"
[license]: LICENSE "MIT License"
