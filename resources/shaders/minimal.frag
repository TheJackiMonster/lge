#version 330 core

in vec3 x_Position;
in vec3 x_Normal;
in vec3 x_Tangent;
in vec3 x_Bitangent;
in vec2 x_Texture;
in vec4 x_Color;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

uniform vec3 u_light;

uniform sampler2D u_diffuse;
uniform sampler2D u_ambient;

out vec4 FragmentColor;

void main() {
    float glass = (1.0f - x_Color.a);
    float metal = min(max((x_Position.y + 1.0f) / 1.5f, 0.0f), 1.0f);

    vec3 g_Light = (u_view * vec4(u_light, 1)).xyz;
    vec3 n_Light = normalize(x_Position - g_Light);

    vec3 n_Reflect = -normalize(reflect(n_Light, x_Normal));

    float ambient = 0.2f * (1.0f + glass + metal * 0.5f);
    float diffuse = max(dot(x_Normal, -n_Light), 0.0f);
    float specular = pow(max(dot(n_Reflect, normalize(x_Position)), 0.0f), 10.0f);

    float brightness = ambient + mix(diffuse, specular, metal) + mix(0.0f, specular, glass);

    float extreme = min(max(brightness - 1.0f, 0.0f), 1.0f);
    float border = pow(max(1.0f + dot(x_Normal, normalize(x_Position)), 0.0f), 5.0f);

    vec2 reflect_uv = vec2((n_Reflect.x + 1.0f) * 0.5f, (n_Reflect.y + 1.0f) * 0.5f);

    vec4 TextureColorReflect = texture(u_ambient, reflect_uv);
    vec4 TextureColor = texture(u_diffuse, x_Texture);

    vec3 Color = mix(brightness * x_Color.rgb * TextureColor.rgb, vec3(1.0f, 1.0f, 1.0f), extreme);
    float Alpha = mix(x_Color.a, 1.0f, (border + pow(extreme, 10.0f)) * glass);

    float metal_threshhold = pow(metal, 1.0f);

    vec4 Position = u_projection * vec4(x_Position, 1);
    vec2 ScreenUV = 0.5f * (vec2(1.0f, 1.0f) - Position.xy / Position.z);

    FragmentColor = texture(u_ambient, ScreenUV) * (1.0f - Alpha) + vec4(mix(Color, TextureColorReflect.rgb, metal_threshhold), Alpha);
}

