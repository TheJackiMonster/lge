#version 430 core

uniform uint u_particle_count_0;
uniform uint u_particle_count_1;

uniform uint u_scan_size;

struct t_particle {
    uint m_amount;
    float m_time;
    vec3 m_position;
    vec3 m_linear_delta [2];
    vec3 m_angular_delta [2];
    vec3 m_movement [2];
};

layout (std430) restrict buffer u_particle_block_0 {
    t_particle u_particles_0 [];
};

layout (std430) restrict buffer u_particle_block_1 {
    t_particle u_particles_1 [];
};

struct t_particle_collision {
    vec4 m_delta [3];
    vec3 m_movement0;
    uint m_amount;
    vec3 m_movement1;
    float m_time;
};

layout (local_size_x = 256) in;

t_particle_collision get_particle(uint index, uint id) {
    t_particle p = (index == 0? u_particles_0[id] : u_particles_1[id]);
    t_particle_collision pc;

    pc.m_amount = p.m_amount;
    pc.m_time = p.m_time;
    pc.m_delta[0] = vec4(p.m_linear_delta[0], p.m_linear_delta[1].x);
    pc.m_delta[1] = vec4(p.m_angular_delta[0], p.m_linear_delta[1].y);
    pc.m_delta[2] = vec4(p.m_angular_delta[1], p.m_linear_delta[1].z);
    pc.m_movement0 = p.m_movement[0];
    pc.m_movement1 = p.m_movement[1];

    return pc;
}

void set_particle(uint index, uint id, t_particle_collision pc) {
    t_particle p = (index == 0? u_particles_0[id] : u_particles_1[id]);

    p.m_amount = pc.m_amount;
    p.m_time = pc.m_time;
    p.m_linear_delta[0] = pc.m_delta[0].xyz;
    p.m_linear_delta[1] = vec3(pc.m_delta[0].w, pc.m_delta[1].w, pc.m_delta[2].w);
    p.m_angular_delta[0] = pc.m_delta[1].xyz;
    p.m_angular_delta[1] = pc.m_delta[2].xyz;
    p.m_movement[0] = pc.m_movement0;
    p.m_movement[1] = pc.m_movement1;

    if (index == 0) {
        u_particles_0[id] = p;
    } else {
        u_particles_1[id] = p;
    }
}

shared t_particle_collision shared_particles [256];

void main() {
    uint full_count = u_particle_count_0 + u_particle_count_1;
    uint particle_index = gl_GlobalInvocationID.x;

    if (particle_index * u_scan_size >= full_count) {
        return;
    }

    uint buffer_index = (particle_index / u_particle_count_0) % 2;

    uint id = (particle_index - buffer_index * u_particle_count_0);

    uint local_size = gl_WorkGroupSize.x;
    uint local_index = gl_LocalInvocationIndex;

    if (gl_WorkGroupID.x + 1 == gl_NumWorkGroups.x) {
        uint local_rest = (((full_count + u_scan_size - 1) / u_scan_size) % gl_WorkGroupSize.x);

        if (local_rest > 0) {
            local_size = local_rest;
        }
    }

    shared_particles[local_index] = get_particle(buffer_index, id);

    barrier();

    uint index, compare_index;
    t_particle_collision comp, part;

    for (uint size = 1; size < local_size; size *= 2) {
        index = 2 * size * local_index;
        compare_index = index + size;

        if (compare_index < local_size) {
            comp = shared_particles[compare_index];

            if (comp.m_amount > 0) {
                part = shared_particles[index];

                if ((part.m_amount == 0) || (comp.m_time < part.m_time)) {
                    part = comp;
                } else
                if (comp.m_time == part.m_time) {
                    part.m_amount += comp.m_amount;

                    part.m_delta[0] += comp.m_delta[0];
                    part.m_delta[1] += comp.m_delta[1];
                    part.m_delta[2] += comp.m_delta[2];

                    if (length(comp.m_movement0) > length(part.m_movement0)) {
                        part.m_movement0 = comp.m_movement0;
                    }

                    if (length(comp.m_movement1) > length(part.m_movement1)) {
                        part.m_movement1 = comp.m_movement1;
                    }
                }

                shared_particles[index] = part;
            }
        }

        memoryBarrierShared();
    }

    if (local_index == 0) {
        set_particle(buffer_index, gl_WorkGroupID.x, shared_particles[local_index]);
    }
}