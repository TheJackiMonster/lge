#version 400

layout(points) in;
layout(triangle_strip, max_vertices=6) out;

in vec4 x_Bounds[];
in vec4 x_TextureBounds[];
in vec4 x_Color[];
in float x_Depth[];

uniform mat3 u_transform;

out vec2 y_Relative;
out vec2 y_Size;
out vec2 y_Texture;
out vec4 y_Color;

void corner(vec2 relative) {
    y_Relative = 2 * relative - vec2(1, 1);
    y_Size = x_Bounds[0].zw;
    y_Texture = x_TextureBounds[0].xy + x_TextureBounds[0].zw * relative;
    y_Color = x_Color[0];

    vec2 position = (u_transform * vec3(x_Bounds[0].xy + x_Bounds[0].zw * relative, 1)).xy;

    gl_Position = vec4(position, x_Depth[0], 1);

    EmitVertex();
}

void main() {
    if (x_Color[0].a > 0.0f) {
        corner(vec2(0, 0));
        corner(vec2(1, 0));
        corner(vec2(0, 1));

        EndPrimitive();

        corner(vec2(0, 1));
        corner(vec2(1, 0));
        corner(vec2(1, 1));

        EndPrimitive();
    }
}
