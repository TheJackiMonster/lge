#version 330 core

in vec4 Bounds;
in vec4 TextureBounds;
in vec4 Color;
in float Depth;

out vec4 x_Bounds;
out vec4 x_TextureBounds;
out vec4 x_Color;
out float x_Depth;

void main() {
    x_Bounds = Bounds;
    x_TextureBounds = TextureBounds;
    x_Color = Color;
    x_Depth = Depth * 2 - 1;
}
