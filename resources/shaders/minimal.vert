#version 330 core

in vec3 Position;
in vec3 Normal;
in vec3 Tangent;
in vec3 Bitangent;
in vec2 Texture;
in vec4 Color;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

uniform vec3 u_light;

out vec3 x_Position;
out vec3 x_Normal;
out vec3 x_Tangent;
out vec3 x_Bitangent;
out vec2 x_Texture;
out vec4 x_Color;

void main() {
    mat4 ModelView = u_view * u_model;

    vec4 g_Position = ModelView * vec4(Position, 1);
    mat4 n_ModelView = transpose(inverse(ModelView));

    x_Position = g_Position.xyz;
    x_Normal = normalize((n_ModelView * vec4(Normal, 0)).xyz);
    x_Tangent = normalize((n_ModelView * vec4(Tangent, 0)).xyz);
    x_Bitangent = normalize((n_ModelView * vec4(Bitangent, 0)).xyz);
    x_Texture = Texture;
    x_Color = Color;

    gl_Position = u_projection * g_Position;
}
