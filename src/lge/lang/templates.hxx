#pragma once
//
// Created by thejackimonster on 30.11.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "types.hxx"

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::add_const;
	
	using std::add_cv;
	
	using std::add_volatile;
	
	using std::remove_const;
	
	using std::remove_cv;
	
	using std::remove_volatile;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::add_pointer;
	
	using std::add_lvalue_reference;
	
	using std::add_rvalue_reference;
	
	using std::decay;
	
	using std::make_signed;
	
	using std::make_unsigned;
	
	using std::remove_all_extents;
	
	using std::remove_extent;
	
	using std::remove_pointer;
	
	using std::remove_reference;
	
	using std::underlying_type;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::aligned_storage;
	
	using std::aligned_union;
	
	using std::common_type;
	
	using std::conditional;
	
	using std::enable_if;
	
	using std::result_of;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	template <typename T>
	struct const_ref {
		typedef typename conditional< is_arithmetic< T >::value, T, const T& >::type type;
	};
	
	/** @} */

}
