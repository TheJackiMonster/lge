#pragma once
//
// Created by thejackimonster on 24.01.20.
//
// Copyright (c) 2020 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <typeindex>
#include <typeinfo>
#include <type_traits>

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::type_index;
	
	using std::type_info;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::is_array;
	
	using std::is_class;
	
	using std::is_enum;
	
	using std::is_floating_point;
	
	using std::is_function;
	
	using std::is_integral;
	
	using std::is_lvalue_reference;
	
	using std::is_member_function_pointer;
	
	using std::is_member_object_pointer;
	
	using std::is_pointer;
	
	using std::is_rvalue_reference;
	
	using std::is_union;
	
	using std::is_void;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::is_arithmetic;
	
	using std::is_compound;
	
	using std::is_fundamental;
	
	using std::is_member_pointer;
	
	using std::is_object;
	
	using std::is_reference;
	
	using std::is_scalar;
	
	/** @} */
	
}

namespace lge {
	
	/** @addtogroup lang_group
	 *  @{
	 */
	
	using std::is_abstract;
	
	using std::is_const;
	
	using std::is_empty;
	
	using std::is_literal_type;
	
	using std::is_pod;
	
	using std::is_polymorphic;
	
	using std::is_signed;
	
	using std::is_standard_layout;
	
	using std::is_trivial;
	
	using std::is_trivially_copyable;
	
	using std::is_unsigned;
	
	using std::is_volatile;
	
	/** @} */
	
}
