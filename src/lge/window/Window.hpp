#pragma once

//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <GLFW/glfw3.h>

#include "../lang/primitives.hxx"
#include "../lang/string.hxx"
#include "../math/vec.hxx"
#include "../memory/move.hxx"
#include "../util/event.hxx"

namespace lge {
	
	/** @defgroup glfw_group The GLFW Group
	 *  A group for classes and structures representing GLFW functionality.
	 *  @{
	 */
	
	/**
	 * An object-oriented abstraction layer of GLFW to handle windows in C++.
	 * @see <a href="https://www.glfw.org/docs/latest/window_guide.html">GLFW: Window guide</a>
	 *
	 * @author TheJackiMonster
	 * @since 08.12.2017
	 */
	class Window {
	private:
		static int m_window_count;
		
		static void init();
		
		static void terminate();
		
		static void onError(int error, const char* description);
		
		static void onResize(GLFWwindow* resource, int width, int height);
		
		static void onKeyEvent(GLFWwindow* resource, int key, int scancode, int action, int mods);

#if ((GLFW_VERSION_MAJOR == 3) and (GLFW_VERSION_MINOR < 1)) or (GLFW_VERSION_MAJOR < 3)
		
		static void onCharEvent(GLFWwindow* resource, uint codepoint);

#endif
		
		static void onCharModsEvent(GLFWwindow* resource, uint codepoint, int mods);
		
		static void onMouseEvent(GLFWwindow* resource, int button, int action, int mods);
		
		static void onMouseMoveEvent(GLFWwindow* resource, double x, double y);
		
		static void onMouseScrollEvent(GLFWwindow* resource, double xoffset, double yoffset);
		
		GLFWwindow* m_resource;
		
		int m_switch_resources[4];
		
		void setup();
	
	public:
		event< int, int > e_Resize;
		event< int, int, int, int > e_Key;
		event< int, int > e_Char;
		event< int, int, int > e_Mouse;
		event< double, double > e_MouseMove;
		event< double, double > e_MouseScroll;
		
		/**
		 * Checks for any occured internal events related to active windows.
		 */
		static void pollEvents();
		
		/**
		 * Sets interval to synchronize the framerate during swapping
		 * the draw-buffer of active windows.
		 * @see swap()
		 *
		 * @param interval <i>in frames</i>
		 */
		static void setSwapInterval(int interval);
		
		/**
		 * Creates a fullscreen-mode window with specific title.
		 *
		 * @param title
		 */
		explicit Window(const string& title);
		
		/**
		 * Creates a window with specific title and size.
		 *
		 * @param title
		 * @param width <i>(in pixels)</i>
		 * @param height <i>(in pixels)</i>
		 */
		explicit Window(const string& title, uint width, uint height);
		
		Window(const Window& other) = delete;
		
		Window(Window&& other) noexcept;
		
		~Window();
		
		Window& operator=(const Window& other) = delete;
		
		Window& operator=(Window&& other) noexcept;
		
		/**
		 * Returns the actual state of the windows fullscreen-mode.
		 *
		 * @return <b>true</b> if the window is in fullscreen-mode.
		 */
		[[nodiscard]]
		bool isFullscreen() const;
		
		/**
		 * Returns the actual width of the window.
		 *
		 * @return the width <i>(in pixels)</i>
		 */
		[[nodiscard]]
		uint getWidth() const;
		
		/**
		 * Returns the actual height of the window.
		 *
		 * @return the height <i>(in pixels)</i>
		 */
		[[nodiscard]]
		uint getHeight() const;
		
		/**
		 * Returns the actual x-coord of the windows position.
		 *
		 * @return x-coord of position <i>(in pixels)</i>
		 */
		[[nodiscard]]
		int getX() const;
		
		/**
		 * Returns the actual y-coord of the windows position.
		 *
		 * @return y-coord of position <i>(in pixels)</i>
		 */
		[[nodiscard]]
		int getY() const;
		
		/**
		 * Returns if the window is currently in focus of active input.
		 * @see focus()
		 *
		 * @return <b>true</b> if the window is in focus, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isFocused() const;
		
		/**
		 * Returns if the window is currently iconified.
		 *
		 * @return <b>true</b> if the window is iconified, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isIconified() const;
		
		/**
		 * Returns if the window is currently maximized.
		 *
		 * @return <b>true</b> if the window is maximized, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isMaximized() const;
		
		/**
		 * Returns if the window is currently visible.
		 *
		 * @return <b>true</b> if the window is visible, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isVisible() const;
		
		/**
		 * Returns if the window can be resized by the user dynamicly.
		 *
		 * @return <b>true</b> if the window can be resized, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isResizable() const;
		
		/**
		 * Returns if the window has decorations such as a title-bar.
		 *
		 * @return <b>true</b> if the window has decorations, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isDecorated() const;
		
		/**
		 * Returns if the window is floating.
		 *
		 * @return <b>true</b> if the window is floating, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isFloating() const;
		
		/**
		 * Returns if this window is the current context for graphics rendering.
		 * @see makeContext()
		 *
		 * @return <b>true</b> if this window is the current active context, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool isContext() const;
		
		/**
		 * Returns the actual state of any occured closing-event.
		 * @see pollEvents()
		 *
		 * @return <b>true</b> if the window shall be closed, otherwise <b>false</b>.
		 */
		[[nodiscard]]
		bool shouldClose() const;
		
		/**
		 * Sets the windows title.
		 *
		 * @param title
		 */
		void setTitle(const string& title);
		
		/**
		 * Activates/Deactivates the windows fullscreen-mode.
		 *
		 * @param fullscreen
		 */
		void setFullscreen(bool fullscreen);
		
		/**
		 * Sets the actual width of the window.
		 *
		 * @param width <i>(in pixels)</i>
		 */
		void setWidth(uint width);
		
		/**
		 * Sets the actual height of the window.
		 *
		 * @param height <i>(in pixels)</i>
		 */
		void setHeight(uint height);
		
		/**
		 * Sets the actual size of the window.
		 *
		 * @param width <i>(in pixels)</i>
		 * @param height <i>(in pixels)</i>
		 */
		void setSize(uint width, uint height);
		
		/**
		 * Sets the actual x-coord of the windows position.
		 *
		 * @param x <i>(in pixels)</i>
		 */
		void setX(int x);
		
		/**
		 * Sets the actual y-coord of the windows position.
		 *
		 * @param y <i>(in pixels)</i>
		 */
		void setY(int y);
		
		/**
		 * Sets the actual position of the window.
		 *
		 * @param x <i>(in pixels)</i>
		 * @param y <i>(in pixels)</i>
		 */
		void setPosition(int x, int y);
		
		/**
		 * Sets the current state of iconifying seperately.
		 *
		 * @param iconified
		 */
		void setIconified(bool iconified);
		
		/**
		 * Sets the current state of maximizing seperately.
		 *
		 * @param maximized
		 */
		void setMaximized(bool maximized);
		
		/**
		 * Sets the visibility of the window.
		 *
		 * @param visible
		 */
		void setVisible(bool visible);
		
		/**
		 * Makes this window to the active context for graphics rendering.
		 */
		void makeContext();
		
		/**
		 * Requests to bring the window into focus of active input.
		 */
		void focus();
		
		/**
		 * Requests to minimize/iconify the window.
		 * @see restore()
		 */
		void iconify();
		
		/**
		 * Requests to maximize the window.
		 * @see restore()
		 */
		void maximize();
		
		/**
		 * Restores the windows last state before any iconifying or maximizing.
		 * @see iconify()
		 * @see maximize()
		 */
		void restore();
		
		/**
		 * Reveals/Shows the window if it was hidden before.
		 * @see hide()
		 */
		void show();
		
		/**
		 * Hides the window if it was visible before.
		 * @see show()
		 */
		void hide();
		
		/**
		 * Swaps the current draw-buffer for rendering frames and
		 * synchronizes to a specific framerate set with <b>setSwapInterval()</b>.
		 * @see FrameBuffer
		 */
		void swap();
		
	};
	
	/** @} */
	
}
