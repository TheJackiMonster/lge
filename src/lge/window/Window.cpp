//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Window.hpp"

namespace lge {
	
	int Window::m_window_count = 0;
	
	void Window::init() {
		if (m_window_count == 0) {
			if (glfwInit() == GLFW_TRUE) {
				glfwSetErrorCallback(Window::onError);
				
				glfwDefaultWindowHints();
				
				glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
				glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);

				glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_FALSE);
				glfwWindowHint(GLFW_SAMPLES, 4);
				
				glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
				glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
				
				glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
				
				cout << "[GLFW] Version: " << glfwGetVersionString() << endl;
			} else {
				cerr << "[GLFW] Error: Could not be initialized" << endl;
			}
		}
	}
	
	void Window::terminate() {
		if (m_window_count <= 0) {
			glfwTerminate();
		}
	}
	
	void Window::onError(int error, const char* description) {
		cerr << "[ERROR: " << error << "] - " << description << endl;
	}
	
	void Window::onResize(GLFWwindow* resource, int width, int height) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_Resize(width, height);
		}
	}
	
	void Window::onKeyEvent(GLFWwindow* resource, int key, int scancode, int action, int mods) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_Key(key, scancode, action, mods);
		}
	}

#if ((GLFW_VERSION_MAJOR == 3) and (GLFW_VERSION_MINOR < 1)) or (GLFW_VERSION_MAJOR < 3)
	
	void Window::onCharEvent(GLFWwindow* resource, uint codepoint) {
		Window::onCharModsEvent(resource, codepoint, 0);
	}

#endif
	
	void Window::onCharModsEvent(GLFWwindow* resource, uint codepoint, int mods) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_Char(codepoint, mods);
		}
	}
	
	void Window::onMouseEvent(GLFWwindow* resource, int button, int action, int mods) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_Mouse(button, action, mods);
		}
	}
	
	void Window::onMouseMoveEvent(GLFWwindow* resource, double x, double y) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_MouseMove(x, y);
		}
	}
	
	void Window::onMouseScrollEvent(GLFWwindow* resource, double xoffset, double yoffset) {
		auto window = reinterpret_cast<Window*>(glfwGetWindowUserPointer(resource));
		
		if (window != nullptr) {
			window->e_MouseScroll(xoffset, yoffset);
		}
	}
	
	void Window::setup() {
		if (this->m_resource != nullptr) {
			glfwSetWindowUserPointer(this->m_resource, this);
			
			glfwSetWindowSizeCallback(this->m_resource, Window::onResize);
			
			glfwSetKeyCallback(this->m_resource, Window::onKeyEvent);

#if ((GLFW_VERSION_MAJOR == 3) and (GLFW_VERSION_MINOR >= 1)) or (GLFW_VERSION_MAJOR > 3)
			glfwSetCharModsCallback(this->m_resource, Window::onCharModsEvent);
#else
			glfwSetCharCallback(this->m_resource, Window::onCharEvent);
#endif
			
			glfwSetMouseButtonCallback(this->m_resource, Window::onMouseEvent);
			glfwSetCursorPosCallback(this->m_resource, Window::onMouseMoveEvent);
			glfwSetScrollCallback(this->m_resource, Window::onMouseScrollEvent);
			
			m_window_count++;
		} else {
			cerr << "[GLFW] Error: Window could not be created" << endl;
		}
	}
	
	void Window::pollEvents() {
		//cout << "poll() - [begin]" << endl;
		
		glfwPollEvents();
		
		//cout << "poll() - [end]" << endl;
	}
	
	void Window::setSwapInterval(int interval) {
		glfwSwapInterval(interval);
	}
	
	Window::Window(const string& title) {
		Window::init();
		
		GLFWmonitor* monitor = glfwGetPrimaryMonitor();
		
		const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
		
		this->m_resource = glfwCreateWindow(
				videoMode->width,
				videoMode->height,
				title.c_str(),
				monitor,
				nullptr
		);
		
		this->setup();
	}
	
	Window::Window(const string& title, uint width, uint height) {
		Window::init();
		
		this->m_resource = glfwCreateWindow(
				static_cast<int>(width),
				static_cast<int>(height),
				title.c_str(), nullptr, nullptr
		);
		
		this->setup();
	}
	
	Window::Window(Window&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
		
		this->m_switch_resources[0] = other.m_switch_resources[0];
		this->m_switch_resources[1] = other.m_switch_resources[1];
		this->m_switch_resources[2] = other.m_switch_resources[2];
		this->m_switch_resources[3] = other.m_switch_resources[3];
	}
	
	Window::~Window() {
		if (this->m_resource != nullptr) {
			glfwDestroyWindow(this->m_resource);
			
			m_window_count--;
		}
		
		Window::terminate();
	}
	
	Window& Window::operator=(Window&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	bool Window::isFullscreen() const {
		return (glfwGetWindowMonitor(this->m_resource) != nullptr);
	}
	
	uint Window::getWidth() const {
		int value;
		
		glfwGetWindowSize(this->m_resource, &value, nullptr);
		
		return static_cast<uint>(value);
	}
	
	uint Window::getHeight() const {
		int value;
		
		glfwGetWindowSize(this->m_resource, nullptr, &value);
		
		return static_cast<uint>(value);
	}
	
	int Window::getX() const {
		int value;
		
		glfwGetWindowPos(this->m_resource, &value, nullptr);
		
		return value;
	}
	
	int Window::getY() const {
		int value;
		
		glfwGetWindowPos(this->m_resource, nullptr, &value);
		
		return value;
	}
	
	bool Window::isFocused() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_FOCUSED) == GLFW_TRUE);
	}
	
	bool Window::isIconified() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_ICONIFIED) == GLFW_TRUE);
	}
	
	bool Window::isMaximized() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_MAXIMIZED) == GLFW_TRUE);
	}
	
	bool Window::isVisible() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_VISIBLE) == GLFW_TRUE);
	}
	
	bool Window::isResizable() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_RESIZABLE) == GLFW_TRUE);
	}
	
	bool Window::isDecorated() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_DECORATED) == GLFW_TRUE);
	}
	
	bool Window::isFloating() const {
		return (glfwGetWindowAttrib(this->m_resource, GLFW_FLOATING) == GLFW_TRUE);
	}
	
	bool Window::isContext() const {
		return (glfwGetCurrentContext() == this->m_resource);
	}
	
	bool Window::shouldClose() const {
		return (glfwWindowShouldClose(this->m_resource) == GLFW_TRUE);
	}
	
	void Window::setTitle(const string& title) {
		glfwSetWindowTitle(this->m_resource, title.c_str());
	}
	
	void Window::setFullscreen(bool fullscreen) {
		GLFWmonitor* monitor = glfwGetWindowMonitor(this->m_resource);
		
		if ((fullscreen) && (monitor == nullptr)) {
			glfwGetWindowPos(
					this->m_resource,
					&(this->m_switch_resources[0]),
					&(this->m_switch_resources[1])
			);
			
			glfwGetWindowSize(
					this->m_resource,
					&(this->m_switch_resources[2]),
					&(this->m_switch_resources[3])
			);
			
			monitor = glfwGetPrimaryMonitor();
			
			const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
			
			glfwSetWindowMonitor(
					this->m_resource,
					monitor,
					0, 0,
					videoMode->width,
					videoMode->height,
					videoMode->refreshRate
			);
		} else if ((!fullscreen) && (monitor != nullptr)) {
			glfwSetWindowMonitor(
					this->m_resource,
					nullptr,
					this->m_switch_resources[0],
					this->m_switch_resources[1],
					this->m_switch_resources[2],
					this->m_switch_resources[3],
					GLFW_DONT_CARE
			);
		}
	}
	
	void Window::setWidth(uint width) {
		this->setSize(width, this->getHeight());
	}
	
	void Window::setHeight(uint height) {
		this->setSize(this->getWidth(), height);
	}
	
	void Window::setSize(uint width, uint height) {
		glfwSetWindowSize(
				this->m_resource,
				static_cast<int>(width),
				static_cast<int>(height)
		);
	}
	
	void Window::setX(int x) {
		this->setPosition(x, this->getY());
	}
	
	void Window::setY(int y) {
		this->setPosition(this->getX(), y);
	}
	
	void Window::setPosition(int x, int y) {
		glfwSetWindowPos(
				this->m_resource,
				x, y
		);
	}
	
	void Window::setIconified(bool iconified) {
		if (iconified) {
			this->iconify();
		} else {
			this->restore();
		}
	}
	
	void Window::setMaximized(bool maximized) {
		if (maximized) {
			this->maximize();
		} else {
			this->restore();
		}
	}
	
	void Window::setVisible(bool visible) {
		if (visible) {
			this->show();
		} else {
			this->hide();
		}
	}
	
	void Window::makeContext() {
		glfwMakeContextCurrent(this->m_resource);
	}
	
	void Window::focus() {
		glfwFocusWindow(this->m_resource);
	}
	
	void Window::iconify() {
		glfwIconifyWindow(this->m_resource);
	}
	
	void Window::maximize() {
		glfwMaximizeWindow(this->m_resource);
	}
	
	void Window::restore() {
		glfwRestoreWindow(this->m_resource);
	}
	
	void Window::show() {
		glfwShowWindow(this->m_resource);
	}
	
	void Window::hide() {
		glfwHideWindow(this->m_resource);
	}
	
	void Window::swap() {
		glfwSwapBuffers(this->m_resource);
	}
}
