#pragma once

//
// Created by thejackimonster on 01.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "lge/lang/primitives.hxx"

#include <atomic>

namespace lge {
	
	/** @addtogroup system_group
	 *  @{
	 */
	
	template < typename T >
	using atomic = std::atomic< T >;
	
	template < typename T = uint >
	T atomicAdd(atomic< T >& mem, T data) {
		return mem.fetch_add(data);
	}
	
	template < typename T = uint >
	T atomicSub(atomic< T >& mem, T data) {
		return mem.fetch_sub(data);
	}
	
	template < typename T = uint >
	T atomicAnd(atomic< T >& mem, T data) {
		return mem.fetch_and(data);
	}
	
	template < typename T = uint >
	T atomicOr(atomic< T >& mem, T data) {
		return mem.fetch_or(data);
	}
	
	template < typename T = uint >
	T atomicXor(atomic< T >& mem, T data) {
		return mem.fetch_xor(data);
	}
	
	template < typename T = uint >
	T atomicCompSwap(atomic< T >& mem, T compare, T data) {
		mem.compare_exchange_strong(compare, data);
		return compare;
	}
	
	template < typename T = uint >
	T atomicExchange(atomic< T >& mem, T data) {
		return mem.exchange(data);
	}
	
	template < typename T = uint >
	T atomicCounter(const atomic< T >& counter) {
		return counter.load();
	}
	
	template < typename T = uint >
	T atomicCounterDecrement(atomic< T >& counter) {
		return counter--;
	}
	
	template < typename T = uint >
	T atomicCounterIncrement(atomic< T >& counter) {
		return counter++;
	}
	
	/** @} */
	
}
