#pragma once
//
// Created by thejackimonster on 31.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <chrono>

namespace lge {
	
	/** @defgroup time_group The Time Group
	 *  This group is for time related functionality.
	 *  @{
	 */
	
	template < typename T = float >
	T currentTime() {
		static const auto initTime = std::chrono::system_clock::now();
		
		return (static_cast<T>(std::chrono::duration_cast< std::chrono::nanoseconds >(
				std::chrono::system_clock::now() - initTime
		).count()) / 1000000000);
	}
	
	template < typename T = float >
	T deltaTime() {
		static T lastTime = currentTime< T >();
		
		T time = currentTime< T >();
		T result = (time - lastTime);
		
		lastTime = time;
		return result;
	}
	
	/** @} */
	
}
