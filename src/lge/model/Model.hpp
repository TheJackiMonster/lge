#pragma once

//
// Created by thejackimonster on 10.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/string.hxx"
#include "../graphics/vertex/Array.hpp"
#include "../graphics/vec.hxx"
#include "../physics/bounds/AxisAlignedBoundingBox.hpp"
#include "../physics/bounds/AxisAlignedBoundingCube.hpp"
#include "../system/iostream.hxx"

namespace lge {
	
	/** @defgroup model_group The Model Group
	 *  This group is for classes and structures for models or meshes.
	 *  @{
	 */
	
	class Model {
	public:
		typedef vec< 12, 1, float > vertex;
	
	private:
		VertexArray m_array;
		
		uint m_count;
	
	public:
		explicit Model();
		
		~Model();
		
		bool load(const string& path);
		
		void setup(const ShaderAttributes& bindings);
		
		void draw(bool drawPoints = false) const;
		
		void calcAABB(const mat< 4 >& model, AxisAlignedBoundingBox& bounds);
        void calcAABC(AxisAlignedBoundingCube& bounds);
		
		[[nodiscard]]
		vector< vec< 6>> getPositions() const;
		
	};
	
	/** @} */
	
}
