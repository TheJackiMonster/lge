//
// Created by thejackimonster on 10.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Model.hpp"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

namespace lge {
	
	Model::Model() {
		VertexStructure vs(4);
		
		vs.attribute("Position", 3);
		vs.attribute("Normal", 3, true);
		vs.attribute("Color", 4);
		vs.attribute("Texture", 2);
		
		this->m_array.addBuffer(vs);
		this->m_count = 0;
	}
	
	Model::~Model() {
		this->m_array.releaseBuffers();
	}
	
	inline void pushVertex(vector< Model::vertex >& vertices, uint& v, const aiScene* scene, uint i, uint index,
			float& maxLen) {
		const aiMesh* mesh = scene->mMeshes[i];
		
		vertices[v][0] = mesh->mVertices[index].x;
		vertices[v][1] = mesh->mVertices[index].y;
		vertices[v][2] = mesh->mVertices[index].z;
		
		maxLen = max(maxLen, length(vertices[v].column<3>(0)));
		
		if (mesh->HasNormals()) {
			vertices[v][3] = mesh->mNormals[index].x;
			vertices[v][4] = mesh->mNormals[index].y;
			vertices[v][5] = mesh->mNormals[index].z;
		} else {
			vertices[v][3] = 0;
			vertices[v][4] = 0;
			vertices[v][5] = 0;
		}
		
		if (mesh->HasVertexColors(0)) {
			vertices[v][6] = mesh->mColors[0][index].r;
			vertices[v][7] = mesh->mColors[0][index].g;
			vertices[v][8] = mesh->mColors[0][index].b;
			vertices[v][9] = mesh->mColors[0][index].a;
		} else {
			vertices[v][6] = 0.0f;
			vertices[v][7] = 0.0f;
			vertices[v][8] = 0.0f;
			vertices[v][9] = 1.0f;
		}
		
		if (mesh->HasTextureCoords(0)) {
			vertices[v][10] = mesh->mTextureCoords[0][index].x;
			vertices[v][11] = mesh->mTextureCoords[0][index].y;
		} else {
			vertices[v][10] = 0;
			vertices[v][11] = 0;
		}
		
		v++;
	}
	
	bool Model::load(const string& path) {
		Assimp::Importer importer;
		
		const aiScene* scene = importer.ReadFile(
				path.c_str(),
				aiProcess_Triangulate |
				aiProcess_GenSmoothNormals |
				aiProcess_FixInfacingNormals |
				aiProcess_JoinIdenticalVertices |
				aiProcess_PreTransformVertices
		);
		
		if (scene) {
			this->m_count = 0;
			
			for (uint i = 0; i < scene->mNumMeshes; i++) {
				if (scene->mMeshes[i]->HasFaces()) {
					for (uint j = 0; j < scene->mMeshes[i]->mNumFaces; j++) {
						this->m_count += scene->mMeshes[i]->mFaces[j].mNumIndices;
					}
				} else {
					this->m_count += scene->mMeshes[i]->mNumVertices;
				}
			}
			
			vector< vertex > vertices(this->m_count);
			
			float maxLen = 0.0f;
			uint v = 0;
			
			for (uint i = 0; i < scene->mNumMeshes; i++) {
				if (scene->mMeshes[i]->HasFaces()) {
					for (uint j = 0; j < scene->mMeshes[i]->mNumFaces; j++) {
						for (uint k = 0; k < scene->mMeshes[i]->mFaces[j].mNumIndices; k++) {
							pushVertex(vertices, v, scene, i, scene->mMeshes[i]->mFaces[j].mIndices[k], maxLen);
						}
					}
				} else {
					for (uint j = 0; j < scene->mMeshes[i]->mNumVertices; j++) {
						pushVertex(vertices, v, scene, i, j, maxLen);
					}
				}
			}

            for (v = 0; v < this->m_count; v++) {
                vertices[v].column< 3 >(0) /= maxLen * 2.0f;
            }
			
			this->m_array[0].data(this->m_count, vertices.data());
			
			importer.FreeScene();
			
			return true;
		} else {
			cerr << importer.GetErrorString() << endl;
			
			return false;
		}
	}
	
	void Model::setup(const ShaderAttributes& bindings) {
		this->m_array.setup(bindings);
	}
	
	void Model::draw(bool drawPoints) const {
		if (this->m_count > 0) {
			this->m_array.draw(drawPoints? DrawType::POINTS : DrawType::TRIANGLES, this->m_count);
		}
	}
	
	void Model::calcAABB(const mat< 4 >& model, AxisAlignedBoundingBox& bounds) {
		vector< vertex > vertices(this->m_count);
		
		this->m_array[0].getData(this->m_count, vertices.data());
		
		float f_min = numeric_limits< float >::min();
		float f_max = numeric_limits< float >::max();
		
		bounds.resizeTo(lge::vec3(f_max, f_max, f_max), lge::vec3(f_min, f_min, f_min));

        mat< 3 > R = model.field< 3, 3 >(0, 0);

        R.column< 3 >(0) = normalize< 3 >(R.column< 3 >(0));
        R.column< 3 >(1) = normalize< 3 >(R.column< 3 >(1));
        R.column< 3 >(2) = normalize< 3 >(R.column< 3 >(2));

        for (vertex& v : vertices) {
            auto& position = v.column< 3 >(0);

            const vec3 p = R * position;

            bounds.adaptSize(p);
        }
	}

    void Model::calcAABC(AxisAlignedBoundingCube& bounds) {
        //vector< vertex > vertices(this->m_count);

        //this->m_array[0].getData(this->m_count, vertices.data());

        bounds.moveTo(vec3(0.0f));
        bounds.resizeTo(1.0f);

        /*for (vertex& v : vertices) {
            bounds.adaptSize(v.column< 3 >(0));
        }*/
    }
	
	vector< vec< 6>> Model::getPositions() const {
		vector< vec< 6>> positions(this->m_count);
		vector< vertex > vertices(this->m_count);
		
		this->m_array[0].getData(this->m_count, vertices.data());
		
		for (uint i = 0; i < this->m_count; i++) {
			positions[i] = vertices[i].column< 6 >(0);
		}
		
		return positions;
	}
}
