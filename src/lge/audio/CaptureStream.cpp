//
// Created by thejackimonster on 30.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "CaptureStream.hpp"

namespace lge {
	
	AudioCaptureStream::AudioCaptureStream(uint frequency, AudioFormat format, uint max_framerate)
			:
			m_device(frequency, format, (frequency + max_framerate - 1) / max_framerate * 2),
			m_sample_offset(0) {
		this->m_buffer.resize(this->m_device.getSize());
	}
	
	AudioCaptureStream::AudioCaptureStream(const string& name, uint frequency, AudioFormat format, uint max_framerate)
			:
			m_device(name, frequency, format, (frequency + max_framerate - 1) / max_framerate * 2),
			m_sample_offset(0) {
		this->m_buffer.resize(this->m_device.getSize());
	}
	
	AudioCaptureDevice& AudioCaptureStream::getDevice() {
		return this->m_device;
	}
	
	const AudioCaptureDevice& AudioCaptureStream::getDevice() const {
		return this->m_device;
	}
	
	void AudioCaptureStream::update(bool capture) {
		if (capture != this->m_device.isCapturing()) {
			if (capture) {
				this->m_device.start();
			} else {
				this->m_device.stop();
			}
		}
		
		const int max_samples = this->m_device.maxSamples();
		
		int samples = this->m_device.availableSamples();
		
		if (this->m_sample_offset + samples > max_samples) {
			samples = max_samples - this->m_sample_offset;
		}
		
		const lge::AudioFormat& format = this->m_device.getFormat();
		
		if (samples > 0) {
			const int offset = lge::getFormatSize(lge::AudioFormat::STEREO_16, this->m_sample_offset);
			
			this->m_device.capture(this->m_buffer.data() + offset, samples);
			this->m_sample_offset += samples;
		}
	}
	
	bool AudioCaptureStream::pull(lge::AudioBuffer& buffer) {
		const lge::AudioFormat& format = this->m_device.getFormat();
		
		if (this->m_sample_offset > 0) {
			buffer.setData(this->m_device.getFrequency(), format, this->m_buffer.data(), this->m_sample_offset);
			
			this->m_sample_offset = 0;
			return true;
		} else {
			return false;
		}
	}
}

#endif
