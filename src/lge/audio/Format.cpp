//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Format.hpp"

#include <AL/al.h>

namespace lge {
	
	int getFormatValue(AudioFormat format) {
		switch (format) {
			case AudioFormat::MONO_8:
				return AL_FORMAT_MONO8;
			case AudioFormat::MONO_16:
				return AL_FORMAT_MONO16;
			case AudioFormat::STEREO_8:
				return AL_FORMAT_STEREO8;
			case AudioFormat::STEREO_16:
				return AL_FORMAT_STEREO16;
			default:
				return 0;
		}
	}
	
	int getFormatBits(AudioFormat format) {
		switch (format) {
			case AudioFormat::MONO_8:
				return 8 * 1;
			case AudioFormat::MONO_16:
				return 16 * 1;
			case AudioFormat::STEREO_8:
				return 8 * 2;
			case AudioFormat::STEREO_16:
				return 16 * 2;
			default:
				return 0;
		}
	}
	
	int getFormatChannels(AudioFormat format) {
		if ((format == AudioFormat::MONO_8) || (format == AudioFormat::MONO_16)) {
			return 1;
		} else if ((format == AudioFormat::STEREO_8) || (format == AudioFormat::STEREO_16)) {
			return 2;
		} else {
			return 0;
		}
	}
	
	int getFormatSize(AudioFormat format, int samples) {
		return samples * getFormatBits(format) / 8;
	}
}

#endif
