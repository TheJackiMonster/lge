//
// Created by thejackimonster on 08.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Source.hpp"

namespace lge {
	
	AudioSource::AudioSource() {
		alGenSources(1, &(this->m_resource));
	}
	
	AudioSource::AudioSource(lge::AudioSource&& other) noexcept {
		this->m_resource = other.m_resource;
		other.m_resource = 0;
	}
	
	AudioSource::~AudioSource() {
		if (this->m_resource != 0) {
			alDeleteSources(1, &(this->m_resource));
		}
	}
	
	AudioSource& AudioSource::operator=(lge::AudioSource&& other) noexcept {
		if (this->m_resource != 0) {
			alDeleteSources(1, &(this->m_resource));
		}
		
		this->m_resource = other.m_resource;
		other.m_resource = 0;
		
		return *this;
	}
	
	bool AudioSource::usesBuffer(const AudioBuffer& buffer) {
		ALint buffer_instance;
		
		alGetSourcei(this->m_resource, AL_BUFFER, &buffer_instance);
		
		return (static_cast<ALuint>(buffer_instance) == buffer.m_resource);
	}
	
	void AudioSource::setBuffer(const lge::AudioBuffer& buffer) {
		alSourcei(this->m_resource, AL_BUFFER, buffer.m_resource);
	}
	
	void AudioSource::queueBuffer(const lge::AudioBuffer& buffer) {
		alSourceQueueBuffers(this->m_resource, 1, &(buffer.m_resource));
	}
	
	void AudioSource::unqueueBuffer(const lge::AudioBuffer& buffer) {
		ALuint resource = buffer.m_resource;
		
		alSourceUnqueueBuffers(this->m_resource, 1, &resource);
	}
	
	void AudioSource::play() {
		alSourcePlay(this->m_resource);
	}
	
	void AudioSource::stop() {
		alSourceStop(this->m_resource);
	}
	
	void AudioSource::rewind() {
		alSourceRewind(this->m_resource);
	}
	
	void AudioSource::pause() {
		alSourcePause(this->m_resource);
	}
	
	bool AudioSource::isRelative() const {
		ALint value = AL_FALSE;
		
		alGetSourcei(this->m_resource, AL_SOURCE_RELATIVE, &value);
		
		return (value == AL_TRUE);
	}
	
	void AudioSource::setRelative(bool relative) {
		alSourcei(this->m_resource, AL_SOURCE_RELATIVE, relative? AL_TRUE : AL_FALSE);
	}
	
	float AudioSource::getInnerAngle() const {
		float angle = 360.0f;
		
		alGetSourcef(this->m_resource, AL_CONE_INNER_ANGLE, &angle);
		
		return angle;
	}
	
	void AudioSource::setInnerAngle(float angle) {
		alSourcef(this->m_resource, AL_CONE_INNER_ANGLE, angle);
	}
	
	float AudioSource::getOuterAngle() const {
		float angle = 360.0f;
		
		alGetSourcef(this->m_resource, AL_CONE_OUTER_ANGLE, &angle);
		
		return angle;
	}
	
	void AudioSource::setOuterAngle(float angle) {
		alSourcef(this->m_resource, AL_CONE_OUTER_ANGLE, angle);
	}
	
	float AudioSource::getPitch() const {
		float pitch = 1.0f;
		
		alGetSourcef(this->m_resource, AL_PITCH, &pitch);
		
		return pitch;
	}
	
	void AudioSource::setPitch(float pitch) {
		alSourcef(this->m_resource, AL_PITCH, pitch);
	}
	
	vec3 AudioSource::getPosition() const {
		vec3 position;
		
		alGetSourcefv(this->m_resource, AL_POSITION, position.data());
		
		return position;
	}
	
	void AudioSource::setPosition(const vec3& position) {
		alSourcefv(this->m_resource, AL_POSITION, position.data());
	}
	
	vec3 AudioSource::getDirection() const {
		vec3 direction;
		
		alGetSourcefv(this->m_resource, AL_DIRECTION, direction.data());
		
		return direction;
	}
	
	void AudioSource::setDirection(const vec3& direction) {
		alSourcefv(this->m_resource, AL_DIRECTION, direction.data());
	}
	
	vec3 AudioSource::getVelocity() const {
		vec3 velocity;
		
		alGetSourcefv(this->m_resource, AL_VELOCITY, velocity.data());
		
		return velocity;
	}
	
	void AudioSource::setVelocity(const lge::vec3& velocity) {
		alSourcefv(this->m_resource, AL_VELOCITY, velocity.data());
	}
	
	bool AudioSource::isLooping() const {
		ALint value = AL_FALSE;
		
		alGetSourcei(this->m_resource, AL_LOOPING, &value);
		
		return (value == AL_TRUE);
	}
	
	void AudioSource::setLooping(bool looping) {
		alSourcei(this->m_resource, AL_LOOPING, looping? AL_TRUE : AL_FALSE);
	}
	
	float AudioSource::getGain() const {
		float gain = 1.0f;
		
		alGetSourcef(this->m_resource, AL_GAIN, &gain);
		
		return gain;
	}
	
	void AudioSource::setGain(float gain) {
		alSourcef(this->m_resource, AL_GAIN, gain);
	}
	
	float AudioSource::getMinGain() const {
		float gain = 0.0f;
		
		alGetSourcef(this->m_resource, AL_MIN_GAIN, &gain);
		
		return gain;
	}
	
	void AudioSource::setMinGain(float gain) {
		alSourcef(this->m_resource, AL_MIN_GAIN, gain);
	}
	
	float AudioSource::getMaxGain() const {
		float gain = 1.0f;
		
		alGetSourcef(this->m_resource, AL_MAX_GAIN, &gain);
		
		return gain;
	}
	
	void AudioSource::setMaxGain(float gain) {
		alSourcef(this->m_resource, AL_MAX_GAIN, gain);
	}
	
	AudioSourceState AudioSource::getState() const {
		ALint value;
		
		alGetSourcei(this->m_resource, AL_SOURCE_STATE, &value);
		
		switch (value) {
			case AL_PLAYING:
				return AudioSourceState::PLAYING;
			case AL_PAUSED:
				return AudioSourceState::PAUSED;
			case AL_STOPPED:
				return AudioSourceState::STOPPED;
			default:
				return AudioSourceState::INITIAL;
		}
	}
	
	int AudioSource::countQueuedBuffers() const {
		ALint count = 0;
		
		alGetSourcei(this->m_resource, AL_BUFFERS_QUEUED, &count);
		
		return count;
	}
	
	int AudioSource::countProcessedBuffers() const {
		ALint count = 0;
		
		alGetSourcei(this->m_resource, AL_BUFFERS_PROCESSED, &count);
		
		return count;
	}
	
	float AudioSource::getReferenceDistance() const {
		float distance = 1.0f;
		
		alGetSourcef(this->m_resource, AL_REFERENCE_DISTANCE, &distance);
		
		return distance;
	}
	
	void AudioSource::setReferenceDistance(float distance) {
		alSourcef(this->m_resource, AL_REFERENCE_DISTANCE, distance);
	}
	
	float AudioSource::getRolloffFactor() const {
		float factor = 1.0f;
		
		alGetSourcef(this->m_resource, AL_ROLLOFF_FACTOR, &factor);
		
		return factor;
	}
	
	void AudioSource::setRolloffFactor(float factor) {
		alSourcef(this->m_resource, AL_ROLLOFF_FACTOR, factor);
	}
	
	float AudioSource::getOuterGain() const {
		float gain = 0.0f;
		
		alGetSourcef(this->m_resource, AL_CONE_OUTER_GAIN, &gain);
		
		return gain;
	}
	
	void AudioSource::setOuterGain(float gain) {
		alSourcef(this->m_resource, AL_CONE_OUTER_GAIN, gain);
	}
	
	float AudioSource::getMaxDistance() const {
		float distance = std::numeric_limits< float >::infinity();
		
		alGetSourcef(this->m_resource, AL_MAX_DISTANCE, &distance);
		
		return distance;
	}
	
	void AudioSource::setMaxDistance(float distance) {
		alSourcef(this->m_resource, AL_MAX_DISTANCE, distance);
	}
	
	float AudioSource::getTimeOffset() const {
		float offset = 0.0f;
		
		alGetSourcef(this->m_resource, AL_SEC_OFFSET, &offset);
		
		return offset;
	}
	
	void AudioSource::setTimeOffset(float offset) {
		alSourcef(this->m_resource, AL_SEC_OFFSET, offset);
	}
	
	int AudioSource::getSampleOffset() const {
		int offset = 0;
		
		alGetSourcei(this->m_resource, AL_SAMPLE_OFFSET, &offset);
		
		return offset;
	}
	
	void AudioSource::setSampleOffset(int offset) {
		alSourcei(this->m_resource, AL_SAMPLE_OFFSET, offset);
	}
	
	AudioSourceType AudioSource::getType() const {
		ALint value;
		
		alGetSourcei(this->m_resource, AL_SOURCE_TYPE, &value);
		
		switch (value) {
			case AL_STATIC:
				return AudioSourceType::STATIC;
			case AL_STREAMING:
				return AudioSourceType::STREAMING;
			default:
				return AudioSourceType::UNDETERMINED;
		}
	}
}

#endif
