//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Context.hpp"

#include "../memory/move.hxx"
#include "../system/assert.hxx"

namespace lge {
	
	AudioContext::AudioContext(AudioDevice& device) {
		this->m_resource = alcCreateContext(device.m_resource, nullptr);
	}
	
	AudioContext::AudioContext(lge::AudioContext&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
	}
	
	AudioContext::~AudioContext() {
		if (this->m_resource != nullptr) {
			if (this->m_resource == alcGetCurrentContext()) {
				alcMakeContextCurrent(nullptr);
			}
			
			alcDestroyContext(this->m_resource);
		}
	}
	
	AudioContext& AudioContext::operator=(lge::AudioContext&& other) noexcept {
		if (this->m_resource != nullptr) {
			alcDestroyContext(this->m_resource);
		}
		
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	bool AudioContext::makeCurrent() {
		return alcMakeContextCurrent(this->m_resource);
	}
	
	void AudioContext::process() {
		alcProcessContext(this->m_resource);
	}
	
	void AudioContext::suspend() {
		alcSuspendContext(this->m_resource);
	}
	
	int AudioContext::getFrequency() const {
		int frequency;
		
		alcGetIntegerv(alcGetContextsDevice(this->m_resource), ALC_FREQUENCY, 1, &frequency);
		
		return frequency;
	}
	
	int AudioContext::getRefreshRate() const {
		int refreshRate;
		
		alcGetIntegerv(alcGetContextsDevice(this->m_resource), ALC_REFRESH, 1, &refreshRate);
		
		return refreshRate;
	}
	
	bool AudioContext::useSync() const {
		int value;
		
		alcGetIntegerv(alcGetContextsDevice(this->m_resource), ALC_SYNC, 1, &value);
		
		return (value == ALC_TRUE);
	}
	
	int AudioContext::countMonoSources() const {
		int count;
		
		alcGetIntegerv(alcGetContextsDevice(this->m_resource), ALC_MONO_SOURCES, 1, &count);
		
		return count;
	}
	
	int AudioContext::countStereoSources() const {
		int count;
		
		alcGetIntegerv(alcGetContextsDevice(this->m_resource), ALC_STEREO_SOURCES, 1, &count);
		
		return count;
	}
}

#endif
