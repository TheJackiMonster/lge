//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Device.hpp"

#include "../memory/move.hxx"

namespace lge {
	
	AudioDevice::AudioDevice() {
		this->m_resource = alcOpenDevice(nullptr);
	}
	
	AudioDevice::AudioDevice(const string& name) {
		this->m_resource = alcOpenDevice(name.c_str());
	}
	
	AudioDevice::AudioDevice(lge::AudioDevice&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
	}
	
	AudioDevice::~AudioDevice() {
		if (this->m_resource != nullptr) {
			alcCloseDevice(this->m_resource);
		}
	}
	
	AudioDevice& AudioDevice::operator=(lge::AudioDevice&& other) noexcept {
		if (this->m_resource != nullptr) {
			alcCloseDevice(this->m_resource);
		}
		
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	string AudioDevice::getName() const {
		return string(alcGetString(this->m_resource, ALC_DEVICE_SPECIFIER));
	}
	
	vector< string > AudioDevice::listDevices() {
		static vector< string > devices;
		
		if (devices.empty()) {
			const char* current = alcGetString(nullptr, ALC_DEVICE_SPECIFIER);
			
			while (*current != '\0') {
				if (*(current++) != '\0') {
					string name(current - 1);
					
					devices.push_back(name);
					
					current += name.length();
				} else {
					break;
				}
			}
		}
		
		return devices;
	}
}

#endif
