#pragma once

//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "../lang/string.hxx"
#include "../memory/vector.hxx"

#include <AL/alc.h>

namespace lge {
	
	/** @defgroup al_group The OpenAL Group
	 *  A group for classes and structures representing OpenAL functionality.
	 *  @{
	 */
	
	class AudioDevice {
		friend class AudioContext;
	
	private:
		ALCdevice* m_resource;
	
	public:
		explicit AudioDevice();
		
		explicit AudioDevice(const string& name);
		
		AudioDevice(const AudioDevice& other) = delete;
		
		AudioDevice(AudioDevice&& other) noexcept;
		
		~AudioDevice();
		
		AudioDevice& operator=(const AudioDevice& other) = delete;
		
		AudioDevice& operator=(AudioDevice&& other) noexcept;
		
		[[nodiscard]]
		string getName() const;
		
		static vector< string > listDevices();
		
	};
	
	/** @} */
	
}

#endif
