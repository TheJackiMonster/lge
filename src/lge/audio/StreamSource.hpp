#pragma once

//
// Created by thejackimonster on 31.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "CaptureStream.hpp"
#include "Source.hpp"

namespace lge {
	
	/** @addtogroup al_group
	 *  @{
	 */
	
	class AudioStreamSource {
	private:
		AudioSource m_source;
		vector< AudioBuffer > m_buffers;
		uchar m_buffer_state;
	
	public:
		explicit AudioStreamSource(uint buffer_count = 2);
		
		AudioStreamSource(const AudioStreamSource& other) = delete;
		
		AudioStreamSource(AudioStreamSource&& other) = default;
		
		~AudioStreamSource() = default;
		
		AudioStreamSource& operator=(const AudioStreamSource& other) = delete;
		
		AudioStreamSource& operator=(AudioStreamSource&& other) = default;
		
		AudioSource& getSource();
		
		[[nodiscard]]
		const AudioSource& getSource() const;
		
		void update(bool play);
		
		void push(AudioCaptureStream& stream);
		
	};
	
	/** @} */
	
}

#endif
