#pragma once

//
// Created by thejackimonster on 08.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Buffer.hpp"
#include "vec.hxx"

#include <AL/al.h>

namespace lge {
	
	/** @addtogroup al_group
	 *  @{
	 */
	
	enum class AudioSourceState : uchar {
		INITIAL = 0,
		
		PLAYING = 1,
		PAUSED = 2,
		STOPPED = 3
	};
	
	enum class AudioSourceType : uchar {
		UNDETERMINED = 0,
		
		STATIC = 2,
		STREAMING = 3
	};
	
	class AudioSource {
	private:
		ALuint m_resource;
	
	public:
		explicit AudioSource();
		
		AudioSource(const AudioSource& other) = delete;
		
		AudioSource(AudioSource&& other) noexcept;
		
		~AudioSource();
		
		AudioSource& operator=(const AudioSource& other) = delete;
		
		AudioSource& operator=(AudioSource&& other) noexcept;
		
		bool usesBuffer(const AudioBuffer& buffer);
		
		void setBuffer(const AudioBuffer& buffer);
		
		void queueBuffer(const AudioBuffer& buffer);
		
		void unqueueBuffer(const AudioBuffer& buffer);
		
		void play();
		
		void stop();
		
		void rewind();
		
		void pause();
		
		[[nodiscard]]
		bool isRelative() const;
		
		void setRelative(bool relative);
		
		[[nodiscard]]
		float getInnerAngle() const;
		
		void setInnerAngle(float angle);
		
		[[nodiscard]]
		float getOuterAngle() const;
		
		void setOuterAngle(float angle);
		
		[[nodiscard]]
		float getPitch() const;
		
		void setPitch(float pitch);
		
		[[nodiscard]]
		vec3 getPosition() const;
		
		void setPosition(const vec3& position);
		
		[[nodiscard]]
		vec3 getDirection() const;
		
		void setDirection(const vec3& direction);
		
		[[nodiscard]]
		vec3 getVelocity() const;
		
		void setVelocity(const vec3& velocity);
		
		[[nodiscard]]
		bool isLooping() const;
		
		void setLooping(bool looping);
		
		[[nodiscard]]
		float getGain() const;
		
		void setGain(float gain);
		
		[[nodiscard]]
		float getMinGain() const;
		
		void setMinGain(float gain);
		
		[[nodiscard]]
		float getMaxGain() const;
		
		void setMaxGain(float gain);
		
		[[nodiscard]]
		AudioSourceState getState() const;
		
		[[nodiscard]]
		int countQueuedBuffers() const;
		
		[[nodiscard]]
		int countProcessedBuffers() const;
		
		[[nodiscard]]
		float getReferenceDistance() const;
		
		void setReferenceDistance(float distance);
		
		[[nodiscard]]
		float getRolloffFactor() const;
		
		void setRolloffFactor(float factor);
		
		[[nodiscard]]
		float getOuterGain() const;
		
		void setOuterGain(float gain);
		
		[[nodiscard]]
		float getMaxDistance() const;
		
		void setMaxDistance(float distance);
		
		[[nodiscard]]
		float getTimeOffset() const;
		
		void setTimeOffset(float offset);
		
		[[nodiscard]]
		int getSampleOffset() const;
		
		void setSampleOffset(int offset);
		
		[[nodiscard]]
		AudioSourceType getType() const;
		
	};
	
	/** @} */
	
}

#endif
