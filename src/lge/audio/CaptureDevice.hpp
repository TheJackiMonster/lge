#pragma once

//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "../lang/string.hxx"
#include "../memory/vector.hxx"
#include "Format.hpp"

#include <AL/alc.h>

namespace lge {
	
	/** @addtogroup al_group
	 *  @{
	 */
	
	class AudioCaptureDevice {
	private:
		const uint m_frequency;
		const AudioFormat m_format;
		const int m_samples;
		
		ALCdevice* m_resource;
		
		bool m_capturing;
	
	public:
		explicit AudioCaptureDevice(uint frequency, AudioFormat format, int samples);
		
		explicit AudioCaptureDevice(const string& name, uint frequency, AudioFormat format, int samples);
		
		AudioCaptureDevice(const AudioCaptureDevice& other) = delete;
		
		AudioCaptureDevice(AudioCaptureDevice&& other) noexcept;
		
		~AudioCaptureDevice();
		
		AudioCaptureDevice& operator=(const AudioCaptureDevice& other) = delete;
		
		AudioCaptureDevice& operator=(AudioCaptureDevice&& other) noexcept;
		
		[[nodiscard]]
		const AudioFormat getFormat() const;
		
		[[nodiscard]]
		int getFrequency() const;
		
		[[nodiscard]]
		int getBits() const;
		
		[[nodiscard]]
		int getChannels() const;
		
		[[nodiscard]]
		int getSize() const;
		
		[[nodiscard]]
		bool isCapturing() const;
		
		void start();
		
		void stop();
		
		[[nodiscard]]
		int maxSamples() const;
		
		[[nodiscard]]
		int availableSamples() const;
		
		void capture(uchar* data, int samples);
		
		[[nodiscard]]
		string getName() const;
		
		static vector< string > listDevices();
	};
	
	/** @} */
	
}

#endif
