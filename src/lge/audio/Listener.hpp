#pragma once

//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "vec.hxx"

namespace lge {
	
	/** @addtogroup al_group
	 *  @{
	 */
	
	class AudioListener {
	private:
		explicit AudioListener() = default;
	
	public:
		AudioListener(const AudioListener& other) = delete;
		
		AudioListener(AudioListener&& other) = delete;
		
		~AudioListener() = default;
		
		AudioListener& operator=(const AudioListener& other) = delete;
		
		AudioListener& operator=(AudioListener&& other) = delete;
		
		[[nodiscard]]
		vec3 getPosition() const;
		
		void setPosition(const vec3& position);
		
		[[nodiscard]]
		vec3 getVelocity() const;
		
		void setVelocity(const vec3& velocity);
		
		[[nodiscard]]
		float getGain() const;
		
		void setGain(float gain);
		
		void getOrientation(vec3& at, vec3& up);
		
		void setOrientation(const vec3& at, const vec3& up);
		
		static AudioListener& getListener();
	};
	
	/** @} */
	
}

#endif
