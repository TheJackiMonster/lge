//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Buffer.hpp"

#include "../memory/move.hxx"

namespace lge {
	
	AudioBuffer::AudioBuffer() {
		alGenBuffers(1, &(this->m_resource));
	}
	
	AudioBuffer::AudioBuffer(lge::AudioBuffer&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
	}
	
	AudioBuffer::~AudioBuffer() {
		if (this->m_resource != 0) {
			alDeleteBuffers(1, &(this->m_resource));
		}
	}
	
	AudioBuffer& AudioBuffer::operator=(lge::AudioBuffer&& other) noexcept {
		if (this->m_resource != 0) {
			alDeleteBuffers(1, &(this->m_resource));
		}
		
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	void AudioBuffer::setData(int frequency, lge::AudioFormat format, const lge::uchar* data, int samples) {
		const int size = getFormatSize(format, samples);
		
		alBufferData(this->m_resource, getFormatValue(format), reinterpret_cast<const ALvoid*>(data), size, frequency);
	}
	
	int AudioBuffer::getFrequency() const {
		int frequency;
		
		alGetBufferi(this->m_resource, AL_FREQUENCY, &frequency);
		
		return frequency;
	}
	
	int AudioBuffer::getBits() const {
		int bits;
		
		alGetBufferi(this->m_resource, AL_BITS, &bits);
		
		return bits;
	}
	
	int AudioBuffer::getChannels() const {
		int channels;
		
		alGetBufferi(this->m_resource, AL_CHANNELS, &channels);
		
		return channels;
	}
	
	int AudioBuffer::getSize() const {
		int size;
		
		alGetBufferi(this->m_resource, AL_SIZE, &size);
		
		return size;
	}
}

#endif
