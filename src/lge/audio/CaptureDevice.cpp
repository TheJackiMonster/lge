//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "CaptureDevice.hpp"

#include "../memory/move.hxx"

namespace lge {
	
	AudioCaptureDevice::AudioCaptureDevice(uint frequency, AudioFormat format, int samples)
			:
			m_frequency(frequency),
			m_format(format),
			m_samples(samples),
			m_capturing(false) {
		this->m_resource = alcCaptureOpenDevice(nullptr, this->m_frequency, getFormatValue(this->m_format), getSize());
	}
	
	AudioCaptureDevice::AudioCaptureDevice(const string& name, uint frequency, AudioFormat format, int samples)
			:
			m_frequency(frequency),
			m_format(format),
			m_samples(samples),
			m_capturing(false) {
		this->m_resource = alcCaptureOpenDevice(name.c_str(), this->m_frequency, getFormatValue(this->m_format),
												getSize());
	}
	
	AudioCaptureDevice::AudioCaptureDevice(lge::AudioCaptureDevice&& other) noexcept
			:
			m_frequency(other.m_frequency),
			m_format(other.m_format),
			m_samples(other.m_samples),
			m_capturing(other.m_capturing) {
		move_r(this->m_resource, other.m_resource);
	}
	
	AudioCaptureDevice::~AudioCaptureDevice() {
		if (this->m_resource != nullptr) {
			alcCaptureCloseDevice(this->m_resource);
		}
	}
	
	AudioCaptureDevice& AudioCaptureDevice::operator=(lge::AudioCaptureDevice&& other) noexcept {
		if (this->m_resource != nullptr) {
			alcCaptureCloseDevice(this->m_resource);
		}
		
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	const AudioFormat AudioCaptureDevice::getFormat() const {
		return this->m_format;
	}
	
	int AudioCaptureDevice::getFrequency() const {
		return this->m_frequency;
	}
	
	int AudioCaptureDevice::getBits() const {
		return getFormatBits(this->m_format);
	}
	
	int AudioCaptureDevice::getChannels() const {
		return getFormatChannels(this->m_format);
	}
	
	int AudioCaptureDevice::getSize() const {
		return getFormatSize(this->m_format, this->m_samples);
	}
	
	bool AudioCaptureDevice::isCapturing() const {
		return this->m_capturing;
	}
	
	void AudioCaptureDevice::start() {
		alcCaptureStart(this->m_resource);
		
		this->m_capturing = true;
	}
	
	void AudioCaptureDevice::stop() {
		alcCaptureStop(this->m_resource);
		
		this->m_capturing = false;
	}
	
	int AudioCaptureDevice::maxSamples() const {
		return this->m_samples;
	}
	
	int AudioCaptureDevice::availableSamples() const {
		int samples = 0;
		
		alcGetIntegerv(this->m_resource, ALC_CAPTURE_SAMPLES, 1, &samples);
		
		return samples;
	}
	
	void AudioCaptureDevice::capture(lge::uchar* data, int samples) {
		alcCaptureSamples(this->m_resource, reinterpret_cast<ALCvoid*>(data), samples);
	}
	
	string AudioCaptureDevice::getName() const {
		return string(alcGetString(this->m_resource, ALC_CAPTURE_DEVICE_SPECIFIER));
	}
	
	vector< string > AudioCaptureDevice::listDevices() {
		static vector< string > devices;
		
		if (devices.empty()) {
			const char* current = alcGetString(nullptr, ALC_CAPTURE_DEVICE_SPECIFIER);
			
			while (*current != '\0') {
				if (*(current++) != '\0') {
					string name(current - 1);
					
					devices.push_back(name);
					
					current += name.length();
				} else {
					break;
				}
			}
		}
		
		return devices;
	}
}

#endif
