#pragma once

//
// Created by thejackimonster on 30.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Buffer.hpp"
#include "CaptureDevice.hpp"

namespace lge {
	
	/** @addtogroup al_group
	 *  @{
	 */
	
	class AudioCaptureStream {
	private:
		AudioCaptureDevice m_device;
		vector< uchar > m_buffer;
		int m_sample_offset;
	
	public:
		explicit AudioCaptureStream(uint frequency, AudioFormat format, uint max_framerate = 60);
		
		explicit AudioCaptureStream(const string& name, uint frequency, AudioFormat format, uint max_framerate = 60);
		
		AudioCaptureStream(const AudioCaptureStream& other) = delete;
		
		AudioCaptureStream(AudioCaptureStream&& other) = default;
		
		~AudioCaptureStream() = default;
		
		AudioCaptureStream& operator=(const AudioCaptureStream& other) = delete;
		
		AudioCaptureStream& operator=(AudioCaptureStream&& other) = default;
		
		AudioCaptureDevice& getDevice();
		
		[[nodiscard]]
		const AudioCaptureDevice& getDevice() const;
		
		void update(bool capture);
		
		bool pull(AudioBuffer& buffer);
		
	};
	
	/** @} */
	
}

#endif
