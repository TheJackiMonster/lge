//
// Created by thejackimonster on 31.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "StreamSource.hpp"

namespace lge {
	
	AudioStreamSource::AudioStreamSource(uint buffer_count) {
		this->m_buffers.resize(buffer_count);
		this->m_buffer_state = 0;
	}
	
	AudioSource& AudioStreamSource::getSource() {
		return this->m_source;
	}
	
	const AudioSource& AudioStreamSource::getSource() const {
		return this->m_source;
	}
	
	void AudioStreamSource::update(bool play) {
		const AudioSourceState state = this->m_source.getState();
		
		if (play != (state == AudioSourceState::PLAYING)) {
			if (play) {
				if (this->m_source.countQueuedBuffers() > 0) {
					this->m_source.play();
				}
			} else {
				this->m_source.pause();
			}
		}
	}
	
	void AudioStreamSource::push(AudioCaptureStream& stream) {
		if ((this->m_source.countProcessedBuffers() > 0) &&
			((this->m_buffer_state % 2) == 1)) {
			this->m_buffer_state = (this->m_buffer_state + 1) % (2 * this->m_buffers.size());
			
			lge::AudioBuffer& buffer = this->m_buffers[this->m_buffer_state / 2];
			
			this->m_source.unqueueBuffer(buffer);
		}
		
		if ((this->m_buffer_state % 2) == 0) {
			lge::AudioBuffer& buffer = this->m_buffers[this->m_buffer_state / 2];
			
			if (stream.pull(buffer)) {
				this->m_buffer_state++;
				
				if (this->m_source.getState() == lge::AudioSourceState::INITIAL) {
					this->m_buffer_state++;
				}
				
				this->m_source.queueBuffer(buffer);
			}
		}
	}
}

#endif
