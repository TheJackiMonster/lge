//
// Created by thejackimonster on 09.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENAL)

#include "Listener.hpp"

#include <AL/al.h>

namespace lge {
	
	vec3 AudioListener::getPosition() const {
		vec3 position;
		
		alGetListenerfv(AL_POSITION, position.data());
		
		return position;
	}
	
	void AudioListener::setPosition(const vec3& position) {
		alListenerfv(AL_POSITION, position.data());
	}
	
	vec3 AudioListener::getVelocity() const {
		vec3 velocity;
		
		alGetListenerfv(AL_VELOCITY, velocity.data());
		
		return velocity;
	}
	
	void AudioListener::setVelocity(const vec3& velocity) {
		alListenerfv(AL_VELOCITY, velocity.data());
	}
	
	float AudioListener::getGain() const {
		float gain = 1.0f;
		
		alGetListenerf(AL_GAIN, &gain);
		
		return gain;
	}
	
	void AudioListener::setGain(float gain) {
		alListenerf(AL_GAIN, gain);
	}
	
	void AudioListener::getOrientation(lge::vec3& at, lge::vec3& up) {
		float data[6];
		
		alGetListenerfv(AL_ORIENTATION, data);
		
		at[0] = data[0];
		at[1] = data[1];
		at[2] = data[2];
		
		up[0] = data[3];
		up[1] = data[4];
		up[2] = data[5];
	}
	
	void AudioListener::setOrientation(const vec3& at, const vec3& up) {
		float data[6] = {
				at[0], at[1], at[2],
				up[0], up[1], up[2]
		};
		
		alListenerfv(AL_ORIENTATION, data);
	}
	
	AudioListener& AudioListener::getListener() {
		static AudioListener listener;
		return listener;
	}
}

#endif
