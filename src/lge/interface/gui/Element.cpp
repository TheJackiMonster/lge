//
// Created by thejackimonster on 20.07.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Element.hpp"

namespace lge {
	
	GuiElement::GuiElement() {
	}
	
	GuiElement::GuiElement(const GuiElement& other) {
	}
	
	GuiElement::GuiElement(GuiElement&& other) noexcept {
	}
	
	GuiElement::~GuiElement() {
	}
	
	GuiElement& GuiElement::operator=(const GuiElement& other) {
		return *this;
	}
	
	GuiElement& GuiElement::operator=(GuiElement&& other) noexcept {
		return *this;
	}
	
	bool GuiElement::isVisible() const {
		return this->m_visible;
	}
	
	void GuiElement::setVisible(bool visible) {
		this->m_visible = visible;
	}
	
	const vec2& GuiElement::getPosition() const {
		return this->m_sprite.getPosition();
	}
	
	const vec2& GuiElement::getSize() const {
		return this->m_sprite.getSize();
	}
}
