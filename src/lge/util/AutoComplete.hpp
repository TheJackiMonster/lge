#pragma once

//
// Created by thejackimonster on 19.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/primitives.hxx"
#include "../lang/string.hxx"
#include "../memory/vector.hxx"

namespace lge {
	
	class AutoComplete {
	private:
		enum NodeFlags : ubyte {
			END = 0x1,
			DEL = 0x2
		};
		
		struct Node {
		private:
			AutoComplete& m_autoComplete;
			
			const char m_value;
			NodeFlags m_flags;
			
			Node* m_left;
			Node* m_next;
			Node* m_right;
		
		public:
			explicit Node(AutoComplete& autoComplete, char value);
			
			Node(const Node& other) = delete;
			
			Node(Node&& other) = delete;
			
			~Node() = default;
			
			Node& operator=(const Node& other) = delete;
			
			Node& operator=(Node&& other) = delete;
			
			bool containsWord(const string& word, uint index) const;
			
			void addWord(const string& word, uint index);
			
			void removeWord(const string& word, uint index);
			
			void getCompletions(const string& prefix, vector< string >& completions, uint index);
			
			void setEnding(bool ending);
			
			bool isEnding() const;
			
			bool canBeDeleted() const;
		};
		
		vector< Node > m_nodes;
	
	public:
		explicit AutoComplete();
		
		AutoComplete(const AutoComplete& other);
		
		AutoComplete(AutoComplete&& other) noexcept;
		
		~AutoComplete();
		
		AutoComplete& operator=(const AutoComplete& other);
		
		AutoComplete& operator=(AutoComplete&& other) noexcept;
		
		bool containsWord(const string& word);
		
		void addWord(const string& word);
		
		void removeWord(const string& word);
		
		void getCompletions(const string& prefix, vector< string >& completions);
	};
}
