//
// Created by thejackimonster on 01.07.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../memory/vector.hxx"
#include "algorithm.hxx"
#include "functional.hxx"

namespace lge {
	
	/**
	 * Basic <b>function</b> with given types for its parameters and
	 * <b>void</b> as its return-type.
	 *
	 * @author TheJackiMonster
	 * @since 01.07.2019
	 *
	 * @tparam T <i>(list of types for the parameters of the event-function)</i>
	 */
	template < typename... T >
	struct event_function {
		typedef function< void(T...) > type;
	};
	
	/**
	 * A template for handling events and calling multiple functions
	 * if necessary to react to specific events.
	 *
	 * @author TheJackiMonster
	 * @since 01.07.2019
	 *
	 * @tparam T <i>(list of types for the parameters of the event-function)</i>
	 */
	template < typename... T >
	struct event {
	private:
		vector< typename event_function< T... >::type > m_handles;
	
	public:
		event() = default;
		
		event(const event& other) = delete;
		
		event(event&& other) = delete;
		
		~event() = default;
		
		event& operator=(const event& other) = delete;
		
		event& operator=(event&& other) = delete;
		
		/**
		 * Calls all the functions which are bound to this event
		 * with the given parameters.
		 *
		 * @param args <i>(parameters for the event)</i>
		 */
		void operator()(T... args) {
			for (auto& handle : this->m_handles) {
				handle(args...);
			}
		}
		
		/**
		 * Binding a function to the event which gets called
		 * with the specific parameters if the event gets triggered.
		 *
		 * @param handle <i>(of function)</i>
		 * @return reference to itself
		 */
		event& operator+=(typename event_function< T... >::type handle) {
			this->m_handles.push_back(handle);
			
			return *this;
		}
		
		/**
		 * Unbinding a function from the event.
		 *
		 * @param handle <i>(of function)</i>
		 * @return reference to itself
		 */
		event& operator-=(typename event_function< T... >::type handle) {
			this->m_handles.erase(
					remove(this->m_handles.begin(), this->m_handles.end(), handle),
					this->m_handles.end()
			);
			
			return *this;
		}
		
	};
}
