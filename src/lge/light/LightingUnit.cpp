//
// Created by thejackimonster on 22.01.18.
//
// Copyright (c) 2018-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "LightingUnit.hpp"

namespace lge {
	
	bool LightingUnit::add(const DirectionalLight& light) {
		for (ulong i = this->m_directionals.size(); i > 0; i--) {
			if (this->m_directionals[i - 1] == &light) {
				return false;
			}
		}
		
		this->m_directionals.push_back(&light);
		return true;
	}
	
	bool LightingUnit::add(const PointLight& light) {
		for (ulong i = this->m_points.size(); i > 0; i--) {
			if (this->m_points[i - 1] == &light) {
				return false;
			}
		}
		
		this->m_points.push_back(&light);
		return true;
	}
	
	bool LightingUnit::add(const SpotLight& light) {
		for (ulong i = this->m_spots.size(); i > 0; i--) {
			if (this->m_spots[i - 1] == &light) {
				return false;
			}
		}
		
		this->m_spots.push_back(&light);
		return true;
	}
	
	bool LightingUnit::remove(const DirectionalLight& light) {
		for (ulong i = this->m_directionals.size(); i > 0; i--) {
			if (this->m_directionals[i - 1] == &light) {
				this->m_directionals.erase(this->m_directionals.begin() + (i - 1));
				return true;
			}
		}
		
		return false;
	}
	
	bool LightingUnit::remove(const PointLight& light) {
		for (ulong i = this->m_points.size(); i > 0; i--) {
			if (this->m_points[i - 1] == &light) {
				this->m_points.erase(this->m_points.begin() + (i - 1));
				return true;
			}
		}
		
		return false;
	}
	
	bool LightingUnit::remove(const SpotLight& light) {
		for (ulong i = this->m_spots.size(); i > 0; i--) {
			if (this->m_spots[i - 1] == &light) {
				this->m_spots.erase(this->m_spots.begin() + (i - 1));
				return true;
			}
		}
		
		return false;
	}
}
