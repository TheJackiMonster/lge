#pragma once

//
// Created by thejackimonster on 22.01.18.
//
// Copyright (c) 2018-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "DirectionalLight.hpp"
#include "PointLight.hpp"
#include "SpotLight.hpp"

#include "../graphics/ShaderProgram.hpp"

namespace lge {
	
	class LightingUnit {
	private:
		vector< const DirectionalLight* > m_directionals;
		vector< const PointLight* > m_points;
		vector< const SpotLight* > m_spots;
	
	public:
		LightingUnit() = default;
		
		LightingUnit(const LightingUnit& other) = delete;
		
		LightingUnit(LightingUnit&& other) = delete;
		
		~LightingUnit() = default;
		
		LightingUnit& operator=(const LightingUnit& other) = delete;
		
		LightingUnit& operator=(LightingUnit&& other) = delete;
		
		bool add(const DirectionalLight& light);
		
		bool add(const PointLight& light);
		
		bool add(const SpotLight& light);
		
		bool remove(const DirectionalLight& light);
		
		bool remove(const PointLight& light);
		
		bool remove(const SpotLight& light);
	};
}
