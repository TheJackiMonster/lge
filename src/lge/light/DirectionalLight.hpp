#pragma once

//
// Created by thejackimonster on 14.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Light.hpp"

namespace lge {
	
	class DirectionalLight : Light {
	private:
		vec3 m_direction;
	
	public:
		explicit DirectionalLight() = default;
		
		explicit DirectionalLight(const vec3& direction, float brightness);
		
		explicit DirectionalLight(const vec3& direction, const vec3& color, float brightness);
		
		DirectionalLight(const DirectionalLight& other) = default;
		
		DirectionalLight(DirectionalLight&& other) = default;
		
		~DirectionalLight() override = default;
		
		DirectionalLight& operator=(const DirectionalLight& other) = default;
		
		DirectionalLight& operator=(DirectionalLight&& other) = default;
		
		void setDirection(const vec3& direction);
		
		const vec3& getDirection() const;
	};
}
