//
// Created by thejackimonster on 22.01.18.
//
// Copyright (c) 2018-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "SpotLight.hpp"

#include "../math/trigonometry.hxx"

namespace lge {
	
	SpotLight::SpotLight(const vec3& position, const vec3& direction, float brightness)
			:
			DirectionalLight(direction, brightness) {
		this->position() = position;
	}
	
	SpotLight::SpotLight(const vec3& position, const vec3& direction, const vec3& color, float brightness)
			:
			DirectionalLight(direction, color, brightness) {
		this->position() = position;
	}
	
	vec3& SpotLight::position() {
		return this->m_position;
	}
	
	const vec3& SpotLight::position() const {
		return this->m_position;
	}
	
	void SpotLight::setAngle(float radian) {
		this->m_limit = 1.0f - lge::cos< float >(radian);
	}
	
	float SpotLight::getAngle() const {
		return lge::acos< float >(1.0f - this->m_limit);
	}
}
