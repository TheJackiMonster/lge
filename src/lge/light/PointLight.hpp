#pragma once

//
// Created by thejackimonster on 14.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Light.hpp"

namespace lge {
	
	class PointLight : public Light {
	private:
		vec3 m_position;
	
	public:
		explicit PointLight() = default;
		
		explicit PointLight(const vec3& position, float brightness);
		
		explicit PointLight(const vec3& position, const vec3& color, float brightness);
		
		PointLight(const PointLight& other) = default;
		
		PointLight(PointLight&& other) = default;
		
		~PointLight() override = default;
		
		PointLight& operator=(const PointLight& other) = default;
		
		PointLight& operator=(PointLight&& other) = default;
		
		vec3& position();
		
		const vec3& position() const;
	};
}
