#pragma once
//
// Created by thejackimonster on 30.11.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/templates.hxx"
#include "../memory/move.hxx"
#include "../system/assert.hxx"
#include "../system/iostream.hxx"

namespace lge {
	
	/** @defgroup mat_group The Matrix Group
	 *  This group is for matrices, vectors and quaternions.
	 *  @{
	 */
	
	namespace __mat {
		
		template < typename T = float >
		static void __init(T* data, uint len, T def) {
			for (uint i = 0; i < len; i++) {
				*(data + i) = def;
			}
		}
		
		template < typename T = float, typename V, typename... D >
		static void __init(T* data, uint len, T def, V value, D... others) {
			if (len > 0) {
				*data = static_cast<T>(value);
				
				__init(data + 1, len - 1, *data, others...);
			}
		}
	}
	
	/**
	 * A template for using mathematical types like matrices, vectors and quaternions.
	 * @see vec
	 * @see quaternion
	 *
	 * @author TheJackiMonster
	 * @since 30.11.2017
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 */
	template < uint M, uint N = M, uint L = N, typename T = float >
	struct mat {
	private:
		typename enable_if< (L >= N), T >::type m_data[(M - 1) * L + N];
		
		constexpr T* at(const uint row, const uint column) {
			return (this->m_data + (row * L + column));
		}
		
		constexpr const T* at(const uint row, const uint column) const {
			return (this->m_data + (row * L + column));
		}
	
	public:
		/**
		 * Creates a matrix without assignment of any values.
		 */
		mat() {};
		
		/**
		 * Creates a matrix and assigns a list of values row-by-row to it
		 * <i>(the values will be casted to <b>T</b> automaticly)</i>.
		 *
		 * If there are less values assigned than <b>M</b>x<b>N</b> elements,
		 * the rest gets filled up with <b>zero</b>-values.
		 *
		 * @tparam D <i>(types of values)</i>
		 * @param[in] values
		 */
		template < typename... D >
		explicit mat(D... values) {
			__mat::__init< T >(this->m_data, (M - 1) * L + N, static_cast<T>(0), values...);
		}
		
		template < uint K = N, typename S = T >
		mat(const mat< M, N, K, S >& other) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) = other(i, j);
				}
			}
		}
		
		template < uint K = N >
		mat(mat< M, N, K, T >&& other) noexcept {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					move((*this)(i, j), other(i, j));
				}
			}
		}
		
		~mat() = default;
		
		/**
		 * Returns the amount of rows for this matrix.
		 *
		 * @return amount of rows
		 */
		constexpr uint rows() const {
			return M;
		}
		
		/**
		 * Returns the amount of columns for this matrix.
		 *
		 * @return amount of columns
		 */
		constexpr uint columns() const {
			return N;
		}
		
		/**
		 * Returns the amount of cells for this matrix.
		 *
		 * @return amount of cells
		 */
		constexpr uint size() const {
			return M * N;
		}
		
		/**
		 * Returns a pointer to cells of the matrix.
		 *
		 * @tparam S <i>(type of the pointer)</i>
		 * @return pointer to cells
		 */
		template < typename S = T >
		typename enable_if< (N == L), S* >::type data() {
			return this->m_data;
		}
		
		/**
		 * Returns a constant pointer to cells of the matrix.
		 *
		 * @tparam S <i>(type of the pointer)</i>
		 * @return constant pointer to cells
		 */
		template < typename S = T >
		typename enable_if< (N == L), const S* >::type data() const {
			return this->m_data;
		}
		
		/**
		 * Returns a reference to a cell of the matrix.
		 *
		 * @param[in] row <i>(of cell)</i>
		 * @param[in] column <i>(of cell)</i>
		 * @return reference to cell
		 */
		T& operator()(uint row, uint column) {
			return *(this->at(row, column));
		}
		
		/**
		 * Returns a constant reference to a cell of the matrix.
		 *
		 * @param[in] row <i>(of cell)</i>
		 * @param[in] column <i>(of cell)</i>
		 * @return constant reference to cell
		 */
		typename const_ref< T >::type operator()(uint row, uint column) const {
			return *(this->at(row, column));
		}
		
		template < uint K = N, typename S = T >
		mat& operator=(const mat< M, N, K, S >& other) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) = other(i, j);
				}
			}
			
			return *this;
		}
		
		template < uint K = N >
		mat& operator=(mat< M, N, K, T >&& other) noexcept {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					move((*this)(i, j), other(i, j));
				}
			}
			
			return *this;
		}
		
		/**
		 * Returns a reference to a cell of the matrix/vector.
		 *
		 * @tparam S <i>(type of cell)</i>
		 * @param[in] index <i>(of cell)</i>
		 * @return reference to cell
		 */
		template < typename S = T >
		typename enable_if< (N == 1), S& >::type operator[](uint index) {
			return (*this)(index, 0);
		}
		
		/**
		 * Returns a constant reference to a cell of the matrix/vector.
		 *
		 * @tparam S <i>(type of cell)</i>
		 * @param[in] index <i>(of cell)</i>
		 * @return constant reference to cell
		 */
		template < typename S = T >
		typename enable_if< (N == 1), typename const_ref< S >::type >::type operator[](uint index) const {
			return (*this)(index, 0);
		}
		
		/**
		 * Returns a reference to a specific field inside of this matrix
		 * as sub-matrix for a given size and with a specific offset.
		 *
		 * @tparam R <i>(amount of rows)</i>
		 * @tparam C <i>(amount of columns)</i>
		 * @param[in] row <i>(of offset)</i>
		 * @param[in] column <i>(of offset)</i>
		 * @return reference to sub-matrix
		 */
		template < uint R, uint C = R >
		mat< R, C, L, T >& field(uint row, uint column) {
			return *(reinterpret_cast<mat< R, C, L, T >*>(this->at(row, column)));
		}
		
		/**
		 * Returns a constant reference to a specific field inside of this matrix
		 * as sub-matrix for a given size and with a specific offset.
		 *
		 * @tparam R <i>(amount of rows)</i>
		 * @tparam C <i>(amount of columns)</i>
		 * @param[in] row <i>(of offset)</i>
		 * @param[in] column <i>(of offset)</i>
		 * @return constant reference to sub-matrix
		 */
		template < uint R, uint C = R >
		const mat< R, C, L, T >& field(uint row, uint column) const {
			return *(reinterpret_cast<const mat< R, C, L, T >*>(this->at(row, column)));
		}
		
		/**
		 * Returns a reference to a specific row of this matrix.
		 *
		 * @tparam C <i>(length of row)</i>
		 * @param[in] index <i>(of row)</i>
		 * @param[in] offset <i>(in elements)</i>
		 * @return reference to row
		 */
		template < uint C = N >
		mat< 1, C, L, T >& row(uint index, uint offset = 0) {
			return this->field< 1, C >(index, offset);
		}
		
		/**
		 * Returns a constant reference to a specific row of this matrix.
		 *
		 * @tparam C <i>(length of row)</i>
		 * @param[in] index <i>(of row)</i>
		 * @param[in] offset <i>(in elements)</i>
		 * @return constant reference to row
		 */
		template < uint C = N >
		const mat< 1, C, L, T >& row(uint index, uint offset = 0) const {
			return this->field< 1, C >(index, offset);
		}
		
		/**
		 * Returns a reference to a specific column of this matrix.
		 *
		 * @tparam R <i>(length of column)</i>
		 * @param[in] index <i>(of column)</i>
		 * @param[in] offset <i>(in elements)</i>
		 * @return reference to column
		 */
		template < uint R = M >
		mat< R, 1, L, T >& column(uint index, uint offset = 0) {
			return this->field< R, 1 >(offset, index);
		}
		
		/**
		 * Returns a constant reference to a specific column of this matrix.
		 *
		 * @tparam R <i>(length of column)</i>
		 * @param[in] index <i>(of column)</i>
		 * @param[in] offset <i>(in elements)</i>
		 * @return constant reference to column
		 */
		template < uint R = M >
		const mat< R, 1, L, T >& column(uint index, uint offset = 0) const {
			return this->field< R, 1 >(offset, index);
		}
		
		/**
		 * Returns a copy of this matrix with positive sign.
		 *
		 * @return positive copy of matrix
		 */
		mat< M, N, N, T > operator+() const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = +(*this)(i, j);
				}
			}
			
			return result;
		}
		
		/**
		 * Returns the sum of this matrix and another matrix of the same size.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other <i>(matrix of same size)</i>
		 * @return sum of matrices
		 */
		template < uint K = N >
		mat< M, N, N, T > operator+(const mat< M, N, K, T >& other) const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = (*this)(i, j) + other(i, j);
				}
			}
			
			return result;
		}
		
		/**
		 * Returns the sum of this matrix and a constant value.
		 *
		 * @param[in] value
		 * @return sum of matrix and value
		 */
		mat< M, N, N, T> operator+(T value) const {
            mat< M, N, N, T > result;

            uint i, j;

            for (i = 0; i < M; i++) {
                for (j = 0; j < N; j++) {
                    result(i, j) = (*this)(i, j) + value;
                }
            }

            return result;
		}
		
		/**
		 * Adds another matrix of the same size to this matrix.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other <i>(matrix of same size)</i>
		 * @return reference to itself
		 */
		template < uint K = N >
		mat& operator+=(const mat< M, N, K, T >& other) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) += other(i, j);
				}
			}
			
			return *this;
		}
		
		/**
		 * Returns a copy of this matrix with negative sign.
		 *
		 * @return negative copy of matrix
		 */
		mat< M, N, N, T > operator-() const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = -(*this)(i, j);
				}
			}
			
			return result;
		}
		
		/**
		 * Returns the difference of this matrix and another matrix of the same size.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other <i>(matrix of same size)</i>
		 * @return difference of matrices
		 */
		template < uint K = N >
		mat< M, N, N, T > operator-(const mat< M, N, K, T >& other) const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = (*this)(i, j) - other(i, j);
				}
			}
			
			return result;
		}
		
		/**
		 * Returns the difference of this matrix and a constant value.
		 *
		 * @param[in] value
		 * @return difference of matrix and value
		 */
        mat< M, N, N, T> operator-(T value) const {
            mat< M, N, N, T > result;

            uint i, j;

            for (i = 0; i < M; i++) {
                for (j = 0; j < N; j++) {
                    result(i, j) = (*this)(i, j) - value;
                }
            }

            return result;
        }
		
		/**
		 * Subtracts another matrix of the same size from this matrix.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other <i>(matrix of same size)</i>
		 * @return reference to itself
		 */
		template < uint K = N >
		mat& operator-=(const mat< M, N, K, T >& other) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) -= other(i, j);
				}
			}
			
			return *this;
		}
		
		/**
		 * Returns the product of this matrix and a constant value.
		 *
		 * @param value
		 * @return product of matrix and value
		 */
		mat< M, N, N, T > operator*(T value) const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = (*this)(i, j) * value;
				}
			}
			
			return result;
		}
		
		/**
		 * Multiplies this matrix with a constant value.
		 *
		 * @param[in] value
		 * @return reference to itself
		 */
		mat& operator*=(T value) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) *= value;
				}
			}
			
			return *this;
		}
		
		/**
		 * Returns the division of this matrix and a constant value.
		 *
		 * @param[in] value
		 * @return division of matrix and value
		 */
		mat< M, N, N, T > operator/(T value) const {
			mat< M, N, N, T > result;
			
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					result(i, j) = (*this)(i, j) / value;
				}
			}
			
			return result;
		}
		
		/**
		 * Divides this matrix by a constant value.
		 *
		 * @param[in] value
		 * @return reference to itself
		 */
		mat& operator/=(T value) {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					(*this)(i, j) /= value;
				}
			}
			
			return *this;
		}
		
		/**
		 * Returns the product of this matrix and another matrix with fitting size.
		 *
		 * (<b>M</b>x<b>X</b>) = (<b>M</b>x<b>N</b>) * (<b>N</b>x<b>X</b>)
		 *
		 * @tparam X <i>(amount of columns for the other matrix)</i>
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other <i>(matrix of fitting size)</i>
		 * @return product of matrices
		 */
		template < uint X, uint K = X >
		mat< M, X, X, T > operator*(const mat< N, X, K, T >& other) const {
			mat< M, X, X, T > result;
			
			uint i, j, k;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < X; j++) {
					T value = 0;
					
					for (k = 0; k < N; k++) {
						value += (*this)(i, k) * other(k, j);
					}
					
					result(i, j) = value;
				}
			}
			
			return result;
		}
		
		/**
		 * Returns if all elements of this matrix and another matrix match.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other
		 * @return <b>true</b> if other matrix is equal, otherwise <b>false</b>.
		 */
		template < uint K = N >
		bool operator==(const mat< M, N, K, T >& other) const {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					if ((*this)(i, j) == other(i, j)) {
					} else {
						return false;
					}
				}
			}
			
			return true;
		}
		
		/**
		 * Returns if any element of this matrix and the representing element of
		 * another matrix does not match.
		 *
		 * @tparam K <i>(elements per row for indicating the other matrix)</i>
		 * @param[in] other
		 * @return <b>true</b> if other matrix is different, otherwise <b>false</b>.
		 */
		template < uint K = N >
		bool operator!=(const mat< M, N, K, T >& other) const {
			uint i, j;
			
			for (i = 0; i < M; i++) {
				for (j = 0; j < N; j++) {
					if ((*this)(i, j) != other(i, j)) {
						return true;
					}
				}
			}
			
			return false;
		}
	};

	/**
	 * Returns the product of a value and a matrix.
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] value
	 * @param[in] matrix
	 * @return product of value and matrix
	 */
	template <uint M, uint N = M, uint L = N, typename T = float>
	constexpr mat< M, N, N, T > operator*(T value, const mat< M, N, L, T >& matrix) {
	    return matrix * value;
	}
	
	/**
	 * Returns a transposed copy of a given matrix.
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] matrix
	 * @return transposed copy of matrix
	 */
	template < uint M, uint N = M, uint L = N, typename T = float >
	mat< N, M, M, T > transpose(const mat< M, N, L, T >& matrix) {
		mat< N, M, M, T > result;
		
		uint i, j;
		
		for (i = 0; i < M; i++) {
			for (j = 0; j < N; j++) {
				result(j, i) = matrix(i, j);
			}
		}
		
		return result;
	}
	
	/**
	 * Returns a transposed reference of a given vector/column-matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param matrix
	 * @return transposed reference of matrix
	 */
	template < uint S, uint L = 1, typename T = float >
	constexpr mat< S, 1, 1, T >& flip(mat< 1, S, L, T >& matrix) {
		return assert_cast< mat< S, 1, 1, T> >(matrix);
	}
	
	/**
	 * Returns a transposed constant reference of a given vector/column-matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param matrix
	 * @return transposed constant reference of matrix
	 */
	template < uint S, uint L = 1, typename T = float >
	constexpr const mat< S, 1, 1, T >& flip(const mat< 1, S, L, T >& matrix) {
		return assert_cast< const mat< S, 1, 1, T> >(matrix);
	}
	
	/**
	 * Returns a transposed reference of a given row-matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param matrix
	 * @return transposed reference of matrix
	 */
	template < uint S, typename T = float >
	constexpr mat< 1, S, S, T >& flip(mat< S, 1, 1, T >& matrix) {
		return assert_cast< mat< 1, S, S, T> >(matrix);
	}
	
	/**
	 * Returns a transposed constant reference of a given row-matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param matrix
	 * @return transposed constant reference of matrix
	 */
	template < uint S, typename T = float >
	constexpr const mat< 1, S, S, T >& flip(const mat< S, 1, 1, T >& matrix) {
		return assert_cast< const mat< 1, S, S, T> >(matrix);
	}
	
	namespace __mat {
		
		template < typename T = float >
		constexpr T __detFactor(uint i) {
			return static_cast< T >(i % 2) * 2 - 1;
		}
		
		template < uint M, uint L = M >
		struct __det {
			
			template < typename T = float >
			static T _(const mat< M, M, L, T >& matrix) {
				mat< M - 1, M - 1, M - 1, T > sub = matrix.template field< M - 1, M - 1 >(1, 1);
				
				T result = 0;
				uint i;
				
				for (i = 0; i < M; i++) {
					result -= matrix(0, i) * __det< M - 1, M - 1 >::_(sub) * __detFactor< T >(i);
					
					if (i + 1 < M) {
						sub.template column< M - 1 >(i) = matrix.template column< M - 1 >(i, 1);
					}
				}
				
				return result;
			}
		};
		
		template <>
		struct __det< 1, 1 > {
			
			template < typename T = float >
			static T _(const mat< 1, 1, 1, T >& matrix) {
				return matrix(0, 0);
			}
		};
		
		template <>
		struct __det< 0, 0 > {
			
			template < typename T = float >
			static T _(const mat< 0, 0, 0, T >& matrix) {
				return 1;
			}
		};
	}
	
	/**
	 * Returns the determinant of a given matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] matrix
	 * @return determinant value
	 */
	template < uint S, uint L = S, typename T = float >
	constexpr T determinant(const mat< S, S, L, T >& matrix) {
		return __mat::__det< S, L >::template _< T >(matrix);
	}
	
	namespace __mat {
		
		template < uint M, uint L = M >
		struct __inv {
			
			template < typename T = float >
			static constexpr mat< M, M, M, T > _(const mat< M, M, L, T >& matrix) {
				mat< M - 1, M - 1, M - 1, T > sub;
				
				const T d = __det< M, M >::template _< T >(matrix);
				
				mat< M, M, M, T > result;
				uint i, j, k, l;
				
				for (i = 0; i < M; i++) {
					for (j = 0; j < M; j++) {
						for (k = 1; k < M; k++) {
							for (l = 1; l < M; l++) {
								sub(k - 1, l - 1) = matrix((i + k) % M, (j + l) % M);
							}
						}
						
						result(j, i) = __det< M - 1, M - 1 >::template _< T >(sub) / d;
					}
				}
				
				return result;
			}
		};
		
		template <>
		struct __inv< 1, 1 > {
			
			template < typename T = float >
			static constexpr mat< 1, 1, 1, T > _(const mat< 1, 1, 1, T >& matrix) {
				return mat< 1, 1, 1, T >(
						static_cast<T>(1) / matrix(0, 0)
				);
			}
		};
		
		template <>
		struct __inv< 0, 0 > {
			
			template < typename T = float >
			static constexpr mat< 0, 0, 0, T > _(const mat< 0, 0, 0, T >& matrix) {
				return mat< 0, 0, 0, T >();
			}
		};
	}
	
	/**
	 * Returns the inverse matrix of another given matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] matrix
	 * @return inverse matrix
	 */
	template < uint S, uint L = S, typename T = float >
	constexpr mat< S, S, S, T > inverse(const mat< S, S, L, T >& matrix) {
		return __mat::__inv< S, L >::template _< T >(matrix);
	}
	
	/**
	 * Changes a matrix of a given size to an identity-matrix.
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[out] matrix
	 */
	template < uint S, uint L = S, typename T = float >
	constexpr void identity(mat< S, S, L, T >& matrix) {
		for (uint i = 0; i < S; i++) {
			for (uint j = 0; j < S; j++) {
				matrix(i, j) = (i == j? 1 : 0);
			}
		}
	}
	
	/**
	 * Returns an identity-matrix for given size (<b>S</b>x<b>S</b>).
	 *
	 * @tparam S <i>(size of matrix)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @return identity-matrix
	 */
	template < uint S, typename T = float >
	mat< S, S, S, T > identity() {
		mat< S, S, S, T > result;
		identity(result);
		return result;
	}
	
	template < uint M, uint N = M, uint L = N, typename T = float >
	ostream& operator<<(ostream& stream, const mat< M, N, L, T >& matrix) {
		uint i, j;
		
		for (i = 0; i < M; i++) {
			if (i > 0) {
				stream << endl;
			}
			
			for (j = 0; j < N; j++) {
				if (j > 0) {
					stream << " ";
				}
				
				stream << matrix(i, j);
			}
		}
		
		return stream;
	}
	
	/** @} */
	
}
