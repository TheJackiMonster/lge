#pragma once

//
// Created by thejackimonster on 04.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "util.hxx"

namespace lge {
	
	/** @addtogroup mat_group
	 *  @{
	 */
	
	template < uint S, uint L = 1, typename T = float >
	using vec = mat< S, 1, L, T >;
	
	/**
	 * Returns the dot-product as scalar value from two vectors
	 * of equal size.
	 *
	 * @tparam S <i>(size of vectors)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam K <i>(elements per row for indexing for other vector)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] vector
	 * @param[in] other <i>(vector)</i>
	 * @return dot-product of vectors
	 */
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	T dot(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		T result = 0;
		
		for (uint i = 0; i < S; i++) {
			result += vector[i] * other[i];
		}
		
		return result;
	}
	
	/**
	 * Returns the arithmetic length of a vector.
	 *
	 * @tparam S <i>(size of vector)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] vector
	 * @return arithmetic length
	 */
	template < uint S, uint L = 1, typename T = float >
	constexpr T length(const vec< S, L, T >& vector) {
		return sqrt< T >(dot< S, L, L, T >(vector, vector));
	}
	
	/**
	 * Returns the arithmetic distance between two vectors.
	 *
	 * @tparam S <i>(size of vectors)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam K <i>(elements per row for indexing for other vector)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] vector
	 * @param[in] other <i>(vector)</i>
	 * @return arithmetic distance
	 */
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	constexpr T distance(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		return length< S, S, T >(vector - other);
	}
	
	/**
	 * Returns a 3-dimensional vector which is orthogonal to two other
	 * 3-dimensional vectors. This vector is calculated via the cross-product.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam K <i>(elements per row for indexing for other vector)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] vector
	 * @param[in] other <i>(vector)</i>
	 * @return cross-product of vectors
	 */
	template < uint L = 1, uint K = 1, typename T = float >
	constexpr vec< 3, 1, T > cross(const vec< 3, L, T >& vector, const vec< 3, K, T >& other) {
		return vec< 3, 1, T >(
				vector[1] * other[2] - vector[2] * other[1],
				vector[2] * other[0] - vector[0] * other[2],
				vector[0] * other[1] - vector[1] * other[0]
		);
	}
	
	/**
	 * Returns normalized copy of a vector. The returned vectors length
	 * is equal to 1.
	 *
	 * @tparam S <i>(size of vector)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] vector
	 * @return normalized copy of vector
	 */
	template < uint S, uint L = 1, typename T = float >
	constexpr vec< S, 1, T > normalize(const vec< S, L, T >& vector) {
		return vector / length< S, L, T >(vector);
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	mat< S, S, S, T > outerProduct(const vec< S, L, T >& column, const vec< S, K, T >& row) {
		mat< S, S, S, T > result;
		
		uint i, j;
		
		for (i = 0; i < S; i++) {
			for (j = 0; j < S; j++) {
				result(i, j) = column[i] * row[j];
			}
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	vec< S, 1, T > vecProduct(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		vec< S, 1, T > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = vector[i] * other[i];
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	vec< S, 1, T > vecDivision(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		vec< S, 1, T > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = vector[i] / other[i];
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, uint J = 1, typename T = float >
	vec< S, S, T > faceforward(const vec< S, L, T >& normal, const vec< S, K, T >& incident, const vec< S, J, T >& reference) {
		const T d = dot< S, J, K, T >(reference, incident);
		
		if (d < 0) {
			return +normal;
		} else {
			return -normal;
		}
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	vec< S, S, T > reflect(const vec< S, L, T >& incident, const vec< S, K, T >& normal) {
		const T d = dot< S, L, K, T >(normal, incident);
		
		return incident - 2 * d * normal;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	vec< S, S, T > refract(const vec< S, L, T >& incident, const vec< S, K, T >& normal, T eta) {
		const T d = dot< S, L, K, T >(normal, incident);
		const T k = 1 - (eta * eta) * (1 - d * d);
		
		if (k < 0) {
			return vec< S, S, T >(0);
		} else {
			return eta * incident - (eta * d + sqrt< T >(k)) * normal;
		}
	}
	
	/** @} */
	
}
