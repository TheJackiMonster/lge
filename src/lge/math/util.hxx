#pragma once
//
// Created by thejackimonster on 04.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "mat.hxx"

#include <cmath>
#include <limits>

namespace lge {
	
	/** @defgroup math_group The Math Group
	 *  This group is for mathematical functions and structures.
	 *  @{
	 */

    using std::numeric_limits;
	
    /**
     * Returns the absolute of a value x.
     *
     * @tparam T <i>(type of value)</i>
     * @param[in] x
     * @return absolute
     */
	template < typename T >
	constexpr T abs(typename const_ref< T >::type x) {
		return (x < 0? -x : +x);
	}
	
	/**
     * Returns the minimal value between two values: x and y.
     *
     * @tparam T <i>(type of values)</i>
     * @param[in] x
     * @param[in] y
     * @return minimal value
     */
	template < typename T = float >
	constexpr T min(typename const_ref< T >::type x, typename const_ref< T >::type y) {
		return x < y? x : y;
	}

	/**
	 * Returns the minimal value of all elements from the cells of a matrix.
	 * @ingroup mat_group
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] m <i>(matrix)</i>
	 * @return minimal value
	 */
    template < uint M, uint N = M, uint L = N, typename T = float >
    constexpr T min(const mat< M, N, L, T >& m) {
        T result = numeric_limits<T>::max();

        for (uint i = 0; i < M; i++) {
            for (uint j = 0; j < N; j++) {
                result = min(result, m(i, j));
            }
        }

        return result;
    }
	
    /**
     * Returns a matrix which contains only the minimal value for
     * each cell compared between both matrices: x and y.
     * @ingroup mat_group
     *
     * @tparam M <i>(amount of rows)</i>
     * @tparam N <i>(amount of columns)</i>
     * @tparam L <i>(elements per row for indexing matrix x)</i>
     * @tparam K <i>(elements per row for indexing matrix y)</i>
     * @tparam T <i>(type of the cells)</i>
     * @param[in] x <i>(matrix)</i>
     * @param[in] y <i>(matrix)</i>
     * @return matrix with minimal values
     */
	template < uint M, uint N = M, uint L = N, uint K = N, typename T = float >
	constexpr mat< M, N, N, T > min(const mat< M, N, L, T >& x, const mat< M, N, K, T >& y) {
		mat< M, N, N, T > result;
		
		for (uint i = 0; i < M; i++) {
			for (uint j = 0; j < N; j++) {
				result(i, j) = min(x(i, j), y(i, j));
			}
		}
		
		return result;
	}
	
	/**
     * Returns the maximal value between two values: x and y.
     *
     * @tparam T <i>(type of values)</i>
     * @param[in] x
     * @param[in] y
     * @return maximal value
     */
	template < typename T = float >
	constexpr T max(typename const_ref< T >::type x, typename const_ref< T >::type y) {
		return x > y? x : y;
	}
	
	/**
	 * Returns the maximal value of all elements from the cells of a matrix.
	 * @ingroup mat_group
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] m <i>(matrix)</i>
	 * @return maximal value
	 */
    template < uint M, uint N = M, uint L = N, typename T = float >
    constexpr T max(const mat< M, N, L, T >& m) {
        T result = numeric_limits<T>::min();

        for (uint i = 0; i < M; i++) {
            for (uint j = 0; j < N; j++) {
                result = max(result, m(i, j));
            }
        }

        return result;
    }
	
	/**
	 * Returns a matrix which contains only the maximal value for
	 * each cell compared between both matrices: x and y.
	 * @ingroup mat_group
	 *
	 * @tparam M <i>(amount of rows)</i>
	 * @tparam N <i>(amount of columns)</i>
	 * @tparam L <i>(elements per row for indexing matrix x)</i>
	 * @tparam K <i>(elements per row for indexing matrix y)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] x <i>(matrix)</i>
	 * @param[in] y <i>(matrix)</i>
	 * @return matrix with maximal values
	 */
	template < uint M, uint N = M, uint L = N, uint K = N, typename T = float >
	constexpr mat< M, N, N, T > max(const mat< M, N, L, T >& x, const mat< M, N, K, T >& y) {
		mat< M, N, N, T > result;
		
		for (uint i = 0; i < M; i++) {
			for (uint j = 0; j < N; j++) {
				result(i, j) = max(x(i, j), y(i, j));
			}
		}
		
		return result;
	}
	
	template < typename T = float >
	constexpr T clamp(typename const_ref< T >::type value,
					  typename const_ref< T >::type minValue,
					  typename const_ref< T >::type maxValue) {
		return min(max(value, minValue), maxValue);
	}
	
	template < typename T >
	constexpr T exp(typename const_ref< T >::type x) {
		return std::exp(x);
	}
	
	template < typename T >
	constexpr T exp2(typename const_ref< T >::type x) {
		return std::exp2(x);
	}
	
	template < typename T >
	constexpr T pow(typename const_ref< T >::type x, typename const_ref< T >::type y) {
		return std::pow(x, y);
	}
	
	template < typename T >
	constexpr T sqrt(typename const_ref< T >::type value) {
		return std::sqrt(value);
	}
	
	template < typename T = float >
	constexpr T inversesqrt(typename const_ref< T >::type value) {
		return static_cast<T>(1) / sqrt< T >(value);
	}
	
	template < typename T = float, typename A = float >
	constexpr T mix(typename const_ref< T >::type x, typename const_ref< T >::type y,
					typename const_ref< A >::type a) {
		return static_cast<T>(x * (1 - a) + y * a);
	}
	
	template < uint M, uint N = M, uint L = N, uint K = N, typename T = float >
	constexpr bool order(const mat< M, N, L, T >& x, const mat< M, N, K, T >& y) {
		for (uint i = 0; i < M; i++) {
			for (uint j = 0; j < N; j++) {
				auto val = x(i, j) - y(i, j);
				
				if (val == 0) {
					continue;
				} else {
					return val < 0;
				}
			}
		}
		
		return false;
	}
	
}
