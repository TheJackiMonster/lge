#pragma once
//
// Created by thejackimonster on 09.11.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "trigonometry.hxx"
#include "vec.hxx"

namespace lge {
	
	/** @addtogroup mat_group
	 *  @{
	 */

	template < uint L = 1, typename T = float >
	using quaternion = vec< 4, L, T >;
	
	/**
	 * Returns a reference to the 3-dimensional vector-part of
	 * a given quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return reference to vector-part of quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr vec< 3, L, T >& vecPart(quaternion< L, T >& quat) {
		return quat.template column< 3 >(0);
	}
	
	/**
	 * Returns a constant reference to the 3-dimensional vector-part of
	 * a given quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return constant reference to vector-part of quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr const vec< 3, L, T >& vecPart(const quaternion< L, T >& quat) {
		return quat.template column< 3 >(0);
	}
	
	/**
	 * Returns a reference to the scalar-part of
	 * a given quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return reference to scalar-part of quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr T& scalarPart(quaternion< L, T >& quat) {
		return quat[3];
	}
	
	/**
	 * Returns a constant reference to the scalar-part of
	 * a given quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return constant reference to scalar-part of quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr T scalarPart(const quaternion< L, T >& quat) {
		return quat[3];
	}
	
	/**
	 * Returns the product of two quaternions.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam K <i>(elements per row for indicating the other quaternion)</i>
	 * @tparam T <i>(type of the cells)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @param[in] other <i>(quaternion)</i>
	 * @return product of quaternions
	 */
	template < uint L = 1, uint K = 1, typename T = float >
	constexpr quaternion< 1, T > operator*(const quaternion< L, T >& quat, const quaternion< K, T >& other) {
		quaternion< 1, T > result;
		
		scalarPart(result) = scalarPart(quat) * scalarPart(other) - (
				dot(vecPart(quat), vecPart(other))
		);
		
		vecPart(result) = vecPart(other) * scalarPart(quat) + vecPart(quat) * scalarPart(other) + (
				cross(vecPart(quat), vecPart(other))
		);
		
		return result;
	}
	
	/**
	 * Returns the conjugated quaternion of another quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return conjugated quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr quaternion< 1, T > conjugate(const quaternion< L, T >& quat) {
		quaternion< 1, T > result;
		
		scalarPart(result) = scalarPart(quat);
		vecPart(result) = -vecPart(quat);
		
		return result;
	}
	
	/**
	 * Returns the inverse quaternion of another quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return inverse quaternion
	 */
	template < uint L = 1, typename T = float >
	constexpr quaternion< 1, T > inverse(const quaternion< L, T >& quat) {
		return conjugate(quat) / dot(quat, quat);
	}
	
	/**
	 * Returns a quaternion representing a rotation around an axis
	 * with a given angle.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] radian <i>(angle)</i>
	 * @param[in] axis <i>(vector)</i>
	 * @return quaternion representing rotation
	 */
	template < uint L = 1, typename T = float >
	constexpr quaternion< 1, T > rotation(T radian, const vec< 3, L, T >& axis) {
		quaternion< 1, T > result;
		
		scalarPart(result) = cos< T >(radian / 2);
		vecPart(result) = normalize(axis) * sin< T >(radian / 2);
		
		return result;
	}
	
	/**
	 * Returns a transformed vector which represents a position-vector
	 * rotated via a quaternion.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam K <i>(elements per row for indicating the other quaternion)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @param[in] position <i>(vector)</i>
	 * @return transformed vector
	 */
	template < uint L = 1, uint K = 1, typename T = float >
	constexpr vec< 3, 1, T > transform(const quaternion< L, T >& quat, const vec< 3, K, T>& position) {
		vec< 3, 1, T > temp = position * scalarPart(quat) - cross(vecPart(quat), position);
		
		return temp * scalarPart(quat) + vecPart(quat) * dot(vecPart(quat), position) + cross(temp, vecPart(quat));
	}
	
	/**
	 * Returns a rotation matrix representing the given quaternions orientation.
	 *
	 * @tparam L <i>(elements per row for indexing)</i>
	 * @tparam T <i>(type of elements)</i>
	 * @param[in] quat <i>(quaternion)</i>
	 * @return 3x3 rotation matrix
	 */
	template < uint L = 1, typename T = float >
	constexpr mat< 3, 3, 3, T > orientation(const quaternion< L, T >& quat) {
		mat< 3, 3, 3, T > result;
		
		const T& s = scalarPart(quat);
		const vec< 3, L, T >& v = vecPart(quat);
		
		T x2 = v[0] * v[0];
		T y2 = v[1] * v[1];
		T z2 = v[2] * v[2];
		
		result(0, 0) = 1 - 2 * (y2 + z2);
		result(1, 1) = 1 - 2 * (z2 + x2);
		result(2, 2) = 1 - 2 * (x2 + y2);
		
		result(0, 1) = 2 * v[0] * v[1] - 2 * s * v[2];
		result(1, 2) = 2 * v[1] * v[2] - 2 * s * v[0];
		result(2, 0) = 2 * v[2] * v[0] - 2 * s * v[1];
		
		result(0, 2) = 2 * v[0] * v[2] + 2 * s * v[1];
		result(1, 0) = 2 * v[1] * v[0] + 2 * s * v[2];
		result(2, 1) = 2 * v[2] * v[1] + 2 * s * v[0];
		
		return result;
	}
	
	/** @} */

}
