#pragma once

//
// Created by thejackimonster on 01.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/templates.hxx"

#include <cmath>

namespace lge {
	
	/** @addtogroup math_group
	 *  @{
	 */
	
	template < typename T = float >
	constexpr T degrees(typename const_ref< T >::type radians) {
		return radians * 180 / M_PI;
	}
	
	template < typename T = float >
	constexpr T radians(typename const_ref< T >::type degrees) {
		return degrees * M_PI / 180;
	}
	
	template < typename T >
	constexpr T sin(typename const_ref< T >::type radian) {
		return std::sin(radian);
	}
	
	template < typename T >
	constexpr T sinh(typename const_ref< T >::type radian) {
		return std::sinh(radian);
	}
	
	template < typename T >
	constexpr T cos(typename const_ref< T >::type radian) {
		return std::cos(radian);
	}
	
	template < typename T >
	constexpr T cosh(typename const_ref< T >::type radian) {
		return std::cosh(radian);
	}
	
	template < typename T >
	constexpr T tan(typename const_ref< T >::type radian) {
		return std::tan(radian);
	}
	
	template < typename T >
	constexpr T tanh(typename const_ref< T >::type radian) {
		return std::tanh(radian);
	}
	
	template < typename T >
	constexpr T asin(typename const_ref< T >::type ratio) {
		return std::asin(ratio);
	}
	
	template < typename T >
	constexpr T asinh(typename const_ref< T >::type ratio) {
		return std::asinh(ratio);
	}
	
	template < typename T >
	constexpr T acos(typename const_ref< T >::type ratio) {
		return std::acos(ratio);
	}
	
	template < typename T >
	constexpr T acosh(typename const_ref< T >::type ratio) {
		return std::acosh(ratio);
	}
	
	template < typename T >
	constexpr T atan(typename const_ref< T >::type ratio) {
		return std::asin(ratio);
	}
	
	template < typename T >
	constexpr T atanh(typename const_ref< T >::type ratio) {
		return std::asinh(ratio);
	}
	
	template < typename T >
	constexpr T atan2(typename const_ref< T >::type y, typename const_ref< T >::type x) {
		return std::atan2(y, x);
	}
	
	/** @} */
	
}
