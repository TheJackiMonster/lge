#pragma once

//
// Created by thejackimonster on 11.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../graphics/util.hxx"
#include "../system/iostream.hxx"

namespace lge {
	
	/** @defgroup camera_group The Camera Group
	 *  This group is for structs and classes referring to cameras and view-matrices.
	 *  @{
	 */
	
	/**
	 * An object-oriented abstraction layer to handle view-matrices in C++.
	 *
	 * @author TheJackiMonster
	 * @since 11.12.2017
	 */
	class Camera {
	private:
		mat4 m_view;
	
	public:
		/**
		 * Creates a camera storing a view-matrix.
		 */
		explicit Camera();
		
		Camera(const Camera& other) = default;
		
		Camera(Camera&& other) = default;
		
		~Camera() = default;
		
		Camera& operator=(const Camera& other) = default;
		
		Camera& operator=(Camera&& other) = default;
		
		/**
		 * Calculates the view of the camera from a specific position
		 * with a specific direction.
		 *
		 * @param position
		 * @param direction
		 */
		void look(const vec3& position, const vec3& direction);
		
		/**
		 * Calculates the view of the camera from a specific position
		 * at a specific target (stored in <b>center</b>).
		 *
		 * @param position
		 * @param center
		 */
		void lookAt(const vec3& position, const vec3& center);
		
		/**
		 * Rotates the camera around pitch and yaw by specific angles.
		 *
		 * @param deltaPitch
		 * @param deltaYaw
		 */
		void turn(float deltaPitch, float deltaYaw);
		
		/**
		 * Sets the position of the camera.
		 *
		 * @param position
		 */
		void setPosition(const vec3& position);
		
		/**
		 * Returns the current position of the camera.
		 *
		 * @return position of the camera <i>(as vec3)</i>
		 */
		[[nodiscard]]
		vec3 getPosition() const;
		
		/**
		 * Returns the cameras view-matrix as reference.
		 *
		 * @return reference of the cameras view <i>(as mat4)</i>
		 */
		[[nodiscard]]
		const mat4& getView() const;
		
		/**
		 * Returns the direction-vector from the cameras view.
		 *
		 * @return direction of the cameras view <i>(as vec3)</i>
		 */
		[[nodiscard]]
		vec3 getDirection() const;
		
		/**
		 * Returns the side-vector from the cameras view.
		 *
		 * @return side of the cameras view <i>(as vec3)</i>
		 */
		[[nodiscard]]
		vec3 getSide() const;
		
		/**
		 * Returns the up-vector from the cameras view.
		 *
		 * @return up of the cameras view <i>(as vec3)</i>
		 */
		[[nodiscard]]
		vec3 getUp() const;
		
		/**
		 * Calculates the view of the camera from a specific rotation
		 * defined by pitch- and yaw-angles.
		 *
		 * @param pitch
		 * @param yaw
		 */
		void setAngles(float pitch, float yaw);
		
		/**
		 * Returns the cameras rotation as pitch- and yaw-angles stored
		 * in a vec2.
		 *
		 * @return pitch and yaw of the cameras rotation <i>(as vec3)</i>
		 */
		[[nodiscard]]
		vec2 getAngles() const;
		
		/**
		 * Returns the pitch-angle of the cameras rotation.
		 *
		 * @return pitch the cameras rotation
		 */
		[[nodiscard]]
		float getPitch() const;
		
		/**
		 * Returns the yaw-angle of the cameras rotation.
		 *
		 * @return yaw of the cameras rotation
		 */
		[[nodiscard]]
		float getYaw() const;
		
	};
	
	ostream& operator<<(ostream& stream, const Camera& camera);
	
	/** @} */
	
}
