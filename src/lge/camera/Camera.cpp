//
// Created by thejackimonster on 11.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Camera.hpp"

namespace lge {
	
	Camera::Camera() {
		this->m_view = identity< 4 >();
	}
	
	void Camera::look(const vec3 &position, const vec3 &direction) {
		this->lookAt(position, position + direction);
	}
	
	void Camera::lookAt(const vec3& position, const vec3& center) {
		this->m_view = lge::lookAt(position, center, lge::vec3(0, 1, 0));
	}
	
	void Camera::turn(float deltaPitch, float deltaYaw) {
		vec2 angles = this->getAngles();
		this->setAngles(angles[0] + deltaPitch, angles[1] + deltaYaw);
	}
	
	void Camera::setPosition(const vec3& position) {
		this->m_view.column< 3 >(3) = this->m_view.field< 3, 3 >(0, 0) * -position;
	}
	
	vec3 Camera::getPosition() const {
		return -(transpose(this->m_view.field< 3, 3 >(0, 0)) * vec3(this->m_view.column< 3 >(3)));
	}
	
	const mat4& Camera::getView() const {
		return this->m_view;
	}
	
	vec3 Camera::getDirection() const {
		return -flip< 3 >(this->m_view.row< 3 >(2));
	}
	
	vec3 Camera::getSide() const {
		return flip< 3 >(this->m_view.row< 3 >(0));
	}
	
	vec3 Camera::getUp() const {
		return flip< 3 >(this->m_view.row< 3 >(1));
	}
	
	void Camera::setAngles(float pitch, float yaw) {
		float cosPitch = cos< float >(pitch);
		
		vec3 direction = normalize(lge::vec3(
				sin< float >(yaw) * cosPitch,
				sin< float >(pitch),
				cos< float >(yaw) * cosPitch
		));
		
		vec3 position = this->getPosition();
		
		this->lookAt(position, position + direction);
	}
	
	vec2 Camera::getAngles() const {
		vec3 direction = this->getDirection();
		return lge::vec2(
				atan2< float >(direction[1], sqrt< float >(
						direction[0] * direction[0] + direction[2] * direction[2]
				)),
				atan2< float >(direction[0], direction[2])
		);
	}
	
	float Camera::getPitch() const {
		vec3 direction = this->getDirection();
		return atan2< float >(direction[1], sqrt< float >(
				direction[0] * direction[0] + direction[2] * direction[2]
		));
	}
	
	float Camera::getYaw() const {
		vec3 direction = this->getDirection();
		return atan2< float >(direction[0], direction[2]);
	}
	
	ostream& operator<<(ostream& stream, const Camera& camera) {
		stream << "Camera::{ ";
		stream << "position: [ " << flip(camera.getPosition()) << " ], ";
		stream << "direction: [ " << flip(camera.getDirection()) << " ], ";
		stream << "up: [ " << flip(camera.getUp()) << " ] ";
		stream << "}";
		
		return stream;
	}
	
}
