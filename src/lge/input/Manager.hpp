#pragma once
//
// Created by thejackimonster on 06.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/string.hxx"
#include "../math/vec.hxx"
#include "../memory/map.hxx"

namespace lge {
	
	class InputManager {
	private:
	public:
		explicit InputManager();
		
		InputManager(const InputManager& other);
		
		InputManager(InputManager&& other);
		
		~InputManager();
		
		InputManager& operator=(const InputManager& other);
		
		InputManager& operator=(InputManager&& other);
		
		const bool& getBool(const string& name) const;
		
		const float& getFloat(const string& name) const;
		
		const vec< 2 >& getVec2(const string& name) const;
		
		const mat< 4, 4 >& getMat4x4(const string& name) const;
	};
}
