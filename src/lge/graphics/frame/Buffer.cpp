//
// Created by thejackimonster on 02.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Buffer.hpp"
#include "../../memory/vector.hxx"

namespace lge {
	
	GLenum FrameBuffer::getAttachmentByName(const std::string& name) const {
		auto iter = this->m_attachments.find(name);
		
		if (iter != this->m_attachments.end()) {
			return iter->second;
		} else {
			return GL_NONE;
		}
	}
	
	void FrameBuffer::attachByName(const string& name, GLenum value) {
		this->m_attachments[name] = value;
	}
	
	void FrameBuffer::detachByName(const string& name) {
		this->m_attachments.erase(name);
	}
	
	GLenum FrameBuffer::getMaxColorAttachment(GLint max_color_attachments) const {
		GLenum max_attachment = GL_NONE;
		
		for (auto& attachment : this->m_attachments) {
			if (attachment.second >= GL_COLOR_ATTACHMENT0) {
				const int index = attachment.second - GL_COLOR_ATTACHMENT0;
				
				if ((index < max_color_attachments) && (attachment.second > max_attachment)) {
					max_attachment = attachment.second;
				}
			}
		}
		
		return max_attachment;
	}
	
	bool FrameBuffer::hasDoubleBuffer() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->getResource(), GL_DOUBLEBUFFER, &value);
		
		return (value == GL_TRUE);
	}
	
	uint FrameBuffer::getSamples() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->getResource(), GL_SAMPLES, &value);
		
		return static_cast<uint>(value);
	}
	
	uint FrameBuffer::getSampleBuffers() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->getResource(), GL_SAMPLE_BUFFERS, &value);
		
		return static_cast<uint>(value);
	}
	
	bool FrameBuffer::hasStereo() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->getResource(), GL_STEREO, &value);
		
		return (value == GL_TRUE);
	}
	
	void FrameBuffer::setup(const lge::ShaderFragments& bindings) {
		vector< GLenum > buffers(this->m_attachments.size());
		
		GLint max_buffers;
		
		glGetIntegerv(GL_MAX_DRAW_BUFFERS, &max_buffers);
		
		if (max_buffers > buffers.size()) {
			max_buffers = buffers.size();
		}
		
		GLsizei count = 0;
		
		for (auto& attachment : this->m_attachments) {
			if (attachment.second != GL_NONE) {
				int location = bindings.getLocation(attachment.first);
				
				if (location < max_buffers) {
					buffers[location] = attachment.second;
					
					if (count <= location) {
						for (int i = count; i < location; i++) {
							buffers[i] = GL_NONE;
						}
						
						count = location + 1;
					}
				}
			}
		}
		
		for (int i = count; i < max_buffers; i++) {
			buffers[i] = GL_NONE;
		}
		
		glNamedFramebufferDrawBuffers(this->getResource(), count, buffers.data());
	}
	
	bool FrameBuffer::checkStatus(bool read) const {
		const GLuint resource = this->getResource();
		
		const GLenum status = resource != 0? glCheckNamedFramebufferStatus(
				resource, (read? GL_READ_FRAMEBUFFER : GL_DRAW_FRAMEBUFFER)
		) : GL_FRAMEBUFFER_COMPLETE;
		
		const bool result = (status == GL_FRAMEBUFFER_COMPLETE);
		
		if (!result) {
			cerr << "[STATUS][FB]: ( " << resource << " ) ";
			
			switch (status) {
				case GL_FRAMEBUFFER_UNDEFINED:
					cerr << "UNDEFINED";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
					cerr << "INCOMPLETE_ATTACHMENT";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
					cerr << "INCOMPLETE_MISSING_ATTACHMENT";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
					cerr << "INCOMPLETE_DRAW_BUFFER";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
					cerr << "INCOMPLETE_READ_BUFFER";
					break;
				case GL_FRAMEBUFFER_UNSUPPORTED:
					cerr << "UNSUPPORTED";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
					cerr << "INCOMPLETE_MULTISAMPLE";
					break;
				case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
					cerr << "INCOMPLETE_LAYER_TARGETS";
					break;
				case 0:
					cerr << "ERROR_";
					
					switch (glGetError()) {
						case GL_INVALID_ENUM:
							cerr << "INVALID_ENUM";
							break;
						case GL_INVALID_OPERATION:
							cerr << "INVALID_OPERATION";
							break;
						default:
							cerr << "UNKNOWN";
							break;
					}
					
					break;
				default:
					cerr << "UNKNOWN( " << status << " )";
					break;
			}
			
			cerr << endl;
		}
		
		return result;
	}
	
	void FrameBuffer::clearColor(const vec4& color) {
		vec4 values = color;
		GLint previous;
		
		glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &previous);
		
		GLuint resource = this->getResource();
		
		if (static_cast<GLuint>(previous) != resource) {
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, resource);
		}
		
		GLint max_buffers;
		
		glGetIntegerv(GL_MAX_DRAW_BUFFERS, &max_buffers);
		
		for (int i = 0; i < max_buffers; i++) {
			GLint attachment;
			
			glGetIntegerv(GL_DRAW_BUFFER0 + i, &attachment);
			
			if (attachment != GL_NONE) {
				glClearBufferfv(GL_COLOR, i, values.data());
			}
		}
		
		if (static_cast<GLuint>(previous) != resource) {
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, static_cast<GLuint>(previous));
		}
	}
	
	void FrameBuffer::clearDepth(float depth) {
		glClearNamedFramebufferfv(this->getResource(), GL_DEPTH, 0, &depth);
	}
	
	void FrameBuffer::clearStencil(int stencil) {
		glClearNamedFramebufferiv(this->getResource(), GL_STENCIL, 0, &stencil);
	}
	
	void FrameBuffer::clearDepthStencil(float depth, int stencil) {
		glClearNamedFramebufferfi(this->getResource(), GL_DEPTH_STENCIL, 0, depth, stencil);
	}
	
	void FrameBuffer::blitTo(lge::FrameBuffer& buffer, const lge::ivec2& src0, const lge::ivec2& src1,
							 const lge::ivec2& dst0, const lge::ivec2& dst1,
							 bool color, bool depth, bool stencil, bool linear) const {
		GLbitfield bits = 0;
		
		if (color) bits |= GL_COLOR_BUFFER_BIT;
		if (depth) bits |= GL_DEPTH_BUFFER_BIT;
		if (stencil) bits |= GL_STENCIL_BUFFER_BIT;
		
		glBlitNamedFramebuffer(this->getResource(), buffer.getResource(),
							   src0[0], src0[1], src1[0], src1[1],
							   dst0[0], dst0[1], dst1[0], dst1[1],
							   bits, (linear? GL_LINEAR : GL_NEAREST)
		);
	}
	
	void FrameBuffer::useDrawing() {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->getResource());
	}
	
	void FrameBuffer::useReading() {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, this->getResource());
	}
	
	void FrameBuffer::use() {
		glBindFramebuffer(GL_FRAMEBUFFER, this->getResource());
	}
	
	DefaultFrameBuffer& FrameBuffer::getDefault() {
		static DefaultFrameBuffer frameBuffer;
		return frameBuffer;
	}
}

namespace lge {
	
	GLuint DefaultFrameBuffer::getResource() const {
		return 0;
	}
	
	void DefaultFrameBuffer::attachTarget(const string& name, FrameBufferTarget target) {
		this->attachByName(name, target);
	}
	
	void DefaultFrameBuffer::detachTarget(const std::string& name) {
		this->detachByName(name);
	}
}
