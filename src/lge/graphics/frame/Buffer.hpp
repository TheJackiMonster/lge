#pragma once
//
// Created by thejackimonster on 02.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../shader/Fragments.hpp"
#include "../vec.hxx"

#include "../../lang/string.hxx"
#include "../../memory/map.hxx"

#include "../lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class DefaultFrameBuffer;
	
	class FrameBuffer {
	private:
		map< string, GLenum > m_attachments;
	
	protected:
		explicit FrameBuffer() = default;
		
		[[nodiscard]]
		virtual GLuint getResource() const = 0;
		
		[[nodiscard]]
		GLenum getAttachmentByName(const string& name) const;
		
		void attachByName(const string& name, GLenum value);
		
		void detachByName(const string& name);
		
		[[nodiscard]]
		GLenum getMaxColorAttachment(GLint max_color_attachments) const;
	
	public:
		FrameBuffer(const FrameBuffer& other) = delete;
		
		FrameBuffer(FrameBuffer&& other) = default;
		
		~FrameBuffer() = default;
		
		FrameBuffer& operator=(const FrameBuffer& other) = delete;
		
		FrameBuffer& operator=(FrameBuffer&& other) = default;
		
		[[nodiscard]]
		bool hasDoubleBuffer() const;
		
		[[nodiscard]]
		uint getSamples() const;
		
		[[nodiscard]]
		uint getSampleBuffers() const;
		
		[[nodiscard]]
		bool hasStereo() const;
		
		void setup(const ShaderFragments& bindings);
		
		[[nodiscard]]
		bool checkStatus(bool read = false) const;
		
		void clearColor(const vec4& color);
		
		void clearDepth(float depth = 1.0f);
		
		void clearStencil(int stencil = 0);
		
		void clearDepthStencil(float depth, int stencil);
		
		void blitTo(FrameBuffer& buffer, const ivec2& src0, const ivec2& src1,
					const ivec2& dst0, const ivec2& dst1,
					bool color, bool depth, bool stencil,
					bool linear = false) const;
		
		void useDrawing();
		
		void useReading();
		
		void use();
		
		static DefaultFrameBuffer& getDefault();
	};
	
	enum FrameBufferTarget : GLenum {
		FRONT_LEFT = GL_FRONT_LEFT,
		FRONT_RIGHT = GL_FRONT_RIGHT,
		
		BACK_LEFT = GL_BACK_LEFT,
		BACK_RIGHT = GL_BACK_RIGHT,
		
		NO_TARGET = GL_NONE
	};
	
	class DefaultFrameBuffer : public FrameBuffer {
		friend class FrameBuffer;
	
	private:
		explicit DefaultFrameBuffer() = default;
	
	protected:
		[[nodiscard]]
		GLuint getResource() const override;
	
	public:
		DefaultFrameBuffer(DefaultFrameBuffer&& other) = default;
		
		~DefaultFrameBuffer() = default;
		
		DefaultFrameBuffer& operator=(DefaultFrameBuffer&& other) = default;
		
		void attachTarget(const string& name, FrameBufferTarget target);
		
		void detachTarget(const string& name);
	};
	
	/** @} */
	
}
