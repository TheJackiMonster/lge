//
// Created by thejackimonster on 02.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "BufferObject.hpp"

namespace lge {
	
	GLenum FrameBufferObject::getColorAttachment() const {
		GLint max_attachments;
		
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &max_attachments);
		
		const GLenum max_attachment = this->getMaxColorAttachment(max_attachments);
		
		if (max_attachment == GL_NONE) {
			return GL_COLOR_ATTACHMENT0;
		}
		
		GLenum attachment = GL_NONE;
		GLenum attachment0 = GL_COLOR_ATTACHMENT0;
		
		while (attachment0 <= max_attachment) {
			GLint type;
			
			glGetNamedFramebufferAttachmentParameteriv(
					this->m_fbo, attachment0, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type
			);
			
			if (type == GL_NONE) {
				attachment = attachment0;
				break;
			}
			
			attachment0++;
		}
		
		if (attachment == GL_NONE) {
			const int index = max_attachment + 1 - GL_COLOR_ATTACHMENT0;
			
			if (index < max_attachments) {
				attachment = GL_COLOR_ATTACHMENT0 + index;
			}
		}
		
		return attachment;
	}
	
	void FrameBufferObject::detach(GLuint attachment) {
		GLint type;
		
		glGetNamedFramebufferAttachmentParameteriv(
				this->m_fbo, attachment, GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE, &type
		);
		
		switch (type) {
			case GL_TEXTURE:
				glNamedFramebufferTexture(this->m_fbo, attachment, 0, 0);
				break;
			case GL_RENDERBUFFER:
				glNamedFramebufferRenderbuffer(this->m_fbo, attachment, GL_RENDERBUFFER, 0);
				break;
			default:
				break;
		}
	}
	
	GLuint FrameBufferObject::getResource() const {
		return this->m_fbo;
	}
	
	FrameBufferObject::FrameBufferObject() {
		glCreateFramebuffers(1, &(this->m_fbo));
	}
	
	FrameBufferObject::FrameBufferObject(FrameBufferObject&& other) noexcept {
		move_r(this->m_fbo, other.m_fbo);
	}
	
	FrameBufferObject::~FrameBufferObject() {
		if (this->m_fbo != 0) {
			glDeleteFramebuffers(1, &(this->m_fbo));
		}
	}
	
	FrameBufferObject& FrameBufferObject::operator=(FrameBufferObject&& other) noexcept {
		if (this->m_fbo != 0) {
			glDeleteFramebuffers(1, &(this->m_fbo));
		}
		
		move_r(this->m_fbo, other.m_fbo);
		
		return *this;
	}
	
	uint FrameBufferObject::getDefaultWidth() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->m_fbo, GL_FRAMEBUFFER_DEFAULT_WIDTH, &value);
		
		return static_cast<uint>(value);
	}
	
	uint FrameBufferObject::getDefaultHeight() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->m_fbo, GL_FRAMEBUFFER_DEFAULT_HEIGHT, &value);
		
		return static_cast<uint>(value);
	}
	
	uint FrameBufferObject::getDefaultLayers() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->m_fbo, GL_FRAMEBUFFER_DEFAULT_LAYERS, &value);
		
		return static_cast<uint>(value);
	}
	
	uint FrameBufferObject::getDefaultSamples() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->m_fbo, GL_FRAMEBUFFER_DEFAULT_SAMPLES, &value);
		
		return static_cast<uint>(value);
	}
	
	bool FrameBufferObject::hasDefaultFixedSampleLocations() const {
		GLint value;
		
		glGetNamedFramebufferParameteriv(this->m_fbo, GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS, &value);
		
		return (value == GL_TRUE);
	}
	
	void FrameBufferObject::attachDepth(Texture2DHandle& texture) {
		glNamedFramebufferTexture(this->m_fbo, GL_DEPTH_ATTACHMENT, texture.m_resource, 0);
	}
	
	void FrameBufferObject::attachDepth(RenderBuffer& renderBuffer) {
		glNamedFramebufferRenderbuffer(this->m_fbo, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer.m_resource);
	}
	
	void FrameBufferObject::attachStencil(Texture2DHandle& texture) {
		glNamedFramebufferTexture(this->m_fbo, GL_STENCIL_ATTACHMENT, texture.m_resource, 0);
	}
	
	void FrameBufferObject::attachStencil(RenderBuffer& renderBuffer) {
		glNamedFramebufferRenderbuffer(this->m_fbo, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderBuffer.m_resource);
	}
	
	void FrameBufferObject::attachDepthStencil(Texture2DHandle& texture) {
		glNamedFramebufferTexture(this->m_fbo, GL_DEPTH_STENCIL_ATTACHMENT, texture.m_resource, 0);
	}
	
	void FrameBufferObject::attachDepthStencil(RenderBuffer& renderBuffer) {
		glNamedFramebufferRenderbuffer(this->m_fbo, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER,
									   renderBuffer.m_resource);
	}
	
	void FrameBufferObject::attachColor(const string& name, Texture2DHandle& texture) {
		const GLenum attachment = getColorAttachment();
		
		if (attachment != GL_NONE) {
			glNamedFramebufferTexture(this->m_fbo, attachment, texture.m_resource, 0);
			
			attachByName(name, attachment);
		}
	}
	
	void FrameBufferObject::attachColor(const string& name, RenderBuffer& renderBuffer) {
		const GLenum attachment = getColorAttachment();
		
		if (attachment != GL_NONE) {
			glNamedFramebufferRenderbuffer(this->m_fbo, attachment, GL_RENDERBUFFER, renderBuffer.m_resource);
			
			attachByName(name, attachment);
		}
	}
	
	void FrameBufferObject::detachDepth() {
		this->detach(GL_DEPTH_ATTACHMENT);
	}
	
	void FrameBufferObject::detachStencil() {
		this->detach(GL_STENCIL_ATTACHMENT);
	}
	
	void FrameBufferObject::detachDepthStencil() {
		this->detach(GL_DEPTH_STENCIL_ATTACHMENT);
	}
	
	void FrameBufferObject::detachColor(const string& name) {
		GLenum attachment = this->getAttachmentByName(name);
		
		if (attachment != GL_NONE) {
			this->detach(attachment);
			this->detachByName(name);
		}
	}
}
