#pragma once
//
// Created by thejackimonster on 02.04.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Buffer.hpp"
#include "../RenderBuffer.hpp"
#include "../Texture2DHandle.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class FrameBufferObject : public FrameBuffer {
	private:
		GLuint m_fbo;
		
		[[nodiscard]]
		GLenum getColorAttachment() const;
		
		void detach(GLuint attachment);
	
	protected:
		[[nodiscard]]
		GLuint getResource() const override;
	
	public:
		explicit FrameBufferObject();
		
		FrameBufferObject(FrameBufferObject&& other) noexcept;
		
		~FrameBufferObject();
		
		FrameBufferObject& operator=(FrameBufferObject&& other) noexcept;
		
		[[nodiscard]]
		uint getDefaultWidth() const;
		
		[[nodiscard]]
		uint getDefaultHeight() const;
		
		[[nodiscard]]
		uint getDefaultLayers() const;
		
		[[nodiscard]]
		uint getDefaultSamples() const;
		
		[[nodiscard]]
		bool hasDefaultFixedSampleLocations() const;
		
		void attachDepth(Texture2DHandle& texture);
		
		void attachDepth(RenderBuffer& renderBuffer);
		
		void attachStencil(Texture2DHandle& texture);
		
		void attachStencil(RenderBuffer& renderBuffer);
		
		void attachDepthStencil(Texture2DHandle& texture);
		
		void attachDepthStencil(RenderBuffer& renderBuffer);
		
		void attachColor(const string& name, Texture2DHandle& texture);
		
		void attachColor(const string& name, RenderBuffer& renderBuffer);
		
		void detachDepth();
		
		void detachStencil();
		
		void detachDepthStencil();
		
		void detachColor(const string& name);
	};
	
	/** @} */
	
}
