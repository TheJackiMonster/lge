#pragma once

//
// Created by thejackimonster on 03.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/string.hxx"
#include "../memory/vector.hxx"
#include "../system/iostream.hxx"

#include "lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	enum class ShaderType : uint {
		VERTEX_SHADER = GL_VERTEX_SHADER,
		
		TESS_CONTROL_SHADER = GL_TESS_CONTROL_SHADER,
		TESS_EVALUATION_SHADER = GL_TESS_EVALUATION_SHADER,
		
		GEOMETRY_SHADER = GL_GEOMETRY_SHADER,
		
		FRAGMENT_SHADER = GL_FRAGMENT_SHADER,
		
		COMPUTE_SHADER = GL_COMPUTE_SHADER,
		
		NO_SHADER
	};
	
	class Shader {
		friend class ShaderProgram;
	
	private:
		GLuint m_resource;
		bool m_copy_flag;
		
		explicit Shader(GLuint resource);
	
	public:
		explicit Shader(ShaderType type);
		
		Shader(const Shader& other) = delete;
		
		Shader(Shader&& other) noexcept;
		
		~Shader();
		
		Shader& operator=(const Shader& other) = delete;
		
		Shader& operator=(Shader&& other) noexcept;
		
		ShaderType getType() const;
		
		string getSource() const;
		
		void setSource(const string& source);
		
		void clearSource();
		
		void appendSource(const string& source);
		
		bool loadSource(const string& pathm, bool append = false);
		
		bool compile();
	};
	
	ostream& operator<<(ostream& stream, ShaderType type);
	
	/** @} */
	
}
