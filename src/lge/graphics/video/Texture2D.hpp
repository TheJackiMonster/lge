#pragma once

//
// Created by thejackimonster on 26.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENCV)

#include "../Texture2DHandle.hpp"
#include "Descriptor.hpp"
#include "../../lang/string.hxx"

namespace lge {
	
	/** @addtogroup cv_group
	 *  @{
	 */
	
	typedef int video_device;
	
	class VideoTexture : public Texture2DHandle {
	private:
		cv::VideoCapture m_capture;
		
		VideoDescriptor m_desc;
	
	public:
		explicit VideoTexture();
		
		VideoTexture(VideoTexture&& other) noexcept;
		
		~VideoTexture();
		
		VideoTexture& operator=(VideoTexture&& other) noexcept;
		
		[[nodiscard]]
		TextureFormat getFormat() const override;
		
		bool load(const string& path);
		
		bool load(uint deviceId);
		
		void update(float deltaTime);
		
		void reload();
	
	private:
		static vector< video_device > s_devices;
		
		static const vector< int >& listDevices(cv::VideoCapture& capture);
		
		static video_device getDevice(uint deviceId);
		
	};
	
	/** @} */
	
}

#endif
