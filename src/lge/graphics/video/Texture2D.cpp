//
// Created by thejackimonster on 26.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENCV)

#include "Texture2D.hpp"

#include "../../lang/regex.hxx"
#include "../../lang/string.hxx"
#include "../../memory/move.hxx"
#include "../../system/filesystem.hxx"
#include "../../util/algorithm.hxx"

namespace lge {
	
	VideoTexture::VideoTexture():
			Texture2DHandle(),
			m_capture(),
			m_desc() {
		glGenTextures(1, &(this->m_resource));
	}
	
	VideoTexture::VideoTexture(lge::VideoTexture&& other) noexcept :
			Texture2DHandle(std::move(other)),
			m_capture(other.m_capture) {
		move(this->m_desc, other.m_desc);
	}
	
	VideoTexture::~VideoTexture() {
		if (this->m_capture.isOpened()) {
			this->m_capture.release();
		}
	}
	
	VideoTexture& VideoTexture::operator=(VideoTexture&& other) noexcept {
		Texture2DHandle::operator=(std::move(other));
		
		move(this->m_capture, other.m_capture);
		move(this->m_desc, other.m_desc);
		
		return *this;
	}
	
	TextureFormat VideoTexture::getFormat() const {
		return TextureFormat::RGB8UI;
	}
	
	bool VideoTexture::load(const std::string& path) {
		if (!this->m_capture.open(path)) {
			return false;
		}
		
		this->m_desc.init(this->m_capture, false);
		
		return this->m_capture.isOpened();
	}
	
	bool VideoTexture::load(uint deviceId) {
		const vector< int > devices = VideoTexture::listDevices(this->m_capture);
		
		if (deviceId >= devices.size()) {
			return false;
		}
		
		if (!this->m_capture.open(devices[deviceId], cv::CAP_V4L2)) {
			return false;
		}
		
		// Set Full-HD resolution if possible...
		this->m_capture.set(cv::CAP_PROP_FRAME_WIDTH, 1920);
		this->m_capture.set(cv::CAP_PROP_FRAME_HEIGHT, 1080);
		
		this->m_capture.set(cv::CAP_PROP_FPS, 60);
		
		this->m_desc.init(this->m_capture, true);
		
		return this->m_capture.isOpened();
	}
	
	void VideoTexture::update(float deltaTime) {
		if (this->m_capture.isOpened()) {
			this->m_desc.updateState(deltaTime);
			
			if (this->m_desc.getState() == VideoState::READ_FRAME) {
				if (!m_capture.grab()) {
					this->m_desc.setState(VideoState::NO_FRAME);
				}
			}
		} else {
			this->m_desc.setState(VideoState::NO_FRAME);
		}
	}
	
	void VideoTexture::reload() {
		if (this->m_desc.getState() == VideoState::READ_FRAME) {
			cv::Mat frame_flipped;
			
			if (this->m_capture.retrieve(frame_flipped)) {
				cv::Mat frame;
				
				cv::flip(frame_flipped, frame, 0);
				
				frame_flipped.release();
				
				const GLsizei width = frame.size().width;
				const GLsizei height = frame.size().height;
				
				const GLuint previous = this->bindResource(true);
				
				GLenum format = GL_BGR;
				
				if (this->m_desc.isFirstFrame()) {
					GLint internalFormat = TextureFormat_::getId(this->getFormat());
					
					glTexImage2D(
							GL_TEXTURE_2D, 0, internalFormat,
							width, height, 0, format,
							GL_UNSIGNED_BYTE, frame.data
					);
				} else {
					glTexSubImage2D(
							GL_TEXTURE_2D, 0,
							0, 0, width, height,
							format, GL_UNSIGNED_BYTE,
							frame.data
					);
				}
				
				this->unbindResource(previous);
				
				frame.release();
			}
			
			this->m_desc.setState(VideoState::NEXT_FRAME);
		}
	}
}

namespace lge {
	
	vector< int > VideoTexture::s_devices;
	
	const vector< int >& VideoTexture::listDevices(cv::VideoCapture& capture) {
		if (s_devices.empty()) {
			filesystem::path devices_path("/dev/");
			
			regex device_name_format("(video)[0-9]+");
			
			filesystem::directory_iterator entries(devices_path);
			
			for (const auto& entry : entries) {
				const filesystem::path& path = entry.path();
				const string name = path.filename().string();
				
				if (regex_match(name, device_name_format)) {
					video_device device = stoi(name.substr(5));
					
					if (capture.open(device, cv::CAP_V4L2)) {
						s_devices.push_back(device);
						capture.release();
					}
				}
			}
			
			if (!s_devices.empty()) {
				sort(s_devices.begin(), s_devices.end());
			}
		}
		
		return s_devices;
	}
	
	video_device VideoTexture::getDevice(uint deviceId) {
		if (deviceId < s_devices.size()) {
			return s_devices[deviceId];
		} else {
			return -1;
		}
	}
}

#endif
