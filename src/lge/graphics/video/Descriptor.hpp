#pragma once

//
// Created by thejackimonster on 27.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENCV)

#include "../../lang/primitives.hxx"

#include <opencv2/opencv.hpp>

namespace lge {
	
	/** @defgroup cv_group The OpenCV Group
	 *  A group with additional classes and structures using OpenCV.
	 *  @{
	 */
	
	enum class VideoState : uchar {
		NO_FRAME = 0,
		
		FIRST_FRAME = 1,
		NEXT_FRAME = 2,
		
		READ_FRAME = 3,
		SKIP_FRAME = 4
	};
	
	struct VideoDescriptor {
	private:
		ulong m_frames;
		uint m_framerate;
		float m_time;
		
		VideoState m_state;
		
		bool m_first;
	
	public:
		explicit VideoDescriptor();
		
		VideoDescriptor(const VideoDescriptor& other) = default;
		
		VideoDescriptor(VideoDescriptor&& other) = default;
		
		~VideoDescriptor() = default;
		
		VideoDescriptor& operator=(const VideoDescriptor& other) = default;
		
		VideoDescriptor& operator=(VideoDescriptor&& other) = default;

		void init(const cv::VideoCapture& capture, bool stream);
		
		void updateState(float deltaTime);
		
		[[nodiscard]]
		VideoState getState() const;
		
		void setState(VideoState state);
		
		[[nodiscard]]
		bool isFirstFrame() const;
	};
	
	/** @} */
	
}

#endif
