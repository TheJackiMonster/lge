//
// Created by thejackimonster on 27.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#if defined(LGE_USE_OPENCV)

#include "Descriptor.hpp"
#include "../../math/util.hxx"

namespace lge {
	
	VideoDescriptor::VideoDescriptor()
			:
			m_frames(0),
			m_framerate(0),
			m_time(0),
			m_state(VideoState::NO_FRAME),
			m_first(false) {
	}
	
	void VideoDescriptor::init(const cv::VideoCapture& capture, bool stream) {
		if (capture.isOpened()) {
			setState(VideoState::FIRST_FRAME);
			
			const double frame_count = stream? 0 : capture.get(cv::CAP_PROP_FRAME_COUNT);
			
			this->m_frames = max< ulong >(static_cast<ulong>(frame_count), 0);
			this->m_framerate = static_cast<uint>(capture.get(cv::CAP_PROP_FPS));
			this->m_time = static_cast<float>(capture.get(cv::CAP_PROP_POS_MSEC) / 1000);
		}
	}
	
	void VideoDescriptor::updateState(float deltaTime) {
		float next_time = this->m_time + deltaTime;
		
		bool skip_frame = false;
		
		if (this->m_state != VideoState::FIRST_FRAME) {
			const float frame_pos = this->m_time * this->m_framerate;
			const float next_frame_pos = next_time * this->m_framerate;
			
			const auto frame_index = static_cast<ulong>(frame_pos);
			const auto next_frame_index = static_cast<ulong>(next_frame_pos);
			
			skip_frame = (next_frame_index <= frame_index);
			
			if ((this->m_frames > 0) && (next_frame_pos >= this->m_frames)) {
				next_time = (next_frame_pos - this->m_frames) / this->m_framerate;
				
				skip_frame = false;
			}
		}
		
		this->m_time = next_time;
		
		setState(skip_frame? VideoState::SKIP_FRAME : VideoState::READ_FRAME);
	}
	
	VideoState VideoDescriptor::getState() const {
		return this->m_state;
	}
	
	void VideoDescriptor::setState(VideoState state) {
		this->m_state = state;
		
		if (this->m_state == VideoState::FIRST_FRAME) {
			this->m_first = true;
		} else if (this->m_state != VideoState::READ_FRAME) {
			this->m_first = false;
		}
	}
	
	bool VideoDescriptor::isFirstFrame() const {
		return this->m_first;
	}
}

#endif
