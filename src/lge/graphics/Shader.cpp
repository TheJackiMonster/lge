//
// Created by thejackimonster on 03.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Shader.hpp"

#include "../memory/move.hxx"

#include <fstream>

namespace lge {
	
	Shader::Shader(GLuint resource) {
		this->m_resource = resource;
		this->m_copy_flag = true;
	}
	
	Shader::Shader(ShaderType type) {
		this->m_resource = glCreateShader(static_cast<GLint>(type));
		this->m_copy_flag = false;
	}
	
	Shader::Shader(Shader&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
		move_r(this->m_copy_flag, other.m_copy_flag, false);
	}
	
	Shader::~Shader() {
		if ((this->m_resource != 0) && (!this->m_copy_flag)) {
			glDeleteShader(this->m_resource);
		}
	}
	
	Shader& Shader::operator=(Shader&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
		move_r(this->m_copy_flag, other.m_copy_flag, false);
		
		return *this;
	}
	
	ShaderType Shader::getType() const {
		GLint type;
		
		glGetShaderiv(this->m_resource, GL_SHADER_TYPE, &type);
		
		return static_cast<ShaderType>(type);
	}
	
	string Shader::getSource() const {
		GLint length = 0;
		
		glGetShaderiv(this->m_resource, GL_SHADER_SOURCE_LENGTH, &length);
		
		if (length > 1) {
			vector< GLchar > buffer(length - 1);
			
			glGetShaderSource(this->m_resource, length, nullptr, buffer.data());
			
			return string(buffer.data(), length - 1);
		} else {
			return string();
		}
	}
	
	void Shader::setSource(const string& source) {
		const auto length = static_cast<GLint>(source.length());
		
		if (length >= 0) {
			const auto buffer = source.c_str();
			
			glShaderSource(this->m_resource, 1, &buffer, &length);
		}
	}
	
	void Shader::clearSource() {
		const GLint zero_len = 0;
		
		glShaderSource(this->m_resource, 1, nullptr, &zero_len);
	}
	
	void Shader::appendSource(const string& source) {
		this->setSource(this->getSource().append(source));
	}
	
	bool Shader::loadSource(const string& path, bool append) {
		std::ifstream in(path, std::ios::in);
		
		if (in) {
			string source;
			
			in.seekg(0, std::ios::end);
			source.resize(static_cast<ulong>(in.tellg()));
			in.seekg(0, std::ios::beg);
			
			in.read(source.data(), source.size());
			
			in.close();
			
			if (append) {
				this->appendSource(source);
			} else {
				this->setSource(source);
			}
			
			return true;
		} else {
			cerr << "File not found! '" << path << "'" << endl;
			
			return false;
		}
	}
	
	bool Shader::compile() {
		glCompileShader(this->m_resource);
		
		GLint status;
		
		glGetShaderiv(this->m_resource, GL_COMPILE_STATUS, &status);
		
		if (status == GL_FALSE) {
			GLint size = 0;
			
			glGetShaderiv(this->m_resource, GL_INFO_LOG_LENGTH, &size);
			
			if (size > 0) {
				vector< GLchar > log(static_cast<ulong>(size + 1));
				
				glGetShaderInfoLog(this->m_resource, size, &size, log.data());
				
				GLint type;
				
				glGetShaderiv(this->m_resource, GL_SHADER_TYPE, &type);
				
				cerr << "[" << static_cast< ShaderType >(type) << "] " << log.data() << endl;
			}
			
			return false;
		} else {
			return true;
		}
	}
	
	ostream& operator<<(ostream& stream, ShaderType type) {
		switch (type) {
			case ShaderType::VERTEX_SHADER:
				stream << "VERTEX_SHADER";
				break;
			case ShaderType::TESS_CONTROL_SHADER:
				stream << "TESS_CONTROL_SHADER";
				break;
			case ShaderType::TESS_EVALUATION_SHADER:
				stream << "TESS_EVALUATION_SHADER";
				break;
			case ShaderType::GEOMETRY_SHADER:
				stream << "GEOMETRY_SHADER";
				break;
			case ShaderType::FRAGMENT_SHADER:
				stream << "FRAGMENT_SHADER";
				break;
			case ShaderType::COMPUTE_SHADER:
				stream << "COMPUTE_SHADER";
				break;
			default:
				stream << "NO_SHADER";
				break;
		}
		
		return stream;
	}
}
