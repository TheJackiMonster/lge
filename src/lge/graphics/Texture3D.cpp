//
// Created by thejackimonster on 05.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Texture3D.hpp"
#include "vec.hxx"

namespace lge {
	
	TextureFormat Texture3D::getFormat() const {
		return TextureFormat::R16F;
	}
	
	bool Texture3D::setup(const uvec3& size, const float* data) {
		const GLuint previous = this->bindResource(true);
		
		GLint internalFormat = TextureFormat_::getId(this->getFormat());
		GLenum format = GL_RED;
		
		glTexImage3D(
				GL_TEXTURE_3D, 0, internalFormat,
				size[0], size[1], size[2], 0, format,
				GL_FLOAT, data
		);
		
		this->unbindResource(previous);
		
		return true;
	}

	void Texture3D::getData(uvec3& size, float *data) {
        const GLuint previous = this->bindResource(true);

        if (data == nullptr) {
            int value;

            glGetTexLevelParameteriv(GL_TEXTURE_3D, 0, GL_TEXTURE_WIDTH, &value);
            size[0] = static_cast<uint>(value);

            glGetTexLevelParameteriv(GL_TEXTURE_3D, 0, GL_TEXTURE_HEIGHT, &value);
            size[1] = static_cast<uint>(value);

            glGetTexLevelParameteriv(GL_TEXTURE_3D, 0, GL_TEXTURE_DEPTH, &value);
            size[2] = static_cast<uint>(value);
        }

	    glGetTexImage(GL_TEXTURE_3D, 0, GL_RED, GL_FLOAT, data);

	    this->unbindResource(previous);
	}

}
