#pragma once
//
// Created by thejackimonster on 05.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "texture/Format.hpp"
#include "../lang/primitives.hxx"
#include "../memory/AccessMode.hpp"
#include "../memory/move.hxx"
#include "../memory/vector.hxx"

#include "lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class TextureHandle {
		friend class FrameBufferObject;
	
	protected:
		GLuint m_resource;
		
		[[nodiscard]]
		virtual GLuint bindResource(bool getPrevious) const = 0;
		
		virtual void unbindResource(GLuint previous) const = 0;
	
	public:
		explicit TextureHandle();
		
		TextureHandle(const TextureHandle& other) = delete;
		
		TextureHandle(TextureHandle&& other) noexcept;
		
		~TextureHandle();
		
		TextureHandle& operator=(const TextureHandle& other) = delete;
		
		TextureHandle& operator=(TextureHandle&& other) noexcept;
		
		virtual TextureFormat getFormat() const = 0;
	
	protected:
		static vector< GLuint > s_units;
		static int s_active;
		
		static void bindToUnit(uint unit, const TextureHandle& texture, AccessMode access = AccessMode::NONE);
		
	public:
		static void init();
		
		static uint getActiveUnit();
		
		static void setActiveUnit(uint unit);
		
		static uint getUnit(const TextureHandle& texture, AccessMode access = AccessMode::NONE);
		
	};
	
	/** @} */
	
}
