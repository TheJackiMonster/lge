#pragma once
//
// Created by thejackimonster on 25.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "mat.hxx"
#include "vec.hxx"

/** @addtogroup glsl_group
 *  @{
 */

namespace lge::std140 {
	
	typedef struct { alignas(16) lge::mat< 4, 4 > mat; } mat4x4_T;
	
	typedef mat4x4_T mat4_T;
}

namespace lge::std430 {
	
	typedef struct { alignas(4) float val; } float_t;
	typedef struct { alignas(4) bool val; } bool_t;
	typedef struct { alignas(4) int val; } int_t;
	typedef struct { alignas(4) unsigned int val; } uint_t;
	typedef struct { alignas(8) double val; } double_t;
	
	typedef struct { alignas(8) lge::mat< 2, 2 > mat; } mat2x2_T;
	//typedef struct { alignas(16) lge::mat<2, 3> mat; } mat2x3_T;
	//typedef struct { alignas(16) lge::mat<2, 4> mat; } mat2x4_T;
	
	//typedef struct { alignas(8) lge::mat<3, 2, 4> mat; } mat3x2_T;
	typedef struct { alignas(16) lge::mat< 3, 3, 4 > mat; } mat3x3_T;
	//typedef struct { alignas(16) lge::mat<3, 4, 4> mat; } mat3x4_T;
	
	//typedef struct { alignas(8) lge::mat<4, 2> mat; } mat4x2_T;
	//typedef struct { alignas(16) lge::mat<4, 3> mat; } mat4x3_T;
	typedef struct { alignas(16) lge::mat< 4, 4 > mat; } mat4x4_T;
	
	typedef mat2x2_T mat2_T;
	typedef mat3x3_T mat3_T;
	typedef mat4x4_T mat4_T;
	
	typedef struct { alignas(16) lge::dmat< 2, 2 > mat; } dmat2x2_T;
	//typedef struct { alignas(32) lge::dmat<2, 3> mat; } dmat2x3;
	//typedef struct { alignas(32) lge::dmat<2, 4> mat; } dmat2x4;
	
	//typedef struct { alignas(16) lge::dmat<3, 2> mat; } dmat3x2;
	typedef struct { alignas(32) lge::dmat< 3, 3 > mat; } dmat3x3_T;
	//typedef struct { alignas(32) lge::dmat<3, 4> mat; } dmat3x4;
	
	//typedef struct { alignas(16) lge::dmat<4, 2> mat; } dmat4x2;
	//typedef struct { alignas(32) lge::dmat<4, 3> mat; } dmat4x3;
	typedef struct { alignas(32) lge::dmat< 4, 4 > mat; } dmat4x4_T;
	
	typedef dmat2x2 dmat2_T;
	typedef dmat3x3 dmat3_T;
	typedef dmat4x4 dmat4_T;
	
	typedef struct { alignas(8) lge::vec< 2 > vec; } vec2;
	typedef struct { alignas(16) lge::vec< 3 > vec; } vec3;
	typedef struct { alignas(16) lge::vec< 4 > vec; } vec4;
	
	typedef struct { alignas(8) lge::bvec< 2, 4 > vec; } bvec2;
	typedef struct { alignas(16) lge::bvec< 3, 4 > vec; } bvec3;
	typedef struct { alignas(16) lge::bvec< 4, 4 > vec; } bvec4;
	
	typedef struct { alignas(8) lge::ivec< 2 > vec; } ivec2;
	typedef struct { alignas(16) lge::ivec< 3 > vec; } ivec3;
	typedef struct { alignas(16) lge::ivec< 4 > vec; } ivec4;
	
	typedef struct { alignas(8) lge::uvec< 2 > vec; } uvec2;
	typedef struct { alignas(16) lge::uvec< 3 > vec; } uvec3;
	typedef struct { alignas(16) lge::uvec< 4 > vec; } uvec4;
	
	typedef struct { alignas(16) lge::dvec< 2 > vec; } dvec2;
	typedef struct { alignas(32) lge::dvec< 3 > vec; } dvec3;
	typedef struct { alignas(32) lge::dvec< 4 > vec; } dvec4;
}

/** @} */
