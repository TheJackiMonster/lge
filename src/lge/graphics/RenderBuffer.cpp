//
// Created by thejackimonster on 04.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "RenderBuffer.hpp"

namespace lge {
	
	RenderBuffer::RenderBuffer() {
		glCreateRenderbuffers(1, &(this->m_resource));
	}
	
	RenderBuffer::~RenderBuffer() {
		glDeleteRenderbuffers(1, &(this->m_resource));
	}
	
	bool RenderBuffer::setup(uint width, uint height, uint samples, bool alpha) {
		GLint max_size;
		
		glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &max_size);
		
		bool result = false;
		
		if ((width < max_size) && (height < max_size)) {
			GLenum internalFormat = GL_DEPTH_COMPONENT;// GL_RGB8;
			
			if (alpha) {
				internalFormat = GL_RGBA8;
			}
			
			if (samples > 0) {
				GLint max_samples;
				
				glGetIntegerv(GL_MAX_SAMPLES, &max_samples);
				
				if (samples < max_samples) {
					glNamedRenderbufferStorageMultisample(this->m_resource, samples, internalFormat, width, height);
					
					result = true;
				}
			} else {
				glNamedRenderbufferStorage(this->m_resource, internalFormat, width, height);
				
				result = true;
			}
		}
		
		return result;
	}
	
	uint RenderBuffer::getWidth() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_WIDTH, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getHeight() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_HEIGHT, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getSamples() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_SAMPLES, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getRedSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_RED_SIZE, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getGreenSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_GREEN_SIZE, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getBlueSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_BLUE_SIZE, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getAlphaSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_ALPHA_SIZE, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getDepthSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_DEPTH_SIZE, &value);
		
		return static_cast<uint>(value);
	}
	
	uint RenderBuffer::getStencilSize() const {
		GLint value;
		
		glGetNamedRenderbufferParameteriv(this->m_resource, GL_RENDERBUFFER_STENCIL_SIZE, &value);
		
		return static_cast<uint>(value);
	}
}
