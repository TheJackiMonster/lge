#pragma once

//
// Created by thejackimonster on 04.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../math/vec.hxx"

namespace lge {
	
	/** @addtogroup glsl_group
	 *  @{
	 */
	
	typedef vec< 2 > vec2;
	typedef vec< 3 > vec3;
	typedef vec< 4 > vec4;
	
	template < uint S, uint L = 1 >
	using bvec = vec< S, L, bool >;
	
	typedef bvec< 2 > bvec2;
	typedef bvec< 3 > bvec3;
	typedef bvec< 4 > bvec4;
	
	template < uint S, uint L = 1 >
	using ivec = vec< S, L, int >;
	
	typedef ivec< 2 > ivec2;
	typedef ivec< 3 > ivec3;
	typedef ivec< 4 > ivec4;
	
	template < uint S, uint L = 1 >
	using uvec = vec< S, L, uint >;
	
	typedef uvec< 2 > uvec2;
	typedef uvec< 3 > uvec3;
	typedef uvec< 4 > uvec4;
	
	template < uint S, uint L = 1 >
	using dvec = vec< S, L, double >;
	
	typedef dvec< 2 > dvec2;
	typedef dvec< 3 > dvec3;
	typedef dvec< 4 > dvec4;
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	bvec< S, 1 > equal(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		bvec< S, 1 > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = (vector[i] == other[i]);
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	bvec< S, 1 > lessThan(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		bvec< S, 1 > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = (vector[i] < other[i]);
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	bvec< S, 1 > lessThanEqual(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		bvec< S, 1 > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = (vector[i] <= other[i]);
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	bvec< S, 1 > greaterThan(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		bvec< S, 1 > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = (vector[i] > other[i]);
		}
		
		return result;
	}
	
	template < uint S, uint L = 1, uint K = 1, typename T = float >
	bvec< S, 1 > greaterThanEqual(const vec< S, L, T >& vector, const vec< S, K, T >& other) {
		bvec< S, 1 > result;
		
		for (uint i = 0; i < S; i++) {
			result[i] = (vector[i] >= other[i]);
		}
		
		return result;
	}
	
	template < uint S, uint L = 1 >
	bool all(const bvec< S, L >& vector) {
		bool result = true;
		
		for (uint i = 0; i < S; i++) {
			result &= vector[i];
		}
		
		return result;
	}
	
	template < uint S, uint L = 1 >
	bool any(const bvec< S, L >& vector) {
		bool result = false;
		
		for (uint i = 0; i < S; i++) {
			result |= vector[i];
		}
		
		return result;
	}
	
	/** @} */
	
}
