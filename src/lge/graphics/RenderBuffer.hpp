#pragma once

//
// Created by thejackimonster on 04.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/primitives.hxx"

#include "lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class RenderBuffer {
		friend class FrameBufferObject;
	
	private:
		GLuint m_resource;
	
	public:
		explicit RenderBuffer();
		
		~RenderBuffer();
		
		bool setup(uint width, uint height, uint samples = 0, bool alpha = false);
		
		[[nodiscard]]
		uint getWidth() const;
		
		[[nodiscard]]
		uint getHeight() const;
		
		[[nodiscard]]
		uint getSamples() const;
		
		[[nodiscard]]
		uint getRedSize() const;
		
		[[nodiscard]]
		uint getGreenSize() const;
		
		[[nodiscard]]
		uint getBlueSize() const;
		
		[[nodiscard]]
		uint getAlphaSize() const;
		
		[[nodiscard]]
		uint getDepthSize() const;
		
		[[nodiscard]]
		uint getStencilSize() const;
		
	};
	
	/** @} */
	
}
