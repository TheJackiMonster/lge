//
// Created by thejackimonster on 05.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "TextureHandle.hpp"

#include "../system/assert.hxx"

namespace lge {
	
	TextureHandle::TextureHandle() {
		glGenTextures(1, &(this->m_resource));
	}
	
	TextureHandle::TextureHandle(TextureHandle&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
	}
	
	TextureHandle::~TextureHandle() {
		if (this->m_resource != 0) {
			glDeleteTextures(1, &(this->m_resource));
		}
	}
	
	TextureHandle& TextureHandle::operator=(TextureHandle&& other) noexcept {
		if (this->m_resource != 0) {
			glDeleteTextures(1, &(this->m_resource));
		}
		
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	vector< GLuint > TextureHandle::s_units;
	int TextureHandle::s_active = 0;
	
	void TextureHandle::bindToUnit(uint unit, const TextureHandle& texture, AccessMode access) {
		setActiveUnit(unit);
		texture.bindResource(false);
		
		if (!s_units.empty()) {
			s_units[s_active] = texture.m_resource;
		}
		
		GLenum formatId = TextureFormat_::getId(texture.getFormat());
		GLenum accessId = 0;
		
		switch (access) {
			case AccessMode::READ_ONLY:
				accessId = GL_READ_ONLY;
				break;
			case AccessMode::WRITE_ONLY:
				accessId = GL_WRITE_ONLY;
				break;
			case AccessMode::READ_WRITE:
				accessId = GL_READ_WRITE;
				break;
			default:
				return;
		}
		
		glBindImageTexture(s_active, texture.m_resource, 0, GL_TRUE, 0, accessId, formatId);
	}
	
	void TextureHandle::init() {
		if (s_units.empty()) {
			GLint max_units;
			
			glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &max_units);
			
			if (max_units > s_units.size()) {
				s_units.resize(static_cast<size_t>(max_units), 0);
			}
		}
	}
	
	uint TextureHandle::getActiveUnit() {
		return s_active;
	}
	
	void TextureHandle::setActiveUnit(uint unit) {
		if ((unit >= 0) && (unit < s_units.size())) {
			glActiveTexture(static_cast<GLenum>(GL_TEXTURE0 + unit));
			s_active = unit;
		}
	}
	
	uint TextureHandle::getUnit(const TextureHandle& texture, AccessMode access) {
		GLuint unit = 0;
		GLuint free = s_units.size();
		
		while (unit < s_units.size()) {
			if (s_units[unit] == texture.m_resource) {
				break;
			} else
			if ((unit < free) && (s_units[unit] == 0)) {
				free = unit;
			}
			
			unit++;
		}
		
		if (unit >= s_units.size()) {
			assert(free < s_units.size());
			
			bindToUnit(free, texture, access);
			
			unit = free;
		}
		
		assert(unit < s_units.size());
		
		return unit;
	}
	
}
