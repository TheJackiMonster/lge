#pragma once

//
// Created by thejackimonster on 04.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../math/mat.hxx"

namespace lge {
	
	/** @defgroup glsl_group The GLSL Group
	 *  This group is for types and functions inspired by GLSL.
	 *  @{
	 */
	
	typedef mat< 2, 2 > mat2x2;
	typedef mat< 2, 3 > mat2x3;
	typedef mat< 2, 4 > mat2x4;
	
	typedef mat< 3, 2 > mat3x2;
	typedef mat< 3, 3 > mat3x3;
	typedef mat< 3, 4 > mat3x4;
	
	typedef mat< 4, 2 > mat4x2;
	typedef mat< 4, 3 > mat4x3;
	typedef mat< 4, 4 > mat4x4;
	
	typedef mat2x2 mat2;
	typedef mat3x3 mat3;
	typedef mat4x4 mat4;
	
	template < uint M, uint N = M, uint L = N >
	using dmat = mat< M, N, L, double >;
	
	typedef dmat< 2, 2 > dmat2x2;
	typedef dmat< 2, 3 > dmat2x3;
	typedef dmat< 2, 4 > dmat2x4;
	
	typedef dmat< 3, 2 > dmat3x2;
	typedef dmat< 3, 3 > dmat3x3;
	typedef dmat< 3, 4 > dmat3x4;
	
	typedef dmat< 4, 2 > dmat4x2;
	typedef dmat< 4, 3 > dmat4x3;
	typedef dmat< 4, 4 > dmat4x4;
	
	typedef dmat2x2 dmat2;
	typedef dmat3x3 dmat3;
	typedef dmat4x4 dmat4;
	
	/** @} */
	
}