#pragma once

//
// Created by thejackimonster on 03.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Shader.hpp"

#include "../system/iostream.hxx"
#include "shader/Attributes.hpp"
#include "shader/Fragments.hpp"
#include "shader/storage/buffer_block.hxx"
#include "shader/uniform.hxx"
#include "shader/uniform/buffer_block.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class ShaderProgram {
	private:
		GLuint m_resource;
	
	public:
		explicit ShaderProgram();
		
		ShaderProgram(const ShaderProgram& other) = delete;
		
		ShaderProgram(ShaderProgram&& other) noexcept;
		
		~ShaderProgram();
		
		ShaderProgram& operator=(const ShaderProgram& other) = delete;
		
		ShaderProgram& operator=(ShaderProgram&& other) noexcept;
		
		ShaderAttributes& getAttributes();
		
		ShaderFragments& getFragments();
		
		[[nodiscard]]
		const vector< Shader > getAttachedShaders() const;
		
		void attachShader(const Shader& shader);
		
		void detachShader(const Shader& shader);
		
		bool addShader(ShaderType type, const string& path);
		
		bool link();
		
		template < typename T >
		uniform< T > getUniform(const string& name) const {
			return uniform< T >(this->m_resource, name);
		}
		
		template < typename T >
		uniform_buffer_block< T > getUniformBlock(const string& name) const {
			return uniform_buffer_block< T >(this->m_resource, name);
		}
		
		template < typename T >
		shader_storage_buffer_block< T > getStorageBlock(const string& name) const {
			return shader_storage_buffer_block< T >(this->m_resource, name);
		}
		
		bool validate();
		
		void use();
		
	};
	
	/** @} */
	
}
