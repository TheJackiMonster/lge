#pragma once

//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../lang/primitives.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	enum class VertexAttribType : uint {
		UBYTE = 0x01,
		USHORT = 0x02,
		UINT = 0x04,
		ULONG = 0x08,
		
		BYTE = 0x11,
		SHORT = 0x12,
		INT = 0x14,
		LONG = 0x18,
		
		FLOAT = 0x24,
		DOUBLE = 0x28
	};
	
	namespace VertexAttribType_ {
		
		uint getIndex(VertexAttribType type);
		
		uint getSize(VertexAttribType type);
		
		bool isSigned(VertexAttribType type);
		
		bool isFloatingPoint(VertexAttribType type);
		
	}
	
	/** @} */
	
}
