//
// Created by thejackimonster on 09.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Buffer.hpp"

namespace lge {
	
	VertexBuffer::VertexBuffer(const VertexStructure& structure) {
		glCreateBuffers(1, &(this->m_vbo));
		
		this->m_structure = structure;
	}
	
	VertexBuffer::VertexBuffer(VertexBuffer&& other) noexcept {
		move_r(this->m_vbo, other.m_vbo);
		move(this->m_structure, other.m_structure);
	}
	
	VertexBuffer::~VertexBuffer() {
		glDeleteBuffers(1, &(this->m_vbo));
	}
	
	VertexBuffer& VertexBuffer::operator=(VertexBuffer&& other) noexcept {
		move_r(this->m_vbo, other.m_vbo);
		move(this->m_structure, other.m_structure);
		
		return *this;
	}
	
	bool VertexBuffer::setup(const ShaderAttributes& bindings) {
		glBindBuffer(GL_ARRAY_BUFFER, this->m_vbo);
		
		return this->m_structure.setup(bindings);
	}
	
	void VertexBuffer::data(uint amount, const void* data, bool dynamic) {
		glNamedBufferData(
                this->m_vbo,
				static_cast<ulong>(amount) * this->m_structure.getSize(),
				data,
				(dynamic? GL_DYNAMIC_DRAW : GL_STATIC_DRAW)
		);
	}
	
	void VertexBuffer::subData(uint amount, const void* data, uint offset) {
		glNamedBufferSubData(
                this->m_vbo,
				static_cast<ulong>(offset) * this->m_structure.getSize(),
				static_cast<ulong>(amount) * this->m_structure.getSize(),
				data
		);
	}
	
	void VertexBuffer::getData(uint amount, void* data, uint offset) const {
		glGetNamedBufferSubData(
		        this->m_vbo,
                static_cast<ulong>(offset) * this->m_structure.getSize(),
                static_cast<ulong>(amount) * this->m_structure.getSize(),
                data
        );
	}
}
