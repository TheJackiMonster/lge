//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Structure.hpp"

namespace lge {
	
	const GLenum GL_VERTEX_ATTRIB_TYPE_TABLE[] = {
			GL_UNSIGNED_BYTE,
			GL_UNSIGNED_SHORT,
			GL_UNSIGNED_INT,
	
	#if defined(GL_UNSIGNED_INT64_ARB)
			GL_UNSIGNED_INT64_ARB,
	#else
			GL_UNSIGNED_INT,
	#endif
			
			GL_BYTE,
			GL_SHORT,
			GL_INT,
	
	#if defined(GL_UNSIGNED_INT64_ARB)
			GL_INT64_ARB,
	#else
			GL_INT,
	#endif
			
			GL_FLOAT,
			GL_DOUBLE
	};
	
	VertexStructure::VertexStructure(uint count) {
		GLint attrib_count;
		
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &attrib_count);
		
		if ((count > attrib_count) || (count == 0)) {
			this->m_capacity = static_cast<uint>(attrib_count);
		} else {
			this->m_capacity = count;
		}
	}
	
	uint VertexStructure::getSize() const {
		uint size = 0;
		
		for (const auto& attribute : this->m_attributes) {
			size += attribute.getSize();
		}
		
		return size;
	}
	
	void VertexStructure::reset() {
		this->m_attributes.clear();
		this->m_locations.clear();
	}
	
	VertexStructure& VertexStructure::attribute(const string& name, uint size, bool normalized, VertexAttribType type) {
		if (this->m_attributes.size() < this->m_capacity) {
			this->m_attributes.emplace_back(name, size, normalized, type);
		}
		
		return *this;
	}
	
	bool VertexStructure::setup(const ShaderAttributes& bindings) {
		const uint size = this->getSize();
		
		#if defined(GL_MAX_VERTEX_ATTRIB_STRIDE)
		{
			GLint attrib_stride;
			
			glGetIntegerv(GL_MAX_VERTEX_ATTRIB_STRIDE, &attrib_stride);
			
			if (size > attrib_stride) {
				return false;
			}
		}
		#endif
		
		uint offset = 0;
		
		if (!this->m_locations.empty()) {
			this->m_locations.clear();
		}
		
		for (const auto& attribute : this->m_attributes) {
			const int location = bindings.getLocation(attribute.getName());
			
			if (location >= 0) {
				glVertexAttribPointer(
						static_cast<GLuint>(location),
						attribute.getDimension(),
						GL_VERTEX_ATTRIB_TYPE_TABLE[
								VertexAttribType_::getIndex(attribute.getType())
						],
						static_cast<GLboolean>(attribute.isNormalized()? GL_TRUE : GL_FALSE),
						size,
						reinterpret_cast<void*>(offset)
				);
				
				this->m_locations.push_back(location);
			}
			
			offset += attribute.getSize();
		}
		
		return true;
	}
	
	void VertexStructure::enable() const {
		for (const int location : this->m_locations) {
			glEnableVertexAttribArray(static_cast<GLuint>(location));
		}
	}
	
	void VertexStructure::disable() const {
		for (const int location : this->m_locations) {
			glDisableVertexAttribArray(static_cast<GLuint>(location));
		}
	}
}
