#pragma once

//
// Created by thejackimonster on 09.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../memory/move.hxx"
#include "Structure.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class VertexBuffer {
		friend class VertexArray;
	
	private:
		GLuint m_vbo;
		
		VertexStructure m_structure;
		
		bool setup(const ShaderAttributes& bindings);
	
	public:
		explicit VertexBuffer(const VertexStructure& structure);
		
		VertexBuffer(const VertexBuffer& other) = delete;
		
		VertexBuffer(VertexBuffer&& other) noexcept;
		
		~VertexBuffer();
		
		VertexBuffer& operator=(const VertexBuffer& other) = delete;
		
		VertexBuffer& operator=(VertexBuffer&& other) noexcept;
		
		void data(uint amount, const void* data, bool dynamic = false);
		
		void subData(uint amount, const void* data, uint offset = 0);
		
		void getData(uint amount, void* data, uint offset = 0) const;

	};
	
	/** @} */
	
}
