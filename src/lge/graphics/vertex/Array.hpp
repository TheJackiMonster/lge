#pragma once

//
// Created by thejackimonster on 09.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../memory/vector.hxx"

#include "Buffer.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	enum DrawType {
		POINTS = GL_POINTS,
		LINES = GL_LINES,
		LINE_LOOP = GL_LINE_LOOP,
		LINE_STRIP = GL_LINE_STRIP,
		TRIANGLES = GL_TRIANGLES,
		TRIANGLE_STRIP = GL_TRIANGLE_STRIP,
		TRIANGLE_FAN = GL_TRIANGLE_FAN
		
		#if defined(GL_QUADS)
		, QUADS = GL_QUADS
		#endif
		
		#if defined(GL_QUAD_STRIP)
		, QUAD_STRIP = GL_QUAD_STRIP
		#endif
	};
	
	class VertexArray {
	private:
		GLuint m_vao;
		
		vector< VertexBuffer > m_buffers;
	
	public:
		explicit VertexArray();
		
		VertexArray(const VertexArray& other) = delete;
		
		VertexArray(VertexArray&& other) noexcept;
		
		~VertexArray();
		
		VertexArray& operator=(const VertexArray& other) = delete;
		
		VertexArray& operator=(VertexArray&& other) noexcept;
		
		uint countBuffers() const;
		
		void addBuffer(const VertexStructure& structure);
		
		VertexBuffer& getBuffer(uint index);
		
		const VertexBuffer& getBuffer(uint index) const;
		
		VertexBuffer& operator[](uint index);
		
		const VertexBuffer& operator[](uint index) const;
		
		void removeBufferAt(uint index);
		
		void releaseBuffers();
		
		void setup(const ShaderAttributes& bindings);
		
		void draw(DrawType type, uint amount, uint offset = 0) const;
		
	};
	
	/** @} */
	
}
