//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Attribute.hpp"

namespace lge {
	
	VertexAttribute::VertexAttribute(const string& name, uint dimension, bool normalized, VertexAttribType type) {
		this->m_name = name;
		this->m_dimension = dimension;
		this->m_normalized = normalized;
		this->m_type = type;
	}
	
	VertexAttribute::VertexAttribute(const VertexAttribute& other) {
		this->m_name = other.getName();
		this->m_dimension = other.getDimension();
		this->m_normalized = other.isNormalized();
		this->m_type = other.getType();
	}
	
	VertexAttribute::VertexAttribute(VertexAttribute&& other) noexcept {
		this->m_name = other.m_name;
		this->m_dimension = other.m_dimension;
		this->m_normalized = other.m_normalized;
		this->m_type = other.m_type;
	}
	
	VertexAttribute& VertexAttribute::operator=(const VertexAttribute& other) {
		this->m_name = other.getName();
		this->m_dimension = other.getDimension();
		this->m_normalized = other.isNormalized();
		this->m_type = other.getType();
		
		return *this;
	}
	
	VertexAttribute& VertexAttribute::operator=(VertexAttribute&& other) noexcept {
		this->m_name = other.m_name;
		this->m_dimension = other.m_dimension;
		this->m_normalized = other.m_normalized;
		this->m_type = other.m_type;
		
		return *this;
	}
	
	const string& VertexAttribute::getName() const {
		return this->m_name;
	}
	
	uint VertexAttribute::getDimension() const {
		return this->m_dimension;
	}
	
	bool VertexAttribute::isNormalized() const {
		return this->m_normalized;
	}
	
	VertexAttribType VertexAttribute::getType() const {
		return this->m_type;
	}
	
	uint VertexAttribute::getSize() const {
		return VertexAttribType_::getSize(this->m_type) * this->m_dimension;
	}
}
