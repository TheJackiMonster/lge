#pragma once

//
// Created by thejackimonster on 08.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../memory/vector.hxx"
#include "../shader/Attributes.hpp"
#include "Attribute.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class VertexStructure {
	private:
		uint m_capacity;
		
		vector< VertexAttribute > m_attributes;
		vector< int > m_locations;
	
	public:
		explicit VertexStructure(uint count = 0);
		
		VertexStructure(const VertexStructure& other) = default;
		
		VertexStructure(VertexStructure&& other) = default;
		
		~VertexStructure() = default;
		
		VertexStructure& operator=(const VertexStructure& other) = default;
		
		VertexStructure& operator=(VertexStructure&& other) = default;
		
		[[nodiscard]]
		uint getSize() const;
		
		void reset();
		
		VertexStructure& attribute(const string& name, uint size, bool normalized = false, VertexAttribType type = VertexAttribType::FLOAT);
		
		bool setup(const ShaderAttributes& bindings);
		
		void enable() const;
		
		void disable() const;
	
	};
	
	/** @} */
	
}
