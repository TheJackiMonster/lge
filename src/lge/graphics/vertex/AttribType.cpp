//
// Created by thejackimonster on 09.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AttribType.hpp"

namespace lge::VertexAttribType_ {
	
	uint getIndex(VertexAttribType type) {
		const uint high = ((static_cast<uint>(type) & 0xF0u) >> 2u);
		
		uint low = (static_cast<uint>(type) & 0xFu);
		
		uint index = 0;
		
		while (low > 1u) {
			low >>= 1u;
			index++;
		}
		
		if (isFloatingPoint(type)) {
			index -= 2u;
		}
		
		return index + high;
	}
	
	uint getSize(VertexAttribType type) {
		return (static_cast<uint>(type) & 0xFu);
	}
	
	bool isSigned(VertexAttribType type) {
		return (static_cast<uint>(type) & 0x10u) != 0;
	}
	
	bool isFloatingPoint(VertexAttribType type) {
		return (static_cast<uint>(type) & 0x20u) != 0;
	}
	
}
