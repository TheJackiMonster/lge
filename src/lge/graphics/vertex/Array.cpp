//
// Created by thejackimonster on 09.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Array.hpp"

namespace lge {
	
	VertexArray::VertexArray() {
		glCreateVertexArrays(1, &(this->m_vao));
	}
	
	VertexArray::VertexArray(VertexArray&& other) noexcept {
		move_r(this->m_vao, other.m_vao);
		move(this->m_buffers, other.m_buffers);
	}
	
	VertexArray::~VertexArray() {
		this->releaseBuffers();
		
		glDeleteVertexArrays(1, &(this->m_vao));
	}
	
	VertexArray& VertexArray::operator=(VertexArray&& other) noexcept {
		move_r(this->m_vao, other.m_vao);
		move(this->m_buffers, other.m_buffers);
		
		return *this;
	}
	
	uint VertexArray::countBuffers() const {
		return this->m_buffers.size();
	}
	
	void VertexArray::addBuffer(const VertexStructure& structure) {
		this->m_buffers.emplace_back(structure);
	}
	
	VertexBuffer& VertexArray::getBuffer(uint index) {
		return this->m_buffers[index];
	}
	
	const VertexBuffer& VertexArray::getBuffer(uint index) const {
		return this->m_buffers[index];
	}
	
	VertexBuffer& VertexArray::operator[](uint index) {
		return this->getBuffer(index);
	}
	
	const VertexBuffer& VertexArray::operator[](uint index) const {
		return this->getBuffer(index);
	}
	
	void VertexArray::removeBufferAt(uint index) {
		if (index < this->m_buffers.size()) {
			glBindVertexArray(this->m_vao);
			
			this->m_buffers[index].m_structure.disable();
			
			this->m_buffers.erase(this->m_buffers.begin() + index);
		}
	}
	
	void VertexArray::releaseBuffers() {
		glBindVertexArray(this->m_vao);
		
		for (auto& buffer : this->m_buffers) {
			buffer.m_structure.disable();
		}
		
		this->m_buffers.clear();
	}
	
	void VertexArray::setup(const ShaderAttributes& bindings) {
		glBindVertexArray(this->m_vao);
		
		for (auto& buffer : this->m_buffers) {
			if (buffer.setup(bindings)) {
				buffer.m_structure.enable();
			}
		}
	}
	
	void VertexArray::draw(DrawType type, uint amount, uint offset) const {
		glBindVertexArray(this->m_vao);
		
		glDrawArrays(type, offset, amount);
	}
	
}
