#pragma once

//
// Created by thejackimonster on 24.01.18.
//
// Copyright (c) 2018-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Texture2DHandle.hpp"
#include "../lang/string.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class Texture2D : public Texture2DHandle {
	private:
		TextureFormat m_internalFormat;
		
		void copyData(const Texture2D& other);
	
	public:
		explicit Texture2D();
		
		Texture2D(const Texture2D& other);
		
		Texture2D(Texture2D&& other) noexcept;
		
		Texture2D& operator=(const Texture2D& other);
		
		Texture2D& operator=(Texture2D&& other) noexcept;
		
		[[nodiscard]]
		TextureFormat getFormat() const override;
		
		[[nodiscard]]
		bool hasAlpha() const;
		
		bool setup(uint width, uint height, uint samples = 0, bool alpha = false);
		
		bool load(const string& path, bool alpha = false);

	};
	
	/** @} */
	
}
