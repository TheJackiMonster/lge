//
// Created by thejackimonster on 03.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "ShaderProgram.hpp"

#include "../memory/move.hxx"
#include "../system/assert.hxx"

namespace lge {
	
	ShaderProgram::ShaderProgram() {
		this->m_resource = glCreateProgram();
	}
	
	ShaderProgram::ShaderProgram(ShaderProgram&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
	}
	
	ShaderProgram::~ShaderProgram() {
		if (this->m_resource != 0) {
			glDeleteProgram(this->m_resource);
		}
	}
	
	ShaderProgram& ShaderProgram::operator=(ShaderProgram&& other) noexcept {
		move_r(this->m_resource, other.m_resource);
		
		return *this;
	}
	
	ShaderAttributes& ShaderProgram::getAttributes() {
		return assert_cast< ShaderAttributes >(this->m_resource);
	}
	
	ShaderFragments& ShaderProgram::getFragments() {
		return assert_cast< ShaderFragments >(this->m_resource);
	}
	
	const vector< Shader > ShaderProgram::getAttachedShaders() const {
		GLint count = 0;
		
		glGetProgramiv(this->m_resource, GL_ATTACHED_SHADERS, &count);
		
		vector< Shader > shaders;
		
		if (count > 0) {
			vector< GLuint > resources(count);
			
			glGetAttachedShaders(this->m_resource, count, nullptr, resources.data());
			
			for (int i = 0; i < count; i++) {
				shaders.push_back(Shader(resources[i]));
			}
		}
		
		return shaders;
	}
	
	void ShaderProgram::attachShader(const Shader& shader) {
		glAttachShader(this->m_resource, shader.m_resource);
	}
	
	void ShaderProgram::detachShader(const Shader& shader) {
		glDetachShader(this->m_resource, shader.m_resource);
	}
	
	bool ShaderProgram::addShader(lge::ShaderType type, const std::string& path) {
		Shader shader(type);
		
		if ((shader.loadSource(path)) && (shader.compile())) {
			this->attachShader(shader);
			return true;
		} else {
			return false;
		}
	}
	
	bool ShaderProgram::link() {
		glLinkProgram(this->m_resource);
		
		GLint status = GL_FALSE;
		
		glGetProgramiv(this->m_resource, GL_LINK_STATUS, &status);
		
		if (status == GL_FALSE) {
			GLsizei size;
			
			glGetProgramiv(this->m_resource, GL_INFO_LOG_LENGTH, &size);
			
			if (size > 0) {
				vector< GLchar > log(static_cast<ulong>(size + 1));
				
				glGetProgramInfoLog(this->m_resource, size, &size, log.data());
				
				cerr << log.data() << endl;
			}
			
			return false;
		} else {
			return true;
		}
	}
	
	bool ShaderProgram::validate() {
		glValidateProgram(this->m_resource);
		
		GLint status;
		
		glGetProgramiv(this->m_resource, GL_VALIDATE_STATUS, &status);
		
		if (status == GL_FALSE) {
			GLsizei size;
			
			glGetProgramiv(this->m_resource, GL_INFO_LOG_LENGTH, &size);
			
			if (size > 0) {
				vector< GLchar > log(static_cast<ulong>(size + 1));
				
				glGetProgramInfoLog(this->m_resource, size, &size, log.data());
				
				cerr << log.data() << endl;
			}
			
			return false;
		} else {
			return true;
		}
	}
	
	void ShaderProgram::use() {
		glUseProgram(this->m_resource);
	}
	
}
