//
// Created by thejackimonster on 24.01.18.
//
// Copyright (c) 2018-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Texture2D.hpp"

#if defined(LGE_USE_OPENCV)
#include <opencv2/opencv.hpp>
#elif defined(LGE_USE_PNG)
#include <png.h>
#endif

namespace lge {
	
	void Texture2D::copyData(const Texture2D& other) {
		const GLsizei width = other.getWidth();
		const GLsizei height = other.getHeight();
		
		GLint internalFormat = TextureFormat_::getId(other.getFormat());
		GLenum format = GL_RGB;
		
		cv::Mat img;
		
		this->m_internalFormat = other.getFormat();
		
		if (this->hasAlpha()) {
			format = GL_RGBA;
			
			img.create(height, width, CV_8UC4);
		} else {
			img.create(height, width, CV_8UC3);
		}
		
		const GLuint previous = other.bindResource(true);
		
		glGetTexImage(GL_TEXTURE_2D, 0, format, GL_UNSIGNED_BYTE, img.data);
		
		this->bindResource(false);
		
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, GL_UNSIGNED_BYTE, img.data);
		
		glGenerateMipmap(GL_TEXTURE_2D);
		
		this->unbindResource(previous);
		
		img.release();
	}
	
	Texture2D::Texture2D():
			Texture2DHandle() {
		this->m_internalFormat = TextureFormat::RGB8UI;
	}
	
	Texture2D::Texture2D(const Texture2D& other):
			Texture2DHandle() {
		this->copyData(other);
	}
	
	Texture2D::Texture2D(lge::Texture2D&& other) noexcept :
			Texture2DHandle(std::move(other)) {
		this->m_internalFormat = other.m_internalFormat;
	}
	
	Texture2D& Texture2D::operator=(const Texture2D& other) {
		this->copyData(other);
		
		return *this;
	}
	
	Texture2D& Texture2D::operator=(Texture2D&& other) noexcept {
		Texture2DHandle::operator=(std::move(other));
		
		this->m_internalFormat = other.m_internalFormat;
		
		return *this;
	}
	
	TextureFormat Texture2D::getFormat() const {
		return this->m_internalFormat;
	}
	
	bool Texture2D::hasAlpha() const {
		return TextureFormat_::hasAlpha(this->m_internalFormat);
	}
	
	bool Texture2D::setup(uint width, uint height, uint samples, bool alpha) {
		this->m_internalFormat = (alpha? TextureFormat::RGBA8UI : TextureFormat::RGB8UI);
		
		const GLuint previous = this->bindResource(true);
		
		GLenum internalFormat = TextureFormat_::getId(this->m_internalFormat);
		GLenum format = (alpha? GL_RGBA : GL_RGB);
		
		glTexImage2D(
				GL_TEXTURE_2D, 0, internalFormat,
				width, height, 0, format,
				GL_UNSIGNED_BYTE, nullptr
		);
		
		this->unbindResource(previous);
		
		return true;
	}
	
	bool Texture2D::load(const string& path, bool alpha) {
		#if defined(LGE_USE_OPENCV)
		cv::Mat img = cv::imread(path, alpha? cv::IMREAD_UNCHANGED : cv::IMREAD_ANYCOLOR);
		
		if (img.empty()) {
			return false;
		}
		
		const GLsizei width = img.cols;
		const GLsizei height = img.rows;
		
		this->m_internalFormat = (alpha? TextureFormat::RGBA8UI : TextureFormat::RGB8UI);
		
		const GLuint previous = this->bindResource(true);
		
		GLenum format = GL_RGB;
		
		if (img.type() == CV_8UC1) {
			this->m_internalFormat = TextureFormat::R8UI;
			format = GL_RED;
		}
		
		GLint internalFormat = TextureFormat_::getId(this->m_internalFormat);
		
		if (alpha) {
			format = GL_RGBA;
		}
		
		glTexImage2D(
				GL_TEXTURE_2D, 0, internalFormat,
				width, height, 0, format,
				GL_UNSIGNED_BYTE, img.data
		);
		
		glGenerateMipmap(GL_TEXTURE_2D);
		
		this->unbindResource(previous);
		
		img.release();
		
		return true;
		//#elif defined(LGE_USE_PNG)
		#else
		return false;
		#endif
	}
}
