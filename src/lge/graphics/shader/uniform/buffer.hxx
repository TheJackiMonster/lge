#pragma once
//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../../lang/string.hxx"
#include "../../../lang/templates.hxx"
#include "BufferBinding.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	template < typename T >
	struct uniform_buffer {
		template < typename T0 >
		friend
		struct uniform_buffer_block;
	
	private:
		GLuint m_ubo;
	
	public:
		uniform_buffer() {
			glGenBuffers(1, &(this->m_ubo));
			glBindBuffer(GL_UNIFORM_BUFFER, this->m_ubo);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(T), nullptr, GL_DYNAMIC_DRAW);
		}
		
		explicit uniform_buffer(typename conditional< is_arithmetic< T >::value, T, const T& >::type value) {
			glGenBuffers(1, &(this->m_ubo));
			glBindBuffer(GL_UNIFORM_BUFFER, this->m_ubo);
			glBufferData(GL_UNIFORM_BUFFER, sizeof(T), &(value), GL_DYNAMIC_DRAW);
		}
		
		uniform_buffer(const uniform_buffer< T >& other) = delete;
		
		uniform_buffer(uniform_buffer< T >&& other) noexcept {
			move_r(this->m_ubo, other.m_ubo);
		}
		
		~uniform_buffer() {
			glDeleteBuffers(1, &(this->m_ubo));
		}
		
		uniform_buffer& operator=(const uniform_buffer< T >& other) = delete;
		
		uniform_buffer& operator=(uniform_buffer< T >&& other) noexcept {
			move_r(this->m_ubo, other.m_ubo);
			
			return *this;
		}
		
		inline uniform_buffer< T >&
		operator=(typename conditional< is_arithmetic< T >::value, T, const T& >::type value) {
			glBindBuffer(GL_UNIFORM_BUFFER, this->m_ubo);
			*(reinterpret_cast<T*>(glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY))) = value;
			glUnmapBuffer(GL_UNIFORM_BUFFER);
			return *this;
		}
		
		inline void bind(uint binding = 0) {
			UniformBufferBinding::bind(this->m_ubo, binding);
		}
		
		inline uint getBinding() const {
			return UniformBufferBinding::getBinding(this->m_ubo);
		}
		
	};
	
	/** @} */
	
}
