//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "BufferBinding.hpp"

#include "../../../system/assert.hxx"

namespace lge {
	
	vector< GLuint > UniformBufferBinding::s_bindings;
	
	void UniformBufferBinding::init() {
		if (s_bindings.empty()) {
			GLint max_bindings;
			
			glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &max_bindings);
			
			if (max_bindings > s_bindings.size()) {
				s_bindings.resize(static_cast<size_t>(max_bindings), 0);
			}
		}
	}
	
	void UniformBufferBinding::bind(GLuint ubo, GLuint binding) {
		if ((binding >= 0) && (binding < s_bindings.size())) {
			glBindBufferBase(GL_UNIFORM_BUFFER, binding, ubo);
			s_bindings[binding] = ubo;
		}
	}
	
	GLuint UniformBufferBinding::getBinding(GLuint ubo, bool autoBinding) {
		GLuint binding = 0;
		GLuint free = s_bindings.size();
		
		while (binding < s_bindings.size()) {
			if (s_bindings[binding] == ubo) {
				break;
			} else
			if ((binding < free) && (s_bindings[binding] == 0)) {
				free = binding;
			}
			
			binding++;
		}
		
		if ((autoBinding) && (binding >= s_bindings.size())) {
			assert(free < s_bindings.size());
			
			bind(ubo, free);
			
			binding = free;
		}
		
		assert(binding < s_bindings.size());
		
		return binding;
	}
}
