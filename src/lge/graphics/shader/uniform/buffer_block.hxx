#pragma once

//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "buffer.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	template < typename T >
	struct uniform_buffer_block {
		friend class ShaderProgram;
	
	private:
		GLuint m_program;
		GLint m_block_index;
		
		explicit uniform_buffer_block(GLuint program, const string& name) {
			this->m_program = program;
			this->m_block_index = glGetUniformBlockIndex(this->m_program, name.c_str());
		}
	
	public:
		uniform_buffer_block(const uniform_buffer_block< T >& other) = delete;
		
		uniform_buffer_block(uniform_buffer_block< T >&& other) noexcept = default;
		
		~uniform_buffer_block() = default;
		
		uniform_buffer_block& operator=(const uniform_buffer_block< T >& other) = delete;
		
		uniform_buffer_block& operator=(uniform_buffer_block< T >&& other) noexcept = default;
		
		inline void connect(uniform_buffer< T >& buffer) {
			glUniformBlockBinding(this->m_program, this->m_block_index,
					UniformBufferBinding::getBinding(buffer.m_ubo, true)
			);
		}
	};
	
	/** @} */
	
}
