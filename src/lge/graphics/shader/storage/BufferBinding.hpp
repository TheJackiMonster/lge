#pragma once
//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../../memory/vector.hxx"

#include "../../lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	class ShaderStorageBufferBinding {
		template < typename T >
		friend
		struct shader_storage_buffer;
		
		template < typename T >
		friend
		struct shader_storage_buffer_block;
	
	public:
		ShaderStorageBufferBinding() = delete;
		
		ShaderStorageBufferBinding(const ShaderStorageBufferBinding& other) = delete;
		
		ShaderStorageBufferBinding(ShaderStorageBufferBinding&& other) = delete;
		
		~ShaderStorageBufferBinding() = delete;
		
		ShaderStorageBufferBinding& operator=(const ShaderStorageBufferBinding& other) = delete;
		
		ShaderStorageBufferBinding& operator=(ShaderStorageBufferBinding&& other) = delete;
	
		static void init();
		
	private:
		static vector< GLuint > s_bindings;
		
		static void bind(GLuint ssbo, GLuint binding);
		
		static GLuint getBinding(GLuint ssbo, bool autoBinding = false);
		
	};
	
	/** @} */
	
}
