#pragma once

//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "buffer.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	template < typename T >
	struct shader_storage_buffer_block {
		friend class ShaderProgram;
	
	private:
		GLuint m_program;
		GLint m_block_index;
		
		explicit shader_storage_buffer_block(GLuint program, const string& name) {
			this->m_program = program;
			this->m_block_index = glGetProgramResourceIndex(this->m_program, GL_SHADER_STORAGE_BLOCK, name.c_str());
		}
	
	public:
		shader_storage_buffer_block(const shader_storage_buffer_block< T >& other) = delete;
		
		shader_storage_buffer_block(shader_storage_buffer_block< T >&& other) noexcept = default;
		
		~shader_storage_buffer_block() = default;
		
		shader_storage_buffer_block& operator=(const shader_storage_buffer_block< T >& other) = delete;
		
		shader_storage_buffer_block& operator=(shader_storage_buffer_block< T >&& other) noexcept = default;
		
		inline void connect(const shader_storage_buffer< T >& buffer) {
			glShaderStorageBlockBinding(this->m_program, this->m_block_index,
					ShaderStorageBufferBinding::getBinding(buffer.m_ssbo, true)
			);
		}
		
	};
	
	/** @} */
	
}
