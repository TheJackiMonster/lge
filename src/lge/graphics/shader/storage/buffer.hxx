#pragma once
//
// Created by thejackimonster on 23.09.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../../lang/string.hxx"
#include "../../../lang/templates.hxx"
#include "../../../memory/move.hxx"
#include "BufferBinding.hpp"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	template < typename T >
	struct shader_storage_buffer_element {
		template < typename T0 >
		friend
		struct shader_storage_buffer;
	
	private:
		GLuint m_ssbo;
		uint m_index;
		
		explicit shader_storage_buffer_element(GLuint ssbo, uint index):
				m_ssbo(ssbo),
				m_index(index) {}
	
	public:
		shader_storage_buffer_element(const shader_storage_buffer_element< T >& other) = delete;
		
		shader_storage_buffer_element(shader_storage_buffer_element< T >&& other) = default;
		
		~shader_storage_buffer_element() = default;
		
		shader_storage_buffer_element& operator=(const shader_storage_buffer_element< T >& other) = delete;
		
		shader_storage_buffer_element& operator=(shader_storage_buffer_element< T >&& other) = default;
		
		inline T operator()() const {
			T result = *(reinterpret_cast<T*>(glMapNamedBufferRange(this->m_ssbo, sizeof(T) * this->m_index, sizeof(T), GL_MAP_READ_BIT)));
			glUnmapNamedBuffer(this->m_ssbo);
			return result;
		}
		
		inline shader_storage_buffer_element< T >&
		operator=(typename conditional< is_arithmetic< T >::value, T, const T& >::type value) {
            *(reinterpret_cast<T*>(glMapNamedBufferRange(this->m_ssbo, sizeof(T) * this->m_index, sizeof(T), GL_MAP_WRITE_BIT))) = value;
            glUnmapNamedBuffer(this->m_ssbo);
			return *this;
		}
		
	};
	
	template < typename T >
	struct shader_storage_buffer {
		template < typename T0 >
		friend
		struct shader_storage_buffer_block;
	
	private:
		GLuint m_ssbo;
		uint m_size;
		
	public:
		shader_storage_buffer(uint size = 1) {
			glCreateBuffers(1, &(this->m_ssbo));
			this->data(size, nullptr, true);
		}
		
		shader_storage_buffer(const shader_storage_buffer< T >& other) = delete;
		
		shader_storage_buffer(shader_storage_buffer< T >&& other) noexcept {
			move_r(this->m_ssbo, other.m_ssbo);
			move_r(this->m_size, other.m_size);
		}
		
		~shader_storage_buffer() {
			glDeleteBuffers(1, &(this->m_ssbo));
		}
		
		shader_storage_buffer& operator=(const shader_storage_buffer< T >& other) = delete;
		
		shader_storage_buffer& operator=(shader_storage_buffer< T >&& other) noexcept {
			move_r(this->m_ssbo, other.m_ssbo);
			move_r(this->m_size, other.m_size);
			
			return *this;
		}
		
		inline uint size() const {
			return this->m_size;
		}
		
		inline shader_storage_buffer_element< T > operator[](uint index) {
			return shader_storage_buffer_element< T >(this->m_ssbo, index);
		}
		
		inline const shader_storage_buffer_element< T > operator[](uint index) const {
			return shader_storage_buffer_element< T >(this->m_ssbo, index);
		}
		
		inline void bind(uint binding = 0) const {
			ShaderStorageBufferBinding::bind(this->m_ssbo, binding);
		}
		
		inline void swapBinding(const shader_storage_buffer< T >& other) const {
			uint swap = ShaderStorageBufferBinding::getBinding(this->m_ssbo, true);
			
			this->bind(other.getBinding());
			other.bind(swap);
		}
		
		inline void data(uint amount, const T* data, bool dynamic) {
			this->m_size = amount;
			
			glNamedBufferData(this->m_ssbo, sizeof(T) * this->m_size, data, dynamic? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
		}

		inline void subData(uint amount, const T* data, uint offset = 0) {
            glNamedBufferSubData(
                    this->m_ssbo,
                    static_cast<ulong>(offset) * sizeof(T),
                    static_cast<ulong>(amount) * sizeof(T),
                    data
            );
		}
		
		inline void getData(uint amount, T* data, uint offset = 0) const {
			glGetNamedBufferSubData(
					this->m_ssbo,
					static_cast<ulong>(offset) * sizeof(T),
					static_cast<ulong>(amount) * sizeof(T),
					data
			);
		}
		
		[[nodiscard]]
		inline uint getBinding() const {
			return ShaderStorageBufferBinding::getBinding(this->m_ssbo);
		}
		
	};
	
	/** @} */
	
}
