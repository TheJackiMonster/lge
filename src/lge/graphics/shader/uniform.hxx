#pragma once

//
// Created by thejackimonster on 05.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../lang/string.hxx"
#include "../Texture2DHandle.hpp"
#include "../Texture3DHandle.hpp"
#include "../mat.hxx"
#include "../vec.hxx"

#include "../lib.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */
	
	template < typename T >
	struct uniform {
		friend class ShaderProgram;
	
	private:
		GLuint m_program;
		GLint m_location;
		
		AccessMode m_access;
		
		explicit uniform(GLuint program, const string& name) {
			this->m_program = program;
			this->m_location = glGetUniformLocation(this->m_program, name.c_str());
			
			this->m_access = AccessMode::NONE;
		}
	
	public:
		uniform(const uniform< T >& other) = delete;
		
		uniform(uniform< T >&& other) = default;
		
		~uniform() = default;
		
		uniform& operator=(const uniform< T >& other) = delete;
		
		uniform& operator=(uniform< T >&& other) = default;
		
		inline uniform< T >& operator=(typename conditional< is_arithmetic< T >::value, T, const T& >::type value) {
			return *this;
		}
		
		inline void setAccessMode(AccessMode access) {
			this->m_access = access;
		}
		
		inline AccessMode getAccessMode() const {
			return this->m_access;
		}
		
	};
	
	template <>
	inline uniform< uint >& uniform< uint >::operator=(uint value) {
		glProgramUniform1ui(this->m_program, this->m_location, value);
		return *this;
	}
	
	template <>
	inline uniform< int >& uniform< int >::operator=(int value) {
		glProgramUniform1i(this->m_program, this->m_location, value);
		return *this;
	}
	
	template <>
	inline uniform< float >& uniform< float >::operator=(float value) {
		glProgramUniform1f(this->m_program, this->m_location, value);
		return *this;
	}
	
	template <>
	inline uniform< double >& uniform< double >::operator=(double value) {
		glProgramUniform1d(this->m_program, this->m_location, value);
		return *this;
	}
	
	template <>
	inline uniform< uvec2 >& uniform< uvec2 >::operator=(const uvec2& vector) {
		glProgramUniform2uiv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< uvec3 >& uniform< uvec3 >::operator=(const uvec3& vector) {
		glProgramUniform3uiv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< uvec4 >& uniform< uvec4 >::operator=(const uvec4& vector) {
		glProgramUniform4uiv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< ivec2 >& uniform< ivec2 >::operator=(const ivec2& vector) {
		glProgramUniform2iv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< ivec3 >& uniform< ivec3 >::operator=(const ivec3& vector) {
		glProgramUniform3iv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< ivec4 >& uniform< ivec4 >::operator=(const ivec4& vector) {
		glProgramUniform4iv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< vec2 >& uniform< vec2 >::operator=(const vec2& vector) {
		glProgramUniform2fv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< vec3 >& uniform< vec3 >::operator=(const vec3& vector) {
		glProgramUniform3fv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< vec4 >& uniform< vec4 >::operator=(const vec4& vector) {
		glProgramUniform4fv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< dvec2 >& uniform< dvec2 >::operator=(const dvec2& vector) {
		glProgramUniform2dv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< dvec3 >& uniform< dvec3 >::operator=(const dvec3& vector) {
		glProgramUniform3dv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< dvec4 >& uniform< dvec4 >::operator=(const dvec4& vector) {
		glProgramUniform4dv(this->m_program, this->m_location, 1, vector.data());
		return *this;
	}
	
	template <>
	inline uniform< mat2 >& uniform< mat2 >::operator=(const mat2& matrix) {
		glProgramUniformMatrix2fv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< mat3 >& uniform< mat3 >::operator=(const mat3& matrix) {
		glProgramUniformMatrix3fv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< mat4 >& uniform< mat4 >::operator=(const mat4& matrix) {
		glProgramUniformMatrix4fv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< dmat2 >& uniform< dmat2 >::operator=(const dmat2& matrix) {
		glProgramUniformMatrix2dv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< dmat3 >& uniform< dmat3 >::operator=(const dmat3& matrix) {
		glProgramUniformMatrix3dv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< dmat4 >& uniform< dmat4 >::operator=(const dmat4& matrix) {
		glProgramUniformMatrix4dv(this->m_program, this->m_location, 1, GL_TRUE, matrix.data());
		return *this;
	}
	
	template <>
	inline uniform< Texture2DHandle >& uniform< Texture2DHandle >::operator=(const Texture2DHandle& texture) {
		glProgramUniform1i(this->m_program, this->m_location, Texture2DHandle::getUnit(texture, this->m_access));
		return *this;
	}
	
	template <>
	inline uniform< Texture3DHandle >& uniform< Texture3DHandle >::operator=(const Texture3DHandle& texture) {
		glProgramUniform1i(this->m_program, this->m_location, Texture3DHandle::getUnit(texture, this->m_access));
		return *this;
	}
	
	/** @} */
	
}
