#pragma once

//
// Created by thejackimonster on 05.12.17.
//
// Copyright (c) 2017-2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "mat.hxx"
#include "vec.hxx"

#include "../math/trigonometry.hxx"

namespace lge {
	
	/** @addtogroup glsl_group
	 *  @{
	 */
	
	template < uint S = 4, uint L = 1, typename T = float >
	mat< S, S, S, T > translate(const vec< S - 1, L, T >& vector) {
		mat< S, S, S, T > result;
		
		uint i, j;
		
		for (i = 0; i < S - 1; i++) {
			result(i, i) = 1;
			
			for (j = 1; j <= S - 1; j++) {
				result((i + j) % S, i) = 0;
			}
			
			result(i, S - 1) = vector[i];
		}
		
		result(S - 1, S - 1) = 1;
		
		return result;
	}
	
	template < typename T = float >
	mat< 3, 3, 3, T > rotate(T radian) {
		mat< 3, 3, 3, T > result;
		
		const T s = sin< T >(radian);
		const T c = cos< T >(radian);
		
		for (uint i = 0; i < 2; i++) {
			result(i, i) = c;
			
			result(i, 2) = 0;
			result(2, i) = 0;
		}
		
		result(0, 1) = -s;
		result(1, 0) = +s;
		
		result(2, 2) = 1;
		
		return result;
	}
	
	template < uint L = 1, typename T = float >
	mat< 4, 4, 4, T > rotate(T radian, const vec< 3, L, T >& axis) {
		mat< 4, 4, 4, T > result;
		
		const vec< 3, 1, T > v = normalize< 3 >(axis);
		
		const T s = sin< T >(radian);
		const T c = cos< T >(radian);
		
		result.template field< 3, 3 >(0, 0) = outerProduct< 3 >(v, v) * (1 - c);
		
		mat< 3, 3, 3, T > S;
		
		for (uint i = 0; i < 3; i++) {
			result(i, i) += c;
			
			result(i, 3) = 0;
			result(3, i) = 0;
			
			S((i + 1) % 3, i) = +v[(i + 2) % 3] * s;
			S((i + 2) % 3, i) = -v[(i + 1) % 3] * s;
			
			S(i, i) = 0;
		}
		
		result.template field< 3, 3 >(0, 0) += S;
		
		result(3, 3) = 1;
		
		return result;
	}
	
	template < uint S = 4, uint L = 1, typename T = float >
	mat< S, S, S, T > scale(const vec< S - 1, L, T >& vector) {
		mat< S, S, S, T > result;
		
		uint i, j;
		
		for (i = 0; i < S - 1; i++) {
			result(i, i) = vector[i];
			
			for (j = 1; j <= S - 1; j++) {
				result((i + j) % S, i) = 0;
			}
			
			result(i, S - 1) = 0;
		}
		
		result(S - 1, S - 1) = 1;
		
		return result;
	}
	
	template < uint S = 4, uint L = 1, typename T = float >
	mat< S, S, S, T > factorize(const mat< S, S, L, T >& transform, T factor) {
		mat< S, S, S, T > result;
		
		if (factor != 1) {
			vec< S - 1, 1, T > scaling;
			
			uint i;
			
			for (i = 0; i < S - 1; i++) {
				vec< S - 1 > axis = transpose< S - 1 >(transform.template row< S - 1 >(i));
				
				scaling[i] = (length< S - 1 >(axis) - 1.0f) * factor + 1.0f;
				result.template row< S - 1 >(i) = transpose< S - 1 >(normalize< S - 1 >(axis));
			}
			
			{
				result.template field< S - 1, S - 1 >(0, 0) = mix(identity< S - 1 >(), mat3(result.template field< S - 1, S - 1 >(0, 0)),
														 factor);
			}
			
			for (i = 0; i < S - 1; i++) {
				result.template row< S - 1 >(i) *= scaling[i];
			}
			
			result.template column< S - 1 >(S - 1) = transform.template column< S - 1 >(S - 1) * factor;
			result.template row(S - 1) = transform.template row(S - 1);
		} else {
			result = transform;
		}
		
		return result;
	}
	
	template < uint L = 1, uint K = 1, uint J = 1, typename T = float >
	mat< 4, 4, 4, T > lookAt(const vec< 3, L, T >& position, const vec< 3, K, T >& center, const vec< 3, J, T >& up) {
		mat< 4, 4, 4, T > result;
		
		auto& rotation = result.template field< 3, 3 >(0, 0);
		
		rotation.column(2) = normalize< 3 >(position - center);
		rotation.column(0) = normalize< 3 >(cross(up, rotation.column(2)));
		rotation.column(1) = cross(rotation.column(2), rotation.column(0));
		
		rotation = transpose(rotation);
		
		result.row(3) = transpose(vec< 4, 1, T >(0, 0, 0, 1));
		result.template column< 3 >(3) = rotation * -position;
		
		return result;
	}
	
	template < typename T = float >
	mat< 4, 4, 4, T > ortho(T left, T right, T bottom, T top, T near, T far) {
		mat< 4, 4, 4, T > result;
		
		uint i, j;
		
		for (i = 0; i < 3; i++) {
			for (j = 1; j <= 3; j++) {
				result((i + j) % 4, i) = 0;
			}
		}
		
		result(0, 3) = (left + right) / (left - right);
		result(1, 3) = (bottom + top) / (bottom - top);
		result(2, 3) = (near + far) / (near - far);
		
		result(0, 0) = 2 / (right - left);
		result(1, 1) = 2 / (top - bottom);
		result(2, 2) = 2 / (near - far);
		
		result(3, 3) = 1;
		
		return result;
	}
	
	template < typename T = float >
	mat< 4, 4, 4, T > frustum(T left, T right, T bottom, T top, T near, T far) {
		mat< 4, 4, 4, T > result;
		
		uint i;
		
		for (i = 0; i < 3; i++) {
			result((1 + i), 0) = 0;
			result((2 + i) % 4, 1) = 0;
			result((3 + i) % 4, 3) = 0;
		}
		
		result(0, 2) = (left + right) / (right - left);
		result(1, 2) = (bottom + top) / (top - bottom);
		
		result(0, 0) = 2 * near / (right - left);
		result(1, 1) = 2 * near / (top - bottom);
		
		result(2, 2) = (near + far) / (near - far);
		result(2, 3) = 2 * near * far / (near - far);
		
		result(3, 2) = -1;
		
		return result;
	}
	
	template < typename T = float >
	mat< 4, 4, 4, T > perspective(T fovy, T aspect, T near, T far) {
		mat< 4, 4, 4, T > result;
		
		uint i;
		
		for (i = 0; i < 3; i++) {
			result((1 + i), 0) = 0;
			result((2 + i) % 4, 1) = 0;
			result((3 + i) % 4, 3) = 0;
		}
		
		result(0, 2) = 0;
		result(1, 2) = 0;
		
		const T f = 1 / tan< T >(fovy / 2);
		
		result(0, 0) = f / aspect;
		result(1, 1) = f;
		
		result(2, 2) = (near + far) / (near - far);
		result(2, 3) = 2 * near * far / (near - far);
		
		result(3, 2) = -1;
		
		return result;
	}
	
	template < typename T >
	mat< 4, 4, 4, T > viewport(T x, T y, T width, T height) {
		mat< 4, 4, 4, T > result;
		
		uint i, j;
		
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				result((1 + i + j) % 4, i) = 0;
			}
		}
		
		result(0, 3) = width / 2 + x;
		result(1, 3) = height / 2 + y;
		result(2, 3) = 1;
		
		result(2, 3) /= 2;
		
		result(0, 0) = width / 2;
		result(1, 1) = height / 2;
		result(2, 2) = 1;
		
		result(2, 2) /= 2;
		
		result(3, 3) = 1;
		
		return result;
	}
	
	/** @} */
	
}
