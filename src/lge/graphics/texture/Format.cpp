//
// Created by thejackimonster on 27.12.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Format.hpp"

#include "../lib.hxx"

namespace lge::TextureFormat_ {

    uint getId(TextureFormat format) {
        switch (format) {
            case TextureFormat::R8I:
                return GL_R8I;
            case TextureFormat::R8UI:
                return GL_R8UI;
            case TextureFormat::R16I:
                return GL_R16I;
            case TextureFormat::R16UI:
                return GL_R16UI;
            case TextureFormat::R32I:
                return GL_R32I;
            case TextureFormat::R32UI:
                return GL_R32UI;
            case TextureFormat::R16F:
                return GL_R16F;
            case TextureFormat::R32F:
                return GL_R32F;

            case TextureFormat::RG8I:
                return GL_RG8I;
            case TextureFormat::RG8UI:
                return GL_RG8UI;
            case TextureFormat::RG16I:
                return GL_RG16I;
            case TextureFormat::RG16UI:
                return GL_RG16UI;
            case TextureFormat::RG32I:
                return GL_RG32I;
            case TextureFormat::RG32UI:
                return GL_RG32UI;
            case TextureFormat::RG16F:
                return GL_RG16F;
            case TextureFormat::RG32F:
                return GL_RG32F;

            case TextureFormat::RGB8I:
                return GL_RGB8I;
            case TextureFormat::RGB8UI:
                return GL_RGB8UI;
            case TextureFormat::RGB16I:
                return GL_RGB16I;
            case TextureFormat::RGB16UI:
                return GL_RGB16UI;
            case TextureFormat::RGB32I:
                return GL_RGB32I;
            case TextureFormat::RGB32UI:
                return GL_RGB32UI;
            case TextureFormat::RGB16F:
                return GL_RGB16F;
            case TextureFormat::RGB32F:
                return GL_RGB32F;

            case TextureFormat::RGBA8I:
                return GL_RGBA8I;
            case TextureFormat::RGBA8UI:
                return GL_RGBA8UI;
            case TextureFormat::RGBA16I:
                return GL_RGBA16I;
            case TextureFormat::RGBA16UI:
                return GL_RGBA16UI;
            case TextureFormat::RGBA32I:
                return GL_RGBA32I;
            case TextureFormat::RGBA32UI:
                return GL_RGBA32UI;
            case TextureFormat::RGBA16F:
                return GL_RGBA16F;
            case TextureFormat::RGBA32F:
                return GL_RGBA32F;

            case TextureFormat::R8_SNORM:
                return GL_R8_SNORM;
            case TextureFormat::R16_SNORM:
                return GL_R16_SNORM;
            case TextureFormat::RG8_SNORM:
                return GL_RG8_SNORM;
            case TextureFormat::RG16_SNORM:
                return GL_RG16_SNORM;
            case TextureFormat::RGB8_SNORM:
                return GL_RGB8_SNORM;
            case TextureFormat::RGB16_SNORM:
                return GL_RGB16_SNORM;
            case TextureFormat::RGBA8_SNORM:
                return GL_RGBA8_SNORM;
            case TextureFormat::RGBA16_SNORM:
                return GL_RGBA16_SNORM;

            default:
                return 0;
        }
    }
	
	bool hasRed(TextureFormat format) {
		return ((0x0F000u & static_cast<uint>(format)) != 0);
    }
	
	bool hasGreen(TextureFormat format) {
		return ((0x00F00u & static_cast<uint>(format)) != 0);
    }
	
    bool hasBlue(TextureFormat format) {
		return ((0x000F0u & static_cast<uint>(format)) != 0);
    }
	
    bool hasAlpha(TextureFormat format) {
		return ((0x0000Fu & static_cast<uint>(format)) != 0);
    }

}