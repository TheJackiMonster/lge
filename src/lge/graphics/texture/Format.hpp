#pragma once
//
// Created by thejackimonster on 25.12.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../../lang/primitives.hxx"

namespace lge {
	
	/** @addtogroup gl_group
	 *  @{
	 */

    enum class TextureFormat : uint {
        R8I         = 0x01000,
        R8UI        = 0x11000,
        R16I        = 0x02000,
        R16UI       = 0x12000,
        R32I        = 0x04000,
        R32UI       = 0x14000,
        R16F        = 0x22000,
        R32F        = 0x24000,

        RG8I        = 0x01100,
        RG8UI       = 0x11100,
        RG16I       = 0x02200,
        RG16UI      = 0x12200,
        RG32I       = 0x04400,
        RG32UI      = 0x14400,
        RG16F       = 0x22200,
        RG32F       = 0x24400,
        
        RGB8I       = 0x01110,
        RGB8UI      = 0x11110,
        RGB16I      = 0x02220,
        RGB16UI     = 0x12220,
        RGB32I      = 0x04440,
        RGB32UI     = 0x14440,
        RGB16F      = 0x22220,
        RGB32F      = 0x24440,

        RGBA8I      = 0x01111,
        RGBA8UI     = 0x11111,
        RGBA16I     = 0x02222,
        RGBA16UI    = 0x12222,
        RGBA32I     = 0x04444,
        RGBA32UI    = 0x14444,
        RGBA16F     = 0x22222,
        RGBA32F     = 0x24444,

        R8_SNORM        = 0x41000,
        R16_SNORM       = 0x42000,
        RG8_SNORM       = 0x41100,
        RG16_SNORM      = 0x42200,
        RGB8_SNORM      = 0x41110,
        RGB16_SNORM     = 0x42220,
        RGBA8_SNORM     = 0x41111,
        RGBA16_SNORM    = 0x42222
    };

    namespace TextureFormat_ {

    	uint getId(TextureFormat format);
	
    	bool hasRed(TextureFormat format);
	
    	bool hasGreen(TextureFormat format);
	
    	bool hasBlue(TextureFormat format);
        
    	bool hasAlpha(TextureFormat format);

    }
    
    /** @} */

}
