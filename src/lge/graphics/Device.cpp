//
// Created by thejackimonster on 26.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Device.hpp"
#include "TextureHandle.hpp"
#include "shader/storage/BufferBinding.hpp"
#include "shader/uniform/BufferBinding.hpp"

#include "lib.hxx"

#if defined(LGE_USE_GLAD)

#include <GLFW/glfw3.h>

namespace lge {
	
	bool GraphicsDevice::init() {
		if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) != 0) {
			cout << "[GLAD] Version: " << GLVersion.major << "." << GLVersion.minor << endl;
			
			TextureHandle::init();
			ShaderStorageBufferBinding::init();
			UniformBufferBinding::init();
			
			return true;
		} else {
			cerr << "[GLAD] Error: Unknown" << endl;
			
			return false;
		}
	}
	
	bool GraphicsDevice::checkErrors(bool debug) {
		const GLenum error = glGetError();
		
		const bool error_found = (error != GL_NO_ERROR);
		
		if ((debug) && (error_found)) {
			switch (error) {
				case GL_INVALID_ENUM:
					cout << "[OpenGL] Error: INVALID_ENUM" << endl;
					break;
				case GL_INVALID_VALUE:
					cout << "[OpenGL] Error: INVALID_VALUE" << endl;
					break;
				case GL_INVALID_OPERATION:
					cout << "[OpenGL] Error: INVALID_OPERATION" << endl;
					break;
				case GL_INVALID_INDEX:
					cout << "[OpenGL] Error: INVALID_INDEX" << endl;
					break;
				case GL_INVALID_FRAMEBUFFER_OPERATION:
					cout << "[OpenGL] Error: INVALID_FRAMEBUFFER_OPERATION" << endl;
					break;
				case GL_OUT_OF_MEMORY:
					cout << "[OpenGL] Error: OUT_OF_MEMORY" << endl;
					break;
				default:
					cout << "[OpenGL] Error: ( code = " << std::hex << error << " )" << endl;
					break;
			}
		}
		
		return error_found;
	}
}

#else
																														
namespace lge {
	
	bool GraphicsDevice::init() {
		const GLenum code = glewInit();
		
		if (code == GLEW_OK) {
			cout << "[GLEW] Version: " << (const char*) glewGetString(GLEW_VERSION) << endl;
			
			ShaderStorageBufferBinding::init();
			UniformBufferBinding::init();
			
			return true;
		} else {
			cerr << "[GLEW] Error: " << glewGetErrorString(code) << endl;
			
			return false;
		}
	}
	
	bool GraphicsDevice::checkErrors(bool debug) {
		const GLenum error = glGetError();
		
		const bool error_found = (error != GL_NO_ERROR);
		
		if ((debug) && (error_found)) {
			cout << "[OpenGL] Error: " << glewGetErrorString(error) << endl;
		}
		
		return error_found;
	}
	
}

#endif

namespace lge {
	
	void GraphicsDevice::setParam(DeviceParameter parameter, bool enabled) {
		auto param_id = static_cast<GLenum>(DeviceParameter_::getId(parameter));
		
		if (glIsEnabled(param_id) != enabled) {
			if (enabled) {
				glEnable(param_id);
			} else {
				glDisable(param_id);
			}
		}
	}
	
	bool GraphicsDevice::getParam(lge::DeviceParameter parameter) {
		auto param_id = static_cast<GLenum>(DeviceParameter_::getId(parameter));
		
		return glIsEnabled(param_id);
	}
	
	void GraphicsDevice::setDefaultBlending() {
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	
	void GraphicsDevice::setCullFacing(bool front, bool back) {
		GLenum mode = 0;
		
		if ((front) && (back)) {
			mode = GL_FRONT_AND_BACK;
		} else if (front) {
			mode = GL_FRONT;
		} else if (back) {
			mode = GL_BACK;
		}
		
		if (mode != 0) {
			glCullFace(mode);
		}
	}

	void GraphicsDevice::dispatchCompute(uint sizeX, uint sizeY, uint sizeZ) {
	    glDispatchCompute(sizeX, sizeY, sizeZ);
	}

	void GraphicsDevice::memoryBarrier(uint barrierBits) {
	    glMemoryBarrier(barrierBits);
	}

}
