//
// Created by thejackimonster on 26.02.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Texture2DHandle.hpp"

#include "../system/assert.hxx"

namespace lge {
	
	GLuint Texture2DHandle::bindResource(bool getPrevious) const {
		GLint previous = 0;
		
		if (getPrevious) {
			glGetIntegerv(GL_TEXTURE_BINDING_2D, &previous);
		}
		
		glBindTexture(GL_TEXTURE_2D, this->m_resource);
		
		return static_cast<GLuint>(previous);
	}
	
	void Texture2DHandle::unbindResource(GLuint previous) const {
		glBindTexture(GL_TEXTURE_2D, previous);
	}
	
	void Texture2DHandle::bind(int unit) const {
		Texture2DHandle::bindToUnit(unit, *this);
	}
	
	uint Texture2DHandle::getWidth() const {
		GLint value = 0;
		
		const GLuint previous = this->bindResource(true);
		
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &value);
		
		this->unbindResource(previous);
		
		return static_cast<uint>(value);
	}
	
	uint Texture2DHandle::getHeight() const {
		GLint value = 0;
		
		const GLuint previous = this->bindResource(true);
		
		glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &value);
		
		this->unbindResource(previous);
		
		return static_cast<uint>(value);
	}
	
	void Texture2DHandle::setWrapping(bool repeatX, bool repeatY) {
		const GLuint previous = this->bindResource(true);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeatX? GL_REPEAT : GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeatY? GL_REPEAT : GL_CLAMP_TO_EDGE);
		
		this->unbindResource(previous);
	}
	
	void Texture2DHandle::setMinFilter(bool linear, bool mipmap) {
		const GLuint previous = this->bindResource(true);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
						mipmap? (
								linear? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR
						) : (
								linear? GL_LINEAR : GL_NEAREST
						)
		);
		
		this->unbindResource(previous);
	}
	
	void Texture2DHandle::setMagFilter(bool linear) {
		const GLuint previous = this->bindResource(true);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
						linear? GL_LINEAR : GL_NEAREST
		);
		
		this->unbindResource(previous);
	}
	
}
