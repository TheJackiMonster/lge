//
// Created by thejackimonster on 01.03.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "SpriteBatch.hpp"

#include <iostream>

namespace lge {
	
	SpriteBatch::SpriteBatch() {
		VertexStructure vs(4);
		
		vs.attribute("Bounds", 4);
		vs.attribute("TextureBounds", 4);
		vs.attribute("Color", 4);
		vs.attribute("Depth", 1);
		
		this->m_array.addBuffer(vs);
		
		this->m_count = 0;
		this->m_capacity = 0;
	}
	
	SpriteBatch::~SpriteBatch() {
		this->m_array.releaseBuffers();
	}
	
	void SpriteBatch::setup(const lge::ShaderAttributes& bindings) {
		this->m_array.setup(bindings);
	}
	
	void SpriteBatch::enableSprite(Sprite& sprite) {
		if (sprite.m_identity <= this->m_count) {
			sprite.m_identity = this->getNewIdentity();
			sprite.m_changes = -1;
			sprite.m_changed = true;
		} else {
			auto iterator = std::lower_bound(
					this->m_changedIdentities.begin(),
					this->m_changedIdentities.end(),
					sprite.m_identity
			);
			
			if ((iterator != this->m_changedIdentities.end()) && (*iterator == sprite.m_identity)) {
				this->m_changedIdentities.erase(iterator);
				
				sprite.m_changed = true;
			}
		}
		
		this->m_count++;
	}
	
	void SpriteBatch::updateSprite(Sprite& sprite) {
		if (sprite.m_identity != 0) {
			if (sprite.m_identity > this->m_count) {
				sprite.m_identity = this->getNewIdentity();
				sprite.m_changed = true;
			} else if (this->m_count > this->m_capacity) {
				sprite.m_changed = true;
			}
			
			if (sprite.m_changed) {
				auto iterator = std::lower_bound(
						this->m_updateIdentities.begin(),
						this->m_updateIdentities.end(),
						sprite.m_identity
				);
				
				if ((iterator != this->m_updateIdentities.end()) && (*iterator == sprite.m_identity)) {
					this->m_updateSprites[iterator - this->m_updateIdentities.begin()] = sprite.m_sprite;
				} else {
					this->m_updateSprites.insert(
							this->m_updateSprites.begin() + (iterator - this->m_updateIdentities.begin()),
							sprite.m_sprite
					);
					
					this->m_updateIdentities.insert(iterator, sprite.m_identity);
				}
				
				sprite.m_changes++;
				sprite.m_changed = false;
			}
		}
	}
	
	void SpriteBatch::disableSprite(Sprite& sprite) {
		if (this->m_count > 0) {
			if (sprite.m_identity < this->m_count) {
				auto iterator = std::lower_bound(
						this->m_newIdentities.begin(),
						this->m_newIdentities.end(),
						sprite.m_identity
				);
				
				if ((iterator == this->m_newIdentities.end()) || (*iterator != sprite.m_identity)) {
					this->m_newIdentities.insert(iterator, sprite.m_identity);
				}
			} else if (this->m_newIdentities.size() >= this->m_count) {
				this->m_newIdentities.erase(this->m_newIdentities.begin() + this->m_count - 1,
											this->m_newIdentities.end());
			}
			
			if (sprite.m_changes > 0) {
				auto iterator = std::lower_bound(
						this->m_changedIdentities.begin(),
						this->m_changedIdentities.end(),
						sprite.m_identity
				);
				
				if ((iterator == this->m_changedIdentities.end()) || (*iterator != sprite.m_identity)) {
					this->m_changedIdentities.insert(iterator, sprite.m_identity);
				}
				
				sprite.m_changes = 0;
			}
			
			this->m_count--;
		}
	}
	
	void SpriteBatch::draw() {
		const uint amount = this->m_updateSprites.size();
		
		if (amount > 0) {
			if (amount > this->m_capacity) {
				this->m_capacity = amount;
				
				this->m_array[0].data(amount, this->m_updateSprites.data(), true);
			} else {
				uint index = 0;
				
				while (index < amount) {
					uint count = 1;
					
					while ((index + count < amount) &&
						   (this->m_updateIdentities[index + count] = this->m_updateIdentities[index] + 1)) {
						count++;
					}
					
					this->m_array[0].subData(count, this->m_updateSprites.data() + index,
											 this->m_updateIdentities[index] - 1);
					
					index = index + count;
				}
			}
			
			this->m_updateSprites.clear();
			this->m_updateIdentities.clear();
		}
		
		if (this->m_count > 0) {
			this->m_array.draw(DrawType::POINTS, this->m_count);
		}
	}
	
	uint SpriteBatch::getNewIdentity() {
		uint identity;
		
		if (this->m_newIdentities.size() > 0) {
			identity = this->m_newIdentities[0];
			
			this->m_newIdentities.erase(this->m_newIdentities.begin());
		} else {
			identity = this->m_count + 1;
		}
		
		return identity;
	}
}
