//
// Created by thejackimonster on 03.06.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "Sprite.hpp"

namespace lge {
	
	Sprite::Sprite() {
		this->m_identity = 0;
		this->m_changes = 0;
		this->m_changed = false;
	}
	
	const vec4& Sprite::getBounds() const {
		return this->m_sprite.m_bounds;
	}
	
	const vec2& Sprite::getPosition() const {
		return this->m_sprite.m_bounds.column< 2 >(0, 0);
	}
	
	const vec2& Sprite::getSize() const {
		return this->m_sprite.m_bounds.column< 2 >(0, 2);
	}
	
	const vec4& Sprite::getTextureBounds() const {
		return this->m_sprite.m_textureBounds;
	}
	
	const vec4& Sprite::getColor() const {
		return this->m_sprite.m_color;
	}
	
	float Sprite::getDepth() const {
		return this->m_sprite.m_depth;
	}
	
	void Sprite::setBounds(const vec4& bounds) {
		this->m_sprite.m_bounds = bounds;
	}
	
	void Sprite::setPosition(const vec2& position) {
		this->m_sprite.m_bounds.column< 2 >(0, 0) = position;
		
		this->m_changed = true;
	}
	
	void Sprite::setSize(const vec2& size) {
		this->m_sprite.m_bounds.column< 2 >(0, 2) = size;
		
		this->m_changed = true;
	}
	
	void Sprite::setTextureBounds(const vec4& bounds) {
		this->m_sprite.m_textureBounds = bounds;
		
		this->m_changed = true;
	}
	
	void Sprite::setColor(const vec4& color) {
		this->m_sprite.m_color = color;
		
		this->m_changed = true;
	}
	
	void Sprite::setDepth(float depth) {
		this->m_sprite.m_depth = depth;
		
		this->m_changed = true;
	}
}
