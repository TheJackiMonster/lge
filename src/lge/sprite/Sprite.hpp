#pragma once
//
// Created by thejackimonster on 03.06.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "sprite.hxx"
#include "../memory/vector.hxx"

namespace lge {
	
	/** @addtogroup sprite_group
	 *  @{
	 */
	
	class Sprite {
		friend class SpriteBatch;
	
	private:
		sprite m_sprite;
		
		uint m_identity;
		uint m_changes;
		bool m_changed;
	
	public:
		explicit Sprite();
		
		Sprite(const Sprite& other) = default;
		
		Sprite(Sprite&& other) noexcept = default;
		
		~Sprite() = default;
		
		Sprite& operator=(const Sprite& other) = default;
		
		Sprite& operator=(Sprite&& other) noexcept = default;
		
		[[nodiscard]]
		const vec4& getBounds() const;
		
		[[nodiscard]]
		const vec2& getPosition() const;
		
		[[nodiscard]]
		const vec2& getSize() const;
		
		[[nodiscard]]
		const vec4& getTextureBounds() const;
		
		[[nodiscard]]
		const vec4& getColor() const;
		
		[[nodiscard]]
		float getDepth() const;
		
		void setBounds(const vec4& bounds);
		
		void setPosition(const vec2& position);
		
		void setSize(const vec2& size);
		
		void setTextureBounds(const vec4& bounds);
		
		void setColor(const vec4& color);
		
		void setDepth(float depth);
		
	};
	
	/** @} */
	
}
