#pragma once
//
// Created by thejackimonster on 14.12.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "PrecisionMode.hpp"
#include "RigidBody.hpp"
#include "../graphics/shader/storage/buffer.hxx"
#include "../memory/list.hxx"
#include "../memory/vector.hxx"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	struct t_collision {
		std430::uvec2 m_pair;
		std430::vec2 m_times;
		std430::vec3 m_linear_delta [2];
		std430::vec3 m_angular_delta [2];
		std430::vec3 m_movement [2];
	};
	
	struct t_skip {
		vec3 m_diff_linear;
		vec3 m_diff_angular;
		vec2 m_times;
		uint m_value;
	};
	
	/**
	 * A class handling Rigid-Body-physics for a dynamic amount of objects.
	 * @see RigidBody
	 *
	 * @author TheJackiMonster
	 * @since 14.12.2019
	 */
	class PhysicEngine {
	private:
		vector< RigidBody > m_rigidBodies;
		
		shader_storage_buffer< t_bounds > m_bounds;
		shader_storage_buffer< t_collision > m_collisions;
		shader_storage_buffer< uint > m_indices;
		
		vector< t_bounds > m_bounds_data;
		vector< t_collision > m_collision_data;
		vector< t_skip > m_collision_skip;
		vector< uint > m_idx_data;
		
		vec3 m_gravity;
		float m_time;
	
	public:
		explicit PhysicEngine();
		
		PhysicEngine(const PhysicEngine& other) = delete;
		
		PhysicEngine(PhysicEngine&& other) = delete;
		
		~PhysicEngine();
		
		PhysicEngine& operator=(const PhysicEngine& other) = delete;
		
		PhysicEngine& operator=(PhysicEngine&& other) = delete;

		float update(GraphicsDevice& device, float deltaTime, PrecisionMode mode);
		
		/**
		 * Returns a reference to the list of objects in the engines environment.
		 *
		 * @return reference to list of objects
		 */
		vector< RigidBody >& getRigidBodies();
		
		/**
		 * Returns a constant reference to the list of objects in the engines environment.
		 *
		 * @return constant reference to list of objects
		 */
		[[nodiscard]]
		const vector< RigidBody >& getRigidBodies() const;
		
		void setGravity(const vec3& gravity);
		
		void setGravity(float value);
		
		[[nodiscard]]
		const vec3& getGravity() const;
		
		/**
		 * Adds a new object using a specified Signed-Distance-Field and its model
		 * to this engines environment. Its attributes are mostly normalized:
		 * - center: (0, 0, 0)
		 * - size: 1
		 * - mass: 1
		 * - bounciness: 0
		 * - roughness: 0
		 * - velocity: (0, 0, 0)
		 * - angular_velocity: (0, 0, 0)
		 *
		 * @param sdf Signed-Distance-Field
		 * @return object using Rigid-Body-physics
		 */
		RigidBody& createRigidBody(const SignedDistanceField& sdf);
		
		/**
		 * Removes an object from this engines environment. This method could also deallocate its memory.
		 *
		 * @param body Rigid-Body-object
		 */
		void destroyRigidBody(RigidBody& body);
		
		[[nodiscard]]
		const shader_storage_buffer< t_bounds >& getBounds() const;
		
		[[nodiscard]]
		const shader_storage_buffer< t_collision >& getCollisions() const;
	
	private:
		static list<uint> calculateCollisionWithBounds(GraphicsDevice& device, PhysicEngine& engine, vector<t_collision>& collisions, vector<t_skip>& skips, float deltaTime);
		
		static void calculateCollisionWithParticles(GraphicsDevice& device, PhysicEngine& engine, t_collision& collision, uint index);
		
	};
	
	/** @} */
	
}
