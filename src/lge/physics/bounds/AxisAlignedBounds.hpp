#pragma once
//
// Created by thejackimonster on 19.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../vec.hxx"
#include "../../system/iostream.hxx"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */

    class AxisAlignedBounds {
    private:
    public:
        virtual void moveTo(const vec3& position) = 0;

        void moveBy(const vec3& delta);

        virtual void adaptSize(const vec3& position) = 0;

        [[nodiscard]]
        bool isInside(const vec3& position) const;

        [[nodiscard]]
        bool isInside(const AxisAlignedBounds& other) const;

        [[nodiscard]]
        bool isIntersecting(const AxisAlignedBounds& other) const;

        [[nodiscard]]
        virtual vec3 getMin() const = 0;

        [[nodiscard]]
        virtual vec3 getMax() const = 0;

        [[nodiscard]]
        virtual vec3 getCenter() const = 0;

    };
	
	ostream& operator<<(ostream& stream, const AxisAlignedBounds& bounds);
    
    /** @} */

}
