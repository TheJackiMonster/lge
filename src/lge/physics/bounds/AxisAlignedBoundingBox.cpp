//
// Created by thejackimonster on 12.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AxisAlignedBoundingBox.hpp"

namespace lge {

    AxisAlignedBoundingBox::AxisAlignedBoundingBox(const lge::vec3& bounds_min,
                                                   const lge::vec3& bounds_max) {
        this->resizeTo(bounds_min, bounds_max);
    }

    void AxisAlignedBoundingBox::moveTo(const lge::vec3& position) {
        this->resizeTo(
                m_min / 2 + position - m_max / 2,
                m_max / 2 + position - m_min / 2
        );
    }

    void AxisAlignedBoundingBox::resizeTo(const vec3& bounds_min, const vec3& bounds_max) {
        this->m_min = bounds_min;
        this->m_max = bounds_max;
    }

    void AxisAlignedBoundingBox::adaptSize(const vec3& position) {
        this->resizeTo(min<3, 1>(this->m_min, position), max<3, 1>(this->m_max, position));
    }

    vec3 AxisAlignedBoundingBox::getMin() const {
        return this->m_min;
    }

    vec3 AxisAlignedBoundingBox::getMax() const {
        return this->m_max;
    }

    vec3 AxisAlignedBoundingBox::getCenter() const {
        return (this->m_min + this->m_max) / 2;
    }

}
