//
// Created by thejackimonster on 19.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AxisAlignedBoundingCube.hpp"

namespace lge {

    void AxisAlignedBoundingCube::moveTo(const vec3& position) {
        this->m_center = position;
    }

    void AxisAlignedBoundingCube::resizeTo(float size) {
        this->m_size = size;
    }
    
    void AxisAlignedBoundingCube::resizeBy(float factor) {
		this->m_size *= factor;
    }

    void AxisAlignedBoundingCube::adaptSize(const vec3& position) {
        this->m_size = max< float >(
                this->m_size,
                distance(this->m_center, position)
        );
    }

    void AxisAlignedBoundingCube::ensure(const AxisAlignedBounds& box) {
        this->m_center = box.getCenter();
        this->m_size = max< float >(length(box.getMin()), length(box.getMax()));
    }

    vec3 AxisAlignedBoundingCube::getMin() const {
        return this->m_center - this->m_size;
    }

    vec3 AxisAlignedBoundingCube::getMax() const {
        return this->m_center + this->m_size;
    }

    vec3 AxisAlignedBoundingCube::getCenter() const {
        return this->m_center;
    }

    float AxisAlignedBoundingCube::getSize() const {
        return this->m_size;
    }

}
