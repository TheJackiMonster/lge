#pragma once
//
// Created by thejackimonster on 19.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AxisAlignedBounds.hpp"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */

    class AxisAlignedBoundingCube : public AxisAlignedBounds {
    private:
        vec< 3 > m_center;
        float m_size;

    public:
        explicit AxisAlignedBoundingCube() = default;

        AxisAlignedBoundingCube(const AxisAlignedBoundingCube& other) = default;

        AxisAlignedBoundingCube(AxisAlignedBoundingCube&& other) noexcept = default;

        ~AxisAlignedBoundingCube() = default;

        AxisAlignedBoundingCube& operator=(const AxisAlignedBoundingCube& other) = default;

        AxisAlignedBoundingCube& operator=(AxisAlignedBoundingCube&& other) noexcept = default;

        void moveTo(const vec3& position) override;

        void resizeTo(float size);
        
        void resizeBy(float factor);

        void adaptSize(const vec3& position) override;

        void ensure(const AxisAlignedBounds& box);

        [[nodiscard]]
        vec3 getMin() const override;

        [[nodiscard]]
        vec3 getMax() const override;

        [[nodiscard]]
        vec3 getCenter() const override;

        [[nodiscard]]
        float getSize() const;

    };
    
    /** @} */

}