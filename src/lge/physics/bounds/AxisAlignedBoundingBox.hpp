#pragma once
//
// Created by thejackimonster on 12.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AxisAlignedBounds.hpp"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	class AxisAlignedBoundingBox : public AxisAlignedBounds {
	private:
		vec< 3 > m_min;
		vec< 3 > m_max;
	
	public:
		explicit AxisAlignedBoundingBox() = default;

		AxisAlignedBoundingBox(const vec3& bounds_min, const vec3& bounds_max);

		AxisAlignedBoundingBox(const AxisAlignedBoundingBox& other) = default;
		
		AxisAlignedBoundingBox(AxisAlignedBoundingBox&& other) noexcept = default;
		
		~AxisAlignedBoundingBox() = default;
		
		AxisAlignedBoundingBox& operator=(const AxisAlignedBoundingBox& other) = default;
		
		AxisAlignedBoundingBox& operator=(AxisAlignedBoundingBox&& other) noexcept = default;

		void moveTo(const vec3& position) override;

		void resizeTo(const vec3& bounds_min, const vec3& bounds_max);
		
		void adaptSize(const vec3& position) override;
		
		[[nodiscard]]
		vec3 getMin() const override;
		
		[[nodiscard]]
		vec3 getMax() const override;

        [[nodiscard]]
		vec3 getCenter() const override;

	};
	
	/** @} */
	
}
