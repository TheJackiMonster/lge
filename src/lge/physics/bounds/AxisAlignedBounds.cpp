//
// Created by thejackimonster on 19.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "AxisAlignedBounds.hpp"

namespace lge {

    void AxisAlignedBounds::moveBy(const lge::vec3& delta) {
        this->moveTo(this->getCenter() + delta);
    }

    bool AxisAlignedBounds::isInside(const vec3& position) const {
        auto this_min = this->getMin();
        auto this_max = this->getMax();

        return (
                (position[0] >= this_min[0]) &&
                (position[1] >= this_min[1]) &&
                (position[2] >= this_min[2]) &&
                (position[0] <= this_max[0]) &&
                (position[1] <= this_max[1]) &&
                (position[2] <= this_max[2])
        );
    }

    bool AxisAlignedBounds::isInside(const AxisAlignedBounds& other) const {
        auto this_min = this->getMin();
        auto this_max = this->getMax();

        auto other_min = other.getMin();
        auto other_max = other.getMax();

        return (
                (other_min[0] >= this_min[0]) &&
                (other_min[1] >= this_min[1]) &&
                (other_min[2] >= this_min[2]) &&
                (other_max[0] <= this_max[0]) &&
                (other_max[1] <= this_max[1]) &&
                (other_max[2] <= this_max[2])
        );
    }

    bool AxisAlignedBounds::isIntersecting(const AxisAlignedBounds& other) const {
        auto this_min = this->getMin();
        auto this_max = this->getMax();

        auto other_min = other.getMin();
        auto other_max = other.getMax();

        return (
                (other_max[0] >= this_min[0]) &&
                (other_max[1] >= this_min[1]) &&
                (other_max[2] >= this_min[2]) &&
                (other_min[0] <= this_max[0]) &&
                (other_min[1] <= this_max[1]) &&
                (other_min[2] <= this_max[2])
        );
    }
	
	ostream& operator<<(ostream& stream, const AxisAlignedBounds& bounds) {
    	stream << "AABounds::{ ";
    	stream << "min: [ " << flip(bounds.getMin()) << " ], ";
		stream << "max: [ " << flip(bounds.getMax()) << " ] ";
    	stream << "}";
    	
    	return stream;
    }

}
