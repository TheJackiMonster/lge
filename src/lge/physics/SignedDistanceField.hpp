#pragma once
//
// Created by thejackimonster on 24.11.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../graphics/Device.hpp"
#include "../graphics/glsl.hxx"
#include "../graphics/ShaderProgram.hpp"
#include "../graphics/Texture3D.hpp"
#include "../model/Model.hpp"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	struct t_vertex {
		lge::std430::vec3 m_pos;
		lge::std430::vec3 m_normal;
	};
	
	struct t_particle {
		lge::std430::uint_t m_amount;
		lge::std430::float_t m_time;
		lge::std430::vec4 m_pos;
		lge::std430::vec3 m_linear_delta [2];
		lge::std430::vec3 m_angular_delta [2];
		lge::std430::vec3 m_movement [2];
	};
	
	struct t_inertia {
		lge::std430::uint_t m_amount;
		lge::std430::vec3 m_diagonal;
		lge::std430::vec3 m_other;
	};
	
	class SignedDistanceField {
	private:
		Texture3D m_resource;
		
		shader_storage_buffer< t_particle > m_particles [2];
		
		mat3 m_inertia;
		
		const Model* m_model;
		
	public:
		explicit SignedDistanceField() = default;
		
		SignedDistanceField(const SignedDistanceField& other) = delete;
		
		SignedDistanceField(SignedDistanceField&& other) = default;
		
		~SignedDistanceField() = default;
		
		SignedDistanceField& operator=(const SignedDistanceField& other) = delete;
		
		SignedDistanceField& operator=(SignedDistanceField&& other) = default;
		
		[[nodiscard]]
		const Texture3D& getTexture() const;
		
		[[nodiscard]]
		const lge::shader_storage_buffer< t_particle >& getParticles(bool first = true) const;

		void setModel(const Model& model);

		[[nodiscard]]
		const Model* getModel() const;
		
		bool setup(uint resolution = 8);

		bool load(const string& path);

		void save(const string& path);
		
		void generate(GraphicsDevice& device, const Model& model);
		
		void generateInertia(GraphicsDevice& device);
		
		const mat3& getInertia() const;
	
	private:
		static void generateSDF(GraphicsDevice& device, const vector< t_vertex >& vertices, Texture3D& resource);
		
		static void generateIntertia(GraphicsDevice& device, const Texture3D& resource, mat3& inertia);
		
	};
	
	/** @} */

}
