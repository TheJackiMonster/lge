#pragma once
//
// Created by thejackimonster on 12.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "bounds/AxisAlignedBoundingCube.hpp"
#include "PhysicMaterial.hpp"
#include "SignedDistanceField.hpp"
#include "quat.hxx"
#include "../system/iostream.hxx"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	struct t_bounds {
		std430::float_t m_mass;
		std430::float_t m_bounciness;
		std430::float_t m_roughness;
		std430::vec3 m_velocity;
		std430::vec3 m_angular_frequency;
		std430::vec4 m_cube;
		std430::mat3_T m_orientation;
		std430::mat3_T m_inertia;
	};
	
	/**
	 * A class to represent an object using Rigid-Body-physics.
	 *
	 * @author TheJackiMonster
	 * @since 12.10.2019
	 */
	class RigidBody {
	private:
		AxisAlignedBoundingCube m_bounds;
		const SignedDistanceField* m_sdf;
		PhysicMaterial m_material;

		mat3 m_inverse_inertia;
		
		mat3 m_inertia;
		quat m_orientation;
		vec3 m_angular_frequency;
		vec3 m_velocity;
		float m_mass;
		
		float m_inverse_mass;
		
		float m_time;
		bool m_changed;
	
	public:
		explicit RigidBody(const SignedDistanceField& sdf);
		
		RigidBody(const RigidBody& other) = default;
		
		RigidBody(RigidBody&& other) = default;
		
		~RigidBody() = default;
		
		RigidBody& operator=(const RigidBody& other) = default;
		
		RigidBody& operator=(RigidBody&& other) = default;
		
		void update(float time, const vec3& gravity);
		
		void impulseUpdate(const vec3& linear, const vec3& angular, const vec3& distance);
		
		AxisAlignedBoundingCube& bounds();
		
		[[nodiscard]]
		const AxisAlignedBoundingCube& bounds() const;
		
		[[nodiscard]]
		const SignedDistanceField& getSDF() const;
		
		PhysicMaterial& material();
		
		[[nodiscard]]
		const PhysicMaterial& material() const;

		void setOrientation(const quat& orientation);

		[[nodiscard]]
		const quat& getOrientation() const;
		
		void setAngularFrequency(const vec3& angularFrequency);
		
		[[nodiscard]]
		const vec3& getAngularFrequency() const;
		
		void setVelocity(const lge::vec3& velocity);
		
		[[nodiscard]]
		const vec3& getVelocity() const;
		
		void setMass(float mass);
		
		[[nodiscard]]
		float getMass() const;
		
		void setInertia(const mat3& inertia);
		
		[[nodiscard]]
		const mat3& getInertia() const;
		
		void setInertiaAutomatic();
		
		void setTime(float time);
		
		float getTime() const;
		
		void rotateBy(const vec3& axis);
		
		void copyToBounds(t_bounds& bounds) const;
		
		bool wasChanged();
		
	};
	
	ostream& operator<<(ostream& stream, const RigidBody& body);
	
	/** @} */
	
}
