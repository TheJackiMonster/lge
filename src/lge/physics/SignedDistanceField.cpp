//
// Created by thejackimonster on 24.11.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "SignedDistanceField.hpp"

#include <fstream>
#include <cstring>

#include <lge/shaders/shaders.hxx>

#include "../util/algorithm.hxx"
#include "../util/functional.hxx"

namespace lge {
	
	const Texture3D& SignedDistanceField::getTexture() const {
		return this->m_resource;
	}
	
	const shader_storage_buffer<t_particle>& SignedDistanceField::getParticles(bool first) const {
		return this->m_particles[first? 0 : 1];
	}

	void SignedDistanceField::setModel(const Model& model) {
	    this->m_model = &(model);
	}
	
	const Model* SignedDistanceField::getModel() const {
		return this->m_model;
	}

	bool SignedDistanceField::setup(uint resolution) {
		if (this->m_resource.setup(uvec3(resolution * 8))) {
			this->m_resource.setWrapping(false, false, false);
			this->m_resource.setMinFilter(true, false);
			
			return true;
		} else {
			return false;
		}
	}

	bool SignedDistanceField::load(const string& path) {
        std::ifstream in(path, std::ios::in);

        if (in) {
            uvec4 size (0);

            in.seekg(0, std::ios::beg);
            in.read(reinterpret_cast<char*>(size.data()), size.size() * sizeof(uint));
	
			size.column< 3 >(0) *= 8;

            vector< float > values (size[0] * size[1] * size[2]);
            vector< vec4 > positions (size[3]);

            in.read(reinterpret_cast< char* >(values.data()), values.size() * sizeof(float));
            in.read(reinterpret_cast< char* >(positions.data()), positions.size() * sizeof(vec4));

            in.close();

            vector< t_particle > particles (positions.size());

            for (uint i = 0; i < positions.size(); i++) {
                t_particle p;

				memset(&p, 0, sizeof(p));
	
				p.m_amount.val = 1;
				p.m_pos.vec = positions[i];

                particles[i] = p;
            }

            for (uint i = 0; i < 2; i++) {
				this->m_particles[i].data(particles.size(), particles.data(), false);
            }

            bool result = false;

            if (this->m_resource.setup(size.column< 3 >(0), values.data())) {
                this->m_resource.setWrapping(false, false, false);
                this->m_resource.setMinFilter(true, false);

                result = true;
            }

            this->m_model = nullptr;

            return result;
        } else {
            cerr << "File not found! '" << path << "'" << endl;

            return false;
        }
	}

	void SignedDistanceField::save(const string& path) {
        std::ofstream out (path, std::ios::out);

        if (out) {
            vector< t_particle > particles (this->m_particles[0].size());

            this->m_particles[0].getData(particles.size(), particles.data());

            uvec4 size (0, 0, 0, particles.size());

            this->m_resource.getData(size.column< 3 >(0), nullptr);

            vector<float> values (size[0] * size[1] * size[2]);
            vector<vec4> positions (size[3]);

            this->m_resource.getData(size.column< 3 >(0), values.data());

            for (uint i = 0; i < positions.size(); i++) {
                positions[i] = particles[i].m_pos.vec;
            }

            out.seekp(0, std::ios::beg);
            
            size.column< 3 >(0) /= 8;

            out.write(reinterpret_cast<char*>(size.data()), size.size() * sizeof(float));
            out.write(reinterpret_cast<char*>(values.data()), values.size() * sizeof(float));
            out.write(reinterpret_cast<char*>(positions.data()), positions.size() * sizeof(vec4));

            out.flush();
            out.close();
        }
	}
	
	void SignedDistanceField::generate(GraphicsDevice& device, const Model& model) {
		vector< vec< 6 > > positions = model.getPositions();
		
		vector< t_vertex > vertices (positions.size());
		
		for (uint i = 0; i < positions.size(); i++) {
			auto& v = vertices[i];
			
			v.m_pos.vec = positions[i].column< 3 >(0, 0);
			v.m_normal.vec = positions[i].column< 3 >(0, 3);
		}
		
		SignedDistanceField::generateSDF(device, vertices, this->m_resource);
		
		/*sort(vertices.begin(), vertices.end(), [](const t_vertex& v0, const t_vertex& v1) {
			return order(v0.m_pos.vec, v1.m_pos.vec);
		});
		
		vertices.erase(unique(vertices.begin(), vertices.end(), [](const t_vertex& v0, const t_vertex& v1) {
			return v0.m_pos.vec == v1.m_pos.vec;
		}), vertices.end());*/
		
		vector< t_particle > particles (vertices.size());
		
		for (uint i = 0; i < vertices.size(); i++) {
			auto& p = particles[i];
			
			memset(&p, 0, sizeof(p));
			
			p.m_amount.val = 1;
			p.m_pos.vec.column< 3 >(0) = vertices[i].m_pos.vec;
			p.m_pos.vec[3] = 0;
		}
		
		for (uint i = 0; i < 2; i++) {
			this->m_particles[i].data(particles.size(), particles.data(), false);
		}
		
		cout << particles.size() << " " << positions.size() << endl;
		
		this->m_model = &(model);
	}
	
	void SignedDistanceField::generateInertia(GraphicsDevice& device) {
		SignedDistanceField::generateIntertia(device, this->m_resource, m_inertia);
	}
	
	const mat3& SignedDistanceField::getInertia() const {
		return m_inertia;
	}
	
	void SignedDistanceField::generateSDF(GraphicsDevice& device, const vector< t_vertex >& vertices, Texture3D& resource) {
		static ShaderProgram program;
		
		if (program.getAttachedShaders().empty()) {
			Shader shader (ShaderType::COMPUTE_SHADER);
			
			shader.appendSource(GENERATE_SDF_COMP_SHADER);
			shader.loadSource("../../resources/shaders/generate_sdf.comp");
			
			if (shader.compile()) {
				program.attachShader(shader);
			}
			
			program.link();
		}
		
		uniform< lge::Texture3DHandle > u_sdf = program.getUniform< lge::Texture3DHandle >("u_sdf");
		shader_storage_buffer_block< t_vertex > u_vertices_block = program.getStorageBlock< t_vertex >("u_vertices_block");
		
		u_sdf.setAccessMode(AccessMode::WRITE_ONLY);
		
		device.checkErrors(true);
		
		shader_storage_buffer< t_vertex > u_vertices;
		
		u_vertices.data(vertices.size(), vertices.data(), false);
		
		u_sdf = resource;
		u_vertices_block.connect(u_vertices);
		
		device.checkErrors(true);
		
		if (program.validate()) {
			device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_BUFFER_UPDATE_BARRIER_BIT);
			
			program.use();
			
			uint resolution = resource.getWidth() / 8;
			
			device.dispatchCompute(resolution, resolution, resolution);
			device.memoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
			
			device.checkErrors(true);
		}
	}
	
	void SignedDistanceField::generateIntertia(lge::GraphicsDevice& device, const lge::Texture3D& resource, lge::mat3& inertia) {
		static ShaderProgram program;
		
		if (program.getAttachedShaders().empty()) {
			Shader shader (ShaderType::COMPUTE_SHADER);
			
			shader.appendSource(GENERATE_INERTIA_COMP_SHADER);
			shader.loadSource("../../resources/shaders/generate_inertia.comp");
			
			if (shader.compile()) {
				program.attachShader(shader);
			}
			
			program.link();
		}
		
		uniform< lge::Texture3DHandle > u_sdf = program.getUniform< lge::Texture3DHandle >("u_sdf");
		shader_storage_buffer_block< t_inertia > u_inertias_block = program.getStorageBlock< t_inertia >("u_inertias_block");
		
		u_sdf.setAccessMode(AccessMode::READ_ONLY);
		
		device.checkErrors(true);
		
		uint resolution = resource.getWidth() / 8;
		
		vector<t_inertia> inertia_values(resolution * resolution * resolution);
		
		shader_storage_buffer< t_inertia > u_inertias (inertia_values.size());
		
		u_sdf = resource;
		u_inertias_block.connect(u_inertias);
		
		device.checkErrors(true);
		
		if (program.validate()) {
			device.memoryBarrier(
					GL_SHADER_STORAGE_BARRIER_BIT |
					GL_BUFFER_UPDATE_BARRIER_BIT |
					GL_SHADER_IMAGE_ACCESS_BARRIER_BIT
			);
			
			program.use();
			
			uint resolution = resource.getWidth() / 8;
			
			device.dispatchCompute(resolution, resolution, resolution);
			device.memoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
			
			device.checkErrors(true);
		}
		
		u_inertias.getData(inertia_values.size(), inertia_values.data());
		
		uint amount = 0;
		
		dvec3 diagonal (0);
		dvec3 other (0);
		
		for (const t_inertia& inertia : inertia_values) {
			if (inertia.m_amount.val > 0) {
				amount += inertia.m_amount.val;
				
				diagonal += dvec3(inertia.m_diagonal.vec);
				other += dvec3(inertia.m_other.vec);
			}
		}
		
		if (amount > 0) {
			diagonal /= amount;
			other /= amount;
			
			for (uint i = 0; i < 3; i++) {
				inertia(i, i) = diagonal[i];
			}
			
			inertia(0, 1) = -other[0];
			inertia(1, 0) = -other[0];
			
			inertia(0, 2) = -other[1];
			inertia(2, 0) = -other[1];
			
			inertia(2, 1) = -other[2];
			inertia(1, 2) = -other[2];
		} else {
			inertia *= 0;
		}
	}

}
