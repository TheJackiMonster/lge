#pragma once
//
// Created by thejackimonster on 27.01.20.
//
// Copyright (c) 2020 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../lang/string.hxx"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	enum class PrecisionMode : uint {
		ACCURATE = 0x1,
		DYNAMIC = 0x2,
		REALTIME = 0x3,
		POST_EFFECT = 0x4
	};
	
	namespace PrecisionMode_ {
	
		void initPassTime(PrecisionMode mode, float deltaTime, float& passTime);
		
		void adjustTimings(PrecisionMode mode, float time, float& hitTime, float& passTime);
		
		void checkPassTime(PrecisionMode mode, float deltaTime, float& passTime);
		
		bool useSecondFilter(PrecisionMode mode);
		
		PrecisionMode next(PrecisionMode mode);
		
		string getName(PrecisionMode mode);
		
	}
	
	/** @} */
	
}
