//
// Created by thejackimonster on 27.01.20.
//
// Copyright (c) 2020 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "PrecisionMode.hpp"

#include "../math/util.hxx"

namespace lge::PrecisionMode_ {
	
	void initPassTime(PrecisionMode mode, float deltaTime, float& passTime) {
		if (mode == PrecisionMode::POST_EFFECT) {
			passTime = 0;
		} else {
			passTime = max(deltaTime, 0.0f);
		}
	}
	
	void adjustTimings(PrecisionMode mode, float time, float& hitTime, float& passTime) {
		switch (mode) {
			case PrecisionMode::ACCURATE:
				if (time < passTime) {
					passTime = max(time, 0.0f);
				}
				
				break;
			case PrecisionMode::DYNAMIC:
				if (time < hitTime) {
					passTime = max(hitTime, 0.0f);
					hitTime = time;
				}
				
				break;
			default:
				break;
		}
	}
	
	void checkPassTime(PrecisionMode mode, float deltaTime, float& passTime) {
		if (mode == PrecisionMode::POST_EFFECT) {
			passTime = deltaTime;
		}
	}
	
	bool useSecondFilter(PrecisionMode mode) {
		return (mode == PrecisionMode::ACCURATE) || (mode == PrecisionMode::DYNAMIC);
	}
	
	PrecisionMode next(PrecisionMode mode) {
		return static_cast< PrecisionMode >(1 + static_cast< uint >(mode) % 4);
	}
	
	string getName(PrecisionMode mode) {
		switch (mode) {
			case PrecisionMode::ACCURATE:
				return "ACCURATE";
			case PrecisionMode::DYNAMIC:
				return "DYNAMIC";
			case PrecisionMode::REALTIME:
				return "REALTIME";
			case PrecisionMode::POST_EFFECT:
				return "POST_EFFECT";
			default:
				return "UNKNOWN";
		}
	}

}
