#pragma once
//
// Created by thejackimonster on 14.01.20.
//
// Copyright (c) 2020 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../math/util.hxx"
#include "../system/iostream.hxx"

namespace lge {
	
	/** @addtogroup physics_group
	 *  @{
	 */
	
	class PhysicMaterial {
	private:
		float m_bounciness;
		float m_roughness;
	
	public:
		explicit PhysicMaterial();
		
		PhysicMaterial(const PhysicMaterial& other) = default;
		
		PhysicMaterial(PhysicMaterial&& other) = default;
		
		~PhysicMaterial() = default;
		
		PhysicMaterial& operator=(const PhysicMaterial& other) = default;
		
		PhysicMaterial& operator=(PhysicMaterial&& other) = default;
		
		void setBounciness(float bounciness);
		
		[[nodiscard]]
		float getBounciness() const;
		
		void setRoughness(float roughness);
		
		[[nodiscard]]
		float getRoughness() const;
	
	};
	
	ostream& operator<<(ostream& stream, const PhysicMaterial& material);
	
	/** @} */
	
}
