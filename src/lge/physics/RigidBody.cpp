//
// Created by thejackimonster on 12.10.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "RigidBody.hpp"

#include "../math/trigonometry.hxx"

namespace lge {
	
	RigidBody::RigidBody(const SignedDistanceField& sdf) {
		this->bounds().moveTo(vec3(0));
		this->bounds().resizeTo(1);
		
		this->m_sdf = &(sdf);
		
		this->setVelocity(vec3(0));
		this->setAngularFrequency(vec3(0));
		
		this->setOrientation(quat(0));
		
		this->setMass(1);
		this->setInertiaAutomatic();
		
		this->setTime(0);
	}
	
	void RigidBody::update(float time, const vec3& gravity) {
		if (time > this->getTime()) {
			bool changed = this->m_changed;
			
			float deltaTime = time - this->getTime();
			
			this->bounds().moveBy(this->getVelocity() * deltaTime);
			this->rotateBy(this->getAngularFrequency() * deltaTime);
			
			if (this->m_inverse_mass > 0.0f) {
				this->setVelocity(this->getVelocity() + gravity * deltaTime);
			}
			
			this->setTime(time);
			
			this->m_changed = changed;
		}
	}
	
	void RigidBody::impulseUpdate(const vec3& linear, const vec3& angular, const vec3& distance) {
		bool changed = this->m_changed;
		
		if (this->m_inverse_mass > 0.0f) {
			this->setVelocity(this->getVelocity() + linear);
			this->setAngularFrequency(this->getAngularFrequency() + angular);
			
			this->bounds().moveBy(distance);
		}
		
		this->m_changed = changed;
	}
	
	AxisAlignedBoundingCube& RigidBody::bounds() {
		this->m_changed = true;
		
		return this->m_bounds;
	}
	
	const AxisAlignedBoundingCube& RigidBody::bounds() const {
		return this->m_bounds;
	}
	
	const SignedDistanceField& RigidBody::getSDF() const {
		return *(this->m_sdf);
	}
	
	PhysicMaterial& RigidBody::material() {
		this->m_changed = true;
		
		return this->m_material;
	}
	
	const PhysicMaterial& RigidBody::material() const {
		return this->m_material;
	}

	void RigidBody::setOrientation(const quat& orientation) {
        auto len = length(orientation);
        
        if (len > 0) {
			this->m_orientation = orientation / len;
        } else {
			this->m_orientation = quat(0, 0, 0, 1);
        }
        
        this->m_changed = true;
	}

	const quat& RigidBody::getOrientation() const {
	    return this->m_orientation;
	}
	
	void RigidBody::setAngularFrequency(const lge::vec3& angularFrequency) {
		this->m_angular_frequency = angularFrequency;
		
		this->m_changed = true;
	}
	
	const lge::vec3& RigidBody::getAngularFrequency() const {
		return this->m_angular_frequency;
	}
	
	void RigidBody::setVelocity(const lge::vec3& velocity) {
		this->m_velocity = velocity;
		
		this->m_changed = true;
	}
	
	const vec3& RigidBody::getVelocity() const {
		return this->m_velocity;
	}
	
	void RigidBody::setMass(float mass) {
		this->m_mass = mass;
		
		if (isinff(mass)) {
			this->m_inverse_mass = 0;
		} else {
			this->m_inverse_mass = 1.0f / mass;
		}
		
		this->m_changed = true;
	}
	
	float RigidBody::getMass() const {
		return this->m_mass;
	}
	
	void RigidBody::setInertia(const mat3& inertia) {
		this->m_inertia = inertia;
		
		uint i, j;
		bool flag = false;
		
		for (i = 0; i < inertia.rows(); i++) {
			for (j = 0; j < inertia.columns(); j++) {
				flag |= isinff(inertia(i, j));
			}
		}
		
		if (flag) {
			this->m_inverse_inertia = mat3(0);
		} else {
			this->m_inverse_inertia = inverse(inertia);
		}
		
		this->m_changed = true;
	}
	
	const mat3& RigidBody::getInertia() const {
		return this->m_inertia;
	}
	
	void RigidBody::setInertiaAutomatic() {
		float factor = this->bounds().getSize();
		
		factor *= (factor * this->getMass());
		
		this->setInertia(this->getSDF().getInertia() * factor);
	}
	
	void RigidBody::setTime(float time) {
		this->m_time = time;
	}
	
	float RigidBody::getTime() const {
		return this->m_time;
	}
	
	void RigidBody::rotateBy(const lge::vec3& axis) {
		auto value = length(axis);
		
		if (value > 0) {
			this->setOrientation(rotation(value, axis) * this->getOrientation());
		}
	}
	
	void RigidBody::copyToBounds(t_bounds &bounds) const {
		mat3 R = orientation(this->getOrientation());
		mat3 RT = transpose(R);
		
		bounds.m_mass.val = this->m_inverse_mass;
		bounds.m_bounciness.val = this->material().getBounciness();
		bounds.m_roughness.val = this->material().getRoughness();
		bounds.m_velocity.vec = this->getVelocity();
		bounds.m_angular_frequency.vec = this->getAngularFrequency();
		bounds.m_cube.vec.column<3>(0) = this->bounds().getCenter();
		bounds.m_cube.vec[3] = this->bounds().getSize();
		bounds.m_orientation.mat = RT;
		bounds.m_inertia.mat = transpose(R * this->m_inverse_inertia * RT);
	}
	
	bool RigidBody::wasChanged() {
		if (this->m_changed) {
			this->m_changed = false;
			
			return true;
		} else {
			return false;
		}
	}
	
	ostream& operator<<(ostream& stream, const RigidBody& body) {
		stream << "RigidBody::{ ";
		stream << "bounds: " << body.bounds() << ", ";
		stream << "material: " << body.material() << " ";
		stream << "}";
		
		return stream;
	}
	
}
