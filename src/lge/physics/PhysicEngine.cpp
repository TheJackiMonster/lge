//
// Created by thejackimonster on 14.12.19.
//
// Copyright (c) 2019 thejackimonster. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "PhysicEngine.hpp"

#include "../util/algorithm.hxx"

#include <lge/shaders/shaders.hxx>

namespace lge {
	
	PhysicEngine::PhysicEngine() {
		this->m_gravity = vec3(0);
		this->m_time = 0.0f;
	}
	
	PhysicEngine::~PhysicEngine() {
	}
	
	void update_skip(t_skip& skip, t_collision& col, vector< RigidBody >& bodies) {
		vec3 diff_linear = bodies[col.m_pair.vec[0]].getVelocity() - bodies[col.m_pair.vec[1]].getVelocity();
		vec3 diff_angular = bodies[col.m_pair.vec[0]].getAngularFrequency() - bodies[col.m_pair.vec[1]].getAngularFrequency();
		
		diff_linear -= skip.m_diff_linear;
		diff_angular -= skip.m_diff_angular;
		
		if ((dot(diff_linear, diff_linear) > 0.0f) || (dot(diff_angular, diff_angular) > 0.0f)) {
			skip.m_value = 0;
		}
	}
	
	float PhysicEngine::update(GraphicsDevice& device, float deltaTime, PrecisionMode mode) {
		static double updateError = 0.0;
		
		const uint boundsCount = this->m_rigidBodies.size();
		
		for (uint i = 0; i < boundsCount; i++) {
			this->m_rigidBodies[i].copyToBounds(this->m_bounds_data[i]);
		}
		
		this->m_bounds.subData(boundsCount, this->m_bounds_data.data());
		
		for (uint i = 0; i < boundsCount; i++) {
			if (this->m_rigidBodies[i].wasChanged()) {
				uint k = (i * i - i) / 2;
				
				for (uint j = 1; j < i; j++) {
					update_skip(this->m_collision_skip[k], this->m_collision_data[k], this->m_rigidBodies);
					
					k++;
				}
				
				{
					update_skip(this->m_collision_skip[k], this->m_collision_data[k], this->m_rigidBodies);
					
					k += (i + 1);
				}
				
				for (uint j = i + 1; j < boundsCount; j++) {
					update_skip(this->m_collision_skip[k], this->m_collision_data[k], this->m_rigidBodies);
					
					k += j;
				}
			}
		}
		
		float passTime;
		
		PrecisionMode_::initPassTime(mode, deltaTime, passTime);
		
	    if (deltaTime > 0) {
			list<uint> hits = PhysicEngine::calculateCollisionWithBounds(device, *(this), this->m_collision_data, this->m_collision_skip, deltaTime);
		
			float hitTime = passTime;
		
			for (auto hit = hits.begin(); hit != hits.end();) {
				t_collision& collision = this->m_collision_data[*hit];
				
				if (collision.m_times.vec[0] > passTime) {
					hits.erase(hit, hits.end());
					break;
				}
				
				t_skip& skip = this->m_collision_skip[*hit];
				
				if (skip.m_value < 2) {
					calculateCollisionWithParticles(device, *(this), collision, *hit);
					
					skip.m_diff_linear = this->m_rigidBodies[collision.m_pair.vec[0]].getVelocity() - this->m_rigidBodies[collision.m_pair.vec[1]].getVelocity();
					skip.m_diff_angular = this->m_rigidBodies[collision.m_pair.vec[0]].getAngularFrequency() - this->m_rigidBodies[collision.m_pair.vec[1]].getAngularFrequency();
					skip.m_times = collision.m_times.vec;
					skip.m_value = 2;
				}
				
				float time = collision.m_times.vec[0];
				
				if (time <= passTime) {
					PrecisionMode_::adjustTimings(mode, time, hitTime, passTime);
					
					hit++;
				} else {
					hit = hits.erase(hit);
				}
			}
			
			if (PrecisionMode_::useSecondFilter(mode)) {
				for (auto hit = hits.begin(); hit != hits.end();) {
					auto pass = hit;
					
					hit++;
					
					if (this->m_collision_data[*pass].m_times.vec[0] > passTime) {
						hits.erase(pass);
					}
				}
			}
		
			PrecisionMode_::checkPassTime(mode, deltaTime, passTime);
			
			hits.sort([&] (uint hit0, uint hit1) {
				return this->m_collision_data[hit0].m_times.vec[0] < this->m_collision_data[hit1].m_times.vec[0];
			});
		
			double error = 0.0;
			
			for (unsigned int hit : hits) {
				const t_collision& collision = this->m_collision_data[hit];
				
				for (uint j = 0; j < 2; j++) {
					uint bid = collision.m_pair.vec[j];
					
					RigidBody& body = this->m_rigidBodies[bid];
					
					body.update(this->m_time + collision.m_times.vec[0], vec3(0) /* not yet working */);
					body.impulseUpdate(collision.m_linear_delta[j].vec, collision.m_angular_delta[j].vec, collision.m_movement[j].vec);
					
					error += length(collision.m_movement[j].vec);
				}
			}
		
			updateError += error;
			
			cout << "Error: " << updateError << "; ";
	    }
	    
	    for (auto& col : this->m_collision_skip) {
	    	col.m_times[0] -= passTime;
	    	col.m_times[1] -= passTime;
	    }
		
		this->m_time += passTime;
	    
	    double energy = 0.0;
	 
		for (uint i = 0; i < boundsCount; i++) {
            this->m_rigidBodies[i].update(this->m_time, vec3(0) /* not yet working */);
			this->m_rigidBodies[i].copyToBounds(this->m_bounds_data[i]);
			
			if (this->m_bounds_data[i].m_mass.val > 0) {
				energy += (
					(this->m_rigidBodies[i].getMass() * dot(this->m_rigidBodies[i].getVelocity(), this->m_rigidBodies[i].getVelocity())) +
					(dot(this->m_rigidBodies[i].getInertia() * this->m_rigidBodies[i].getAngularFrequency(), this->m_rigidBodies[i].getAngularFrequency()))
				) / 2;
				
				this->m_rigidBodies[i].setVelocity(this->m_rigidBodies[i].getVelocity() + this->m_gravity * passTime);
			}
		}
		
		if (deltaTime > 0) {
			cout << "Energy: " << energy << "; ";
		}
		
		return passTime;
	}
	
	vector< RigidBody >& PhysicEngine::getRigidBodies() {
		return this->m_rigidBodies;
	}
	
	const vector< RigidBody >& PhysicEngine::getRigidBodies() const {
		return this->m_rigidBodies;
	}
	
	void PhysicEngine::setGravity(const vec3& gravity) {
		this->m_gravity = gravity;
	}
	
	void PhysicEngine::setGravity(float value) {
		this->setGravity(vec3(0, -value, 0));
	}
	
	const vec3& PhysicEngine::getGravity() const {
		return this->m_gravity;
	}
	
	RigidBody& PhysicEngine::createRigidBody(const SignedDistanceField& sdf) {
		uint bodyCount = this->m_rigidBodies.size();
		uint collisionCount = bodyCount * (bodyCount - 1) / 2;
		
		this->m_rigidBodies.emplace_back(sdf);
		
		uint newBodyCount = bodyCount + 1;
		uint newCollisionCount = collisionCount + bodyCount;
		
		this->m_bounds_data.resize(newBodyCount);
		this->m_collision_data.resize(newCollisionCount);
		this->m_collision_skip.resize(newCollisionCount);
		this->m_idx_data.resize(newCollisionCount);
		
		RigidBody& body = this->m_rigidBodies[bodyCount];
		
		body.setTime(this->m_time);
		
		if (this->m_bounds.size() < newBodyCount) {
			vector< t_bounds > bodies (lge::max(this->m_bounds.size() * 2, newBodyCount));
			
			for (uint i = 0; i < newBodyCount; i++) {
				t_bounds data;
				
				this->m_rigidBodies[i].copyToBounds(data);
				
				bodies[i] = data;
			}
			
			this->m_bounds.data(bodies.size(), bodies.data(), true);
		} else {
			t_bounds data;
			
			body.copyToBounds(data);
			
			this->m_bounds[bodyCount] = data;
		}
		
		if (this->m_collisions.size() < newCollisionCount) {
			vector< t_collision > collisions (lge::max(this->m_collisions.size() * 2, newCollisionCount));
			
			uint i, j, k = 0;
			
			for (i = 1; i < newBodyCount; i++) {
				for (j = 0; j < i; j++) {
					t_collision data;
					
					data.m_pair.vec = lge::uvec2(j, i);
					data.m_times.vec = lge::vec2(
							-lge::numeric_limits<float>::infinity(),
							+lge::numeric_limits<float>::infinity()
					);
					
					collisions[k++] = data;
				}
			}
			
			this->m_collisions.data(collisions.size(), collisions.data(), true);
		} else {
			vector< t_collision > collisions (bodyCount);
			
			for (uint i = 0; i < bodyCount; i++) {
				t_collision data;
				
				data.m_pair.vec = lge::uvec2(i, bodyCount);
				data.m_times.vec = lge::vec2(
						-numeric_limits<float>::infinity(),
						+numeric_limits<float>::infinity()
				);
				
				collisions[i] = data;
			}
			
			this->m_collisions.subData(bodyCount, collisions.data(), collisionCount);
		}
		
		return body;
	}
	
	void PhysicEngine::destroyRigidBody(RigidBody& body) {
		//
	}
	
	const shader_storage_buffer< t_bounds >& PhysicEngine::getBounds() const {
		return this->m_bounds;
	}
	
	const shader_storage_buffer< t_collision >& PhysicEngine::getCollisions() const {
		return this->m_collisions;
	}
	
	list<uint> PhysicEngine::calculateCollisionWithBounds(GraphicsDevice& device, PhysicEngine& engine, vector<t_collision>& collisions, vector<t_skip>& skips, float deltaTime) {
		static ShaderProgram programCalc;
		
		if (programCalc.getAttachedShaders().empty()) {
			Shader shader (ShaderType::COMPUTE_SHADER);
			
			shader.appendSource(CALCULATE_COLLISION_BOUNDS_COMP_SHADER);
			shader.loadSource("../../resources/shaders/calculate_collision_bounds.comp");
			
			if (shader.compile()) {
				programCalc.attachShader(shader);
			}
			
			programCalc.link();
		}
		
		uniform< vec2 > uInfinityCalc = programCalc.getUniform< vec2 >("u_infinity");
		uniform< uint > uCountCalc = programCalc.getUniform< uint >("u_count");
		shader_storage_buffer_block< uint > uIndicesBlockCalc = programCalc.getStorageBlock< uint >(
				"u_indices_block");
		shader_storage_buffer_block< t_bounds > uBoundsBlockCalc = programCalc.getStorageBlock< t_bounds >(
				"u_bounds_block");
		shader_storage_buffer_block< t_collision > uCollisionBlockCalc = programCalc.getStorageBlock< t_collision >(
				"u_collision_block");
		
		uint bodyCount = engine.getRigidBodies().size();
		uint collisionCount = bodyCount * (bodyCount - 1) / 2;
		uint indicesCount = 0;
		
		uint skippedStart = 0;
		uint skippedEnd = 0;
		
		for (uint i = 0; i < collisionCount; i++) {
			if ((skips[i].m_value != 1) || (skips[i].m_times[0] < deltaTime)) {
				engine.m_idx_data[indicesCount++] = i;
				
				skippedEnd = i + 1;
			} else {
				if (skippedStart == i) {
					skippedStart++;
				}
			}
		}
		
		if ((programCalc.validate()) && (skippedEnd > skippedStart)) {
			engine.m_indices.data(indicesCount, engine.m_idx_data.data(), false);
			
			uInfinityCalc = vec2(-numeric_limits<float>::infinity(), +numeric_limits<float>::infinity());
			uCountCalc = indicesCount;
			uIndicesBlockCalc.connect(engine.m_indices);
			uBoundsBlockCalc.connect(engine.m_bounds);
			uCollisionBlockCalc.connect(engine.m_collisions);
			
			programCalc.use();
			
			device.dispatchCompute((indicesCount + 511) / 512);
			
			device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
			
			engine.m_collisions.getData((skippedEnd - skippedStart), collisions.data(), skippedStart);
		}
		
		list<uint> possibleHits;
		
		for (uint i = skippedStart; i < skippedEnd; i++) {
			if (skips[i].m_value) {
				collisions[i].m_times.vec = skips[i].m_times;
			} else {
				skips[i].m_diff_linear = engine.m_rigidBodies[collisions[i].m_pair.vec[0]].getVelocity() - engine.m_rigidBodies[collisions[i].m_pair.vec[1]].getVelocity();
				skips[i].m_diff_angular = engine.m_rigidBodies[collisions[i].m_pair.vec[0]].getAngularFrequency() - engine.m_rigidBodies[collisions[i].m_pair.vec[1]].getAngularFrequency();
				skips[i].m_times = collisions[i].m_times.vec;
				skips[i].m_value = 1;
			}
			
			if ((collisions[i].m_times.vec[1] > +0.0f) && (collisions[i].m_times.vec[0] < deltaTime)) {
				possibleHits.push_back(i);
			}
		}
		
		possibleHits.sort([&](uint u0, uint u1) {
			const t_collision c0 = collisions[u0];
			const t_collision c1 = collisions[u1];
			
			if (c0.m_times.vec[0] == c1.m_times.vec[0]) {
				return (c1.m_times.vec[1] > c0.m_times.vec[1]);
			} else {
				return (c1.m_times.vec[0] > c0.m_times.vec[0]);
			}
		});
		
		return possibleHits;
	}
	
	void PhysicEngine::calculateCollisionWithParticles(GraphicsDevice& device, PhysicEngine& engine, t_collision& collision, uint index) {
		static ShaderProgram programCalc, programExt;
		
		if (programCalc.getAttachedShaders().empty()) {
			Shader shader (ShaderType::COMPUTE_SHADER);
			
			shader.appendSource(CALCULATE_PARTICLE_COLLISION_COMP_SHADER);
			shader.loadSource("../../resources/shaders/calculate_particle_collision.comp");
			
			if (shader.compile()) {
				programCalc.attachShader(shader);
			}
			
			programCalc.link();
		}
		
		uniform< uint > uCollisionIndexCalc = programCalc.getUniform< uint >("u_collision_index");
		uniform< Texture3DHandle > uSdf0Calc = programCalc.getUniform< Texture3DHandle >("u_sdf_0");
		uniform< Texture3DHandle > uSdf1Calc = programCalc.getUniform< Texture3DHandle >("u_sdf_1");
		uniform< uint > uParticleCount0Calc = programCalc.getUniform< uint >("u_particle_count_0");
		uniform< uint > uParticleCount1Calc = programCalc.getUniform< uint >("u_particle_count_1");
		
		shader_storage_buffer_block< t_bounds > uBoundsBlockCalc = programCalc.getStorageBlock< t_bounds >(
				"u_bounds_block");
		shader_storage_buffer_block< t_collision > uCollisionBlockCalc = programCalc.getStorageBlock< t_collision >(
				"u_collision_block");
		shader_storage_buffer_block< t_particle > uParticleBlock0Calc = programCalc.getStorageBlock< t_particle >(
				"u_particle_block_0");
		shader_storage_buffer_block< t_particle > uParticleBlock1Calc = programCalc.getStorageBlock< t_particle >(
				"u_particle_block_1");
		
		const RigidBody& body0 = engine.getRigidBodies()[collision.m_pair.vec[0]];
		const RigidBody& body1 = engine.getRigidBodies()[collision.m_pair.vec[1]];
		
		const shader_storage_buffer< t_particle >& particles0 = body0.getSDF().getParticles(true);
		const shader_storage_buffer< t_particle >& particles1 = body1.getSDF().getParticles(false);
		
		device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_BUFFER_UPDATE_BARRIER_BIT);
		
		uint particleCount0 = particles0.size();
		uint particleCount1 = particles1.size();
		
		uint fullParticleCount = (particleCount0 + particleCount1);
		
		if (programCalc.validate()) {
			uCollisionIndexCalc = index;
			uSdf0Calc = body0.getSDF().getTexture();
			uSdf1Calc = body1.getSDF().getTexture();
			uParticleCount0Calc = particleCount0;
			uParticleCount1Calc = particleCount1;
			uBoundsBlockCalc.connect(engine.m_bounds);
			uCollisionBlockCalc.connect(engine.m_collisions);
			uParticleBlock0Calc.connect(particles0);
			uParticleBlock1Calc.connect(particles1);
			
			programCalc.use();
			
			device.dispatchCompute((fullParticleCount + 511) / 512);
		}
		
		if (programExt.getAttachedShaders().empty()) {
			Shader shader (ShaderType::COMPUTE_SHADER);
			
			shader.appendSource(CALCULATE_PARTICLE_EXTREME_COMP_SHADER);
			shader.loadSource("../../resources/shaders/calculate_particle_extreme.comp");
			
			if (shader.compile()) {
				programExt.attachShader(shader);
			}
			
			programExt.link();
		}
		
		uniform< uint > uParticleCount0Ext = programExt.getUniform< uint >("u_particle_count_0");
		uniform< uint > uParticleCount1Ext = programExt.getUniform< uint >("u_particle_count_1");
		uniform< uint > uScanSizeExt = programExt.getUniform< uint >("u_scan_size");
		
		shader_storage_buffer_block< t_particle > uParticleBlock0Ext = programExt.getStorageBlock< t_particle >(
				"u_particle_block_0");
		shader_storage_buffer_block< t_particle > uParticleBlock1Ext = programExt.getStorageBlock< t_particle >(
				"u_particle_block_1");
		
		device.memoryBarrier(GL_UNIFORM_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);
		
		t_particle particle;
		
		if (programExt.validate()) {
			uParticleCount0Ext = particleCount0;
			uParticleCount1Ext = particleCount1;
			uParticleBlock0Ext.connect(particles0);
			uParticleBlock1Ext.connect(particles1);
			
			programExt.use();
			
			uint scanSize = 1;
			
			do {
				uScanSizeExt = scanSize;
				scanSize *= 256;
				
				device.dispatchCompute((fullParticleCount + scanSize - 1) / scanSize);
				device.memoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
			} while (scanSize < fullParticleCount);
			
			particles0.getData(1, &particle);
		}
		
		assert(particle.m_amount.val < fullParticleCount);
		
		if (particle.m_amount.val > 0) {
			collision.m_times.vec[0] = particle.m_time.val;
			
			for (uint i = 0; i < 2; i++) {
				collision.m_linear_delta[i].vec = particle.m_linear_delta[i].vec / particle.m_amount.val;
				collision.m_angular_delta[i].vec = particle.m_angular_delta[i].vec / particle.m_amount.val;
				
				collision.m_movement[i].vec = particle.m_movement[i].vec;
			}
		} else {
			collision.m_times.vec[0] = +numeric_limits<float>::infinity();
			collision.m_times.vec[1] = -numeric_limits<float>::infinity();
		}
	}
	
}
