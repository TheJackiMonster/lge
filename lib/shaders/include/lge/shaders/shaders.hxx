#ifndef LGE_SHADERS_IN_USE
#define LGE_SHADERS_IN_USE

	extern unsigned char MINIMAL_VERT [823];
	extern unsigned int MINIMAL_VERT_LEN;

	const std::string MINIMAL_VERT_SHADER (reinterpret_cast<const char*>(MINIMAL_VERT), MINIMAL_VERT_LEN);

	extern unsigned char MINIMAL_FRAG [1747];
	extern unsigned int MINIMAL_FRAG_LEN;

	const std::string MINIMAL_FRAG_SHADER (reinterpret_cast<const char*>(MINIMAL_FRAG), MINIMAL_FRAG_LEN);

	extern unsigned char CALCULATE_COLLISION_BOUNDS_COMP [1947];
	extern unsigned int CALCULATE_COLLISION_BOUNDS_COMP_LEN;

	const std::string CALCULATE_COLLISION_BOUNDS_COMP_SHADER (reinterpret_cast<const char*>(CALCULATE_COLLISION_BOUNDS_COMP), CALCULATE_COLLISION_BOUNDS_COMP_LEN);

	extern unsigned char CALCULATE_PARTICLE_COLLISION_COMP [7032];
	extern unsigned int CALCULATE_PARTICLE_COLLISION_COMP_LEN;

	const std::string CALCULATE_PARTICLE_COLLISION_COMP_SHADER (reinterpret_cast<const char*>(CALCULATE_PARTICLE_COLLISION_COMP), CALCULATE_PARTICLE_COLLISION_COMP_LEN);

	extern unsigned char CALCULATE_PARTICLE_EXTREME_COMP [3920];
	extern unsigned int CALCULATE_PARTICLE_EXTREME_COMP_LEN;

	const std::string CALCULATE_PARTICLE_EXTREME_COMP_SHADER (reinterpret_cast<const char*>(CALCULATE_PARTICLE_EXTREME_COMP), CALCULATE_PARTICLE_EXTREME_COMP_LEN);

	extern unsigned char GENERATE_INERTIA_COMP [2433];
	extern unsigned int GENERATE_INERTIA_COMP_LEN;

	const std::string GENERATE_INERTIA_COMP_SHADER (reinterpret_cast<const char*>(GENERATE_INERTIA_COMP), GENERATE_INERTIA_COMP_LEN);

	extern unsigned char GENERATE_SDF_COMP [2443];
	extern unsigned int GENERATE_SDF_COMP_LEN;

	const std::string GENERATE_SDF_COMP_SHADER (reinterpret_cast<const char*>(GENERATE_SDF_COMP), GENERATE_SDF_COMP_LEN);

	extern unsigned char SPRITE_VERT [299];
	extern unsigned int SPRITE_VERT_LEN;

	const std::string SPRITE_VERT_SHADER (reinterpret_cast<const char*>(SPRITE_VERT), SPRITE_VERT_LEN);

	extern unsigned char SPRITE_GEOM [908];
	extern unsigned int SPRITE_GEOM_LEN;

	const std::string SPRITE_GEOM_SHADER (reinterpret_cast<const char*>(SPRITE_GEOM), SPRITE_GEOM_LEN);

	extern unsigned char SPRITE_FRAG [1387];
	extern unsigned int SPRITE_FRAG_LEN;

	const std::string SPRITE_FRAG_SHADER (reinterpret_cast<const char*>(SPRITE_FRAG), SPRITE_FRAG_LEN);

#endif
