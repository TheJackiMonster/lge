cmake_minimum_required(VERSION 3.12)

project(shaders_lib C)

include(${lge_config}/Shaders.cmake)

set(sources_dst ${PROJECT_SOURCE_DIR}/include/lge/shaders)

file(MAKE_DIRECTORY ${sources_dst})

set(shaders_header "#ifndef LGE_SHADERS_IN_USE\n#define LGE_SHADERS_IN_USE\n")

#string(APPEND shaders_header "\nnamespace lge::shaders {\n")

set(shader_sources ${lge_shaders_file})

foreach (a_shader IN LISTS lge_shaders)
    get_filename_component(a_filename ${a_shader} NAME)

    file(SIZE ${a_shader} a_size)

    string(TOUPPER ${a_filename} a_varname)
    string(REPLACE "." "_" a_varname ${a_varname})

    string(REPLACE "${lge_shaders_root}/" "" a_path ${a_shader})

    set(a_source "${sources_dst}/${a_filename}.cpp")

    add_custom_command(
            OUTPUT ${a_source}
            WORKING_DIRECTORY ${lge_shaders_root}
            COMMAND xxd -i -C ${a_path} ${a_source}
            COMMENT "Adding shader: ${a_filename}"
    )

    string(APPEND shaders_header "\n\textern unsigned char ${a_varname} [${a_size}]\;\n")
    string(APPEND shaders_header "\textern unsigned int ${a_varname}_LEN\;\n")
    string(APPEND shaders_header "\n\tconst std::string ${a_varname}_SHADER (reinterpret_cast<const char*>(${a_varname}), ${a_varname}_LEN)\;\n")

    list(APPEND shader_sources ${a_source})
endforeach()

#string(APPEND shaders_header "\n}\n")
string(APPEND shaders_header "\n#endif\n")

file(WRITE ${sources_dst}/shaders.hxx ${shaders_header})

add_library(shaders_lib ${shader_sources})

target_include_directories(shaders_lib SYSTEM BEFORE PRIVATE include)

set(LGE_SHADERS_FOUND 1 PARENT_SCOPE)

set(LGE_SHADERS_LIBRARY shaders_lib PARENT_SCOPE)
set(LGE_SHADERS_INCLUDES "${PROJECT_SOURCE_DIR}/include" PARENT_SCOPE)
