
set(lge_config_proj ${lge_config}/projects)

set(lge_proj_path ${PROJECT_SOURCE_DIR}/${lge_proj})

include(${lge_config_proj}/PostProcessingStream.cmake)
include(${lge_config_proj}/SDFRigidBodies.cmake)
include(${lge_config_proj}/SLAMSolver.cmake)
