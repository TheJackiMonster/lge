
set(lge_config_lib ${lge_config}/lib)

set(lge_lib_path ${PROJECT_SOURCE_DIR}/${lge_lib})

list(APPEND lge_flags -pthread)
set(lge_libraries stdc++fs)

set(lge_config_msg " - Library: ")

include(${lge_config_lib}/Assimp.cmake)  # assimp									# libassimp-dev
include(${lge_config_lib}/GLAD.cmake)    #                                          #
include(${lge_config_lib}/GLEW.cmake)    # glew / glew-wayland						# libglew-dev
include(${lge_config_lib}/GLFW.cmake)    # glfw-x11 / glfw-wayland					# libglfw3-dev
include(${lge_config_lib}/GnuTLS.cmake)  # gnutls									#
include(${lge_config_lib}/Lua.cmake)     # lua										#
include(${lge_config_lib}/OpenACC.cmake) # 											#
include(${lge_config_lib}/OpenAL.cmake)  # openal									# libopenal-dev
include(${lge_config_lib}/OpenCL.cmake)  # opencl-mesa / opencl-nvidia				#
include(${lge_config_lib}/OpenCV.cmake)  # opencv									# libopencv-dev
include(${lge_config_lib}/OpenGL.cmake)  # mesa / nvidia							# libopengl0
include(${lge_config_lib}/OpenMP.cmake)  # openmp									#
include(${lge_config_lib}/OpenSSL.cmake) # openssl
include(${lge_config_lib}/PNG.cmake)     # libpng
include(${lge_config_lib}/SDL.cmake)     # sdl										# libsdl2-dev
include(${lge_config_lib}/Shaders.cmake) #                                          #
include(${lge_config_lib}/Vulkan.cmake)  # vulkan-intel / vulkan-radeon / nvidia    # libvulkan-dev
include(${lge_config_lib}/zlib.cmake)    # zlib										# zlib1g-dev

list(REMOVE_DUPLICATES lge_flags)

if (lge_includes)
    list(REMOVE_DUPLICATES lge_includes)
endif ()

include(${lge_config_ext}/CheckLibraries.cmake)

if (lge_definitions)
    list(REMOVE_DUPLICATES lge_definitions)
endif ()

list(JOIN lge_flags " " lge_flags)
