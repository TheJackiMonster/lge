
list(APPEND lge_shaders ${lge_shaders_root}/calculate_collision_bounds.comp)

list(APPEND lge_shaders ${lge_shaders_root}/calculate_particle_collision.comp)
list(APPEND lge_shaders ${lge_shaders_root}/calculate_particle_extreme.comp)

list(APPEND lge_shaders ${lge_shaders_root}/generate_inertia.comp)
list(APPEND lge_shaders ${lge_shaders_root}/generate_sdf.comp)
