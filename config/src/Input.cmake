
list(APPEND lge_headers ${lge_source_root}/input/Handle.hpp)
list(APPEND lge_headers ${lge_source_root}/input/Keyboard.hpp)
list(APPEND lge_headers ${lge_source_root}/input/Manager.hpp)
list(APPEND lge_headers ${lge_source_root}/input/Mouse.hpp)

list(APPEND lge_sources ${lge_source_root}/input/Handle.cpp)
list(APPEND lge_sources ${lge_source_root}/input/Keyboard.cpp)
list(APPEND lge_sources ${lge_source_root}/input/Manager.cpp)
list(APPEND lge_sources ${lge_source_root}/input/Mouse.cpp)
