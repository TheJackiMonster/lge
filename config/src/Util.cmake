
list(APPEND lge_headers ${lge_source_root}/util/AutoComplete.hpp)

list(APPEND lge_templates ${lge_source_root}/util/algorithm.hxx)
list(APPEND lge_templates ${lge_source_root}/util/event.hxx)
list(APPEND lge_templates ${lge_source_root}/util/functional.hxx)

list(APPEND lge_sources ${lge_source_root}/util/AutoComplete.cpp)
