
list(APPEND lge_headers ${lge_source_root}/graphics/vertex/Array.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/vertex/AttribType.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/vertex/Attribute.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/vertex/Buffer.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/vertex/Structure.hpp)

list(APPEND lge_sources ${lge_source_root}/graphics/vertex/Array.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/vertex/AttribType.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/vertex/Attribute.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/vertex/Buffer.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/vertex/Structure.cpp)
