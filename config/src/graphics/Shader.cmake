
include(${lge_config_src}/graphics/shader/Storage.cmake)
include(${lge_config_src}/graphics/shader/Uniform.cmake)

list(APPEND lge_headers ${lge_source_root}/graphics/shader/Attributes.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/shader/Fragments.hpp)

list(APPEND lge_templates ${lge_source_root}/graphics/shader/uniform.hxx)

list(APPEND lge_sources ${lge_source_root}/graphics/shader/Attributes.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/shader/Fragments.cpp)
