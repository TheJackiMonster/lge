
list(APPEND lge_headers ${lge_source_root}/graphics/shader/storage/BufferBinding.hpp)

list(APPEND lge_templates ${lge_source_root}/graphics/shader/storage/buffer.hxx)
list(APPEND lge_templates ${lge_source_root}/graphics/shader/storage/buffer_block.hxx)

list(APPEND lge_sources ${lge_source_root}/graphics/shader/storage/BufferBinding.cpp)
