
list(APPEND lge_headers ${lge_source_root}/graphics/shader/uniform/BufferBinding.hpp)

list(APPEND lge_templates ${lge_source_root}/graphics/shader/uniform/buffer.hxx)
list(APPEND lge_templates ${lge_source_root}/graphics/shader/uniform/buffer_block.hxx)

list(APPEND lge_sources ${lge_source_root}/graphics/shader/uniform/BufferBinding.cpp)
