
list(APPEND lge_headers ${lge_source_root}/graphics/frame/Buffer.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/frame/BufferObject.hpp)

list(APPEND lge_sources ${lge_source_root}/graphics/frame/Buffer.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/frame/BufferObject.cpp)
