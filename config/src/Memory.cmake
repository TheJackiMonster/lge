
list(APPEND lge_headers ${lge_source_root}/memory/AccessMode.hpp)

list(APPEND lge_templates ${lge_source_root}/memory/list.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/map.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/move.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/queue.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/set.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/stack.hxx)
list(APPEND lge_templates ${lge_source_root}/memory/vector.hxx)

list(APPEND lge_sources ${lge_source_root}/memory/AccessMode.cpp)
