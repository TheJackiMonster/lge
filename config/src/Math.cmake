
list(APPEND lge_templates ${lge_source_root}/math/complex.hxx)
list(APPEND lge_templates ${lge_source_root}/math/geometry.hxx)
list(APPEND lge_templates ${lge_source_root}/math/mat.hxx)
list(APPEND lge_templates ${lge_source_root}/math/quaternion.hxx)
list(APPEND lge_templates ${lge_source_root}/math/trigonometry.hxx)
list(APPEND lge_templates ${lge_source_root}/math/util.hxx)
list(APPEND lge_templates ${lge_source_root}/math/vec.hxx)
