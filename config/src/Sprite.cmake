
list(APPEND lge_headers ${lge_source_root}/sprite/Sprite.hpp)
list(APPEND lge_headers ${lge_source_root}/sprite/SpriteBatch.hpp)

list(APPEND lge_templates ${lge_source_root}/sprite/sprite.hxx)

list(APPEND lge_sources ${lge_source_root}/sprite/Sprite.cpp)
list(APPEND lge_sources ${lge_source_root}/sprite/SpriteBatch.cpp)
