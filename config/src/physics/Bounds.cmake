
list(APPEND lge_headers ${lge_source_root}/physics/bounds/AxisAlignedBoundingBox.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/bounds/AxisAlignedBoundingCube.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/bounds/AxisAlignedBounds.hpp)

list(APPEND lge_sources ${lge_source_root}/physics/bounds/AxisAlignedBoundingBox.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/bounds/AxisAlignedBoundingCube.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/bounds/AxisAlignedBounds.cpp)
