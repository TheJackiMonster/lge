
include(${lge_config_src}/physics/Bounds.cmake)

list(APPEND lge_headers ${lge_source_root}/physics/PrecisionMode.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/PhysicEngine.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/PhysicMaterial.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/RigidBody.hpp)
list(APPEND lge_headers ${lge_source_root}/physics/SignedDistanceField.hpp)

list(APPEND lge_headers ${lge_source_root}/physics/quat.hxx)
list(APPEND lge_headers ${lge_source_root}/physics/vec.hxx)

list(APPEND lge_headers ${lge_source_root}/physics/PrecisionMode.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/PhysicEngine.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/PhysicMaterial.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/RigidBody.cpp)
list(APPEND lge_sources ${lge_source_root}/physics/SignedDistanceField.cpp)
