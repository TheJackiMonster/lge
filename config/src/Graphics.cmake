
include(${lge_config_src}/graphics/Device.cmake)
include(${lge_config_src}/graphics/Frame.cmake)
include(${lge_config_src}/graphics/Shader.cmake)
include(${lge_config_src}/graphics/Texture.cmake)
include(${lge_config_src}/graphics/Vertex.cmake)
include(${lge_config_src}/graphics/Video.cmake)

list(APPEND lge_headers ${lge_source_root}/graphics/Device.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/RenderBuffer.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/Shader.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/ShaderProgram.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/Texture2D.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/Texture2DHandle.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/Texture3D.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/Texture3DHandle.hpp)
list(APPEND lge_headers ${lge_source_root}/graphics/TextureHandle.hpp)

list(APPEND lge_templates ${lge_source_root}/graphics/glsl.hxx)
list(APPEND lge_templates ${lge_source_root}/graphics/mat.hxx)
list(APPEND lge_templates ${lge_source_root}/graphics/util.hxx)
list(APPEND lge_templates ${lge_source_root}/graphics/vec.hxx)

list(APPEND lge_sources ${lge_source_root}/graphics/Device.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/RenderBuffer.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/Shader.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/ShaderProgram.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/Texture2D.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/Texture2DHandle.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/Texture3D.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/Texture3DHandle.cpp)
list(APPEND lge_sources ${lge_source_root}/graphics/TextureHandle.cpp)
