
list(APPEND lge_headers ${lge_source_root}/light/Light.hpp)
list(APPEND lge_headers ${lge_source_root}/light/DirectionalLight.hpp)
list(APPEND lge_headers ${lge_source_root}/light/PointLight.hpp)
list(APPEND lge_headers ${lge_source_root}/light/SpotLight.hpp)
list(APPEND lge_headers ${lge_source_root}/light/LightingUnit.hpp)

list(APPEND lge_sources ${lge_source_root}/light/Light.cpp)
list(APPEND lge_sources ${lge_source_root}/light/DirectionalLight.cpp)
list(APPEND lge_sources ${lge_source_root}/light/PointLight.cpp)
list(APPEND lge_sources ${lge_source_root}/light/SpotLight.cpp)
list(APPEND lge_sources ${lge_source_root}/light/LightingUnit.cpp)
