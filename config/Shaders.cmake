
set(lge_config_shaders ${lge_config}/shaders)
set(lge_shaders_root ${lge_resources}/shaders)

include(${lge_config_shaders}/Minimal.cmake)
include(${lge_config_shaders}/Physics.cmake)
include(${lge_config_shaders}/Sprite.cmake)

list(REMOVE_DUPLICATES lge_shaders)
