
if (EXISTS "${lge_lib_path}/glad")
    add_subdirectory(${lge_lib}/glad)
endif ()

if (GLAD_FOUND)
    list(APPEND lge_libraries ${GLAD_LIBRARIES})
    list(APPEND lge_includes ${GLAD_INCLUDES})

    list(APPEND lge_definitions LGE_USE_GLAD)

    message(${lge_config_msg} " GLAD    -   ")
endif ()


