
find_package(ZLIB QUIET)

if (ZLIB_FOUND)
    list(APPEND lge_includes ${ZLIB_INCLUDE_DIRS})
    list(APPEND lge_libraries ${ZLIB_LIBRARIES})

    message(${lge_config_msg} " zlib    -   " ${ZLIB_VERSION_STRING})
endif ()
