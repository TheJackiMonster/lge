
find_package(Lua QUIET)

if (LUA_FOUND OR Lua_FOUND)
    list(APPEND lge_includes ${LUA_INCLUDE_DIR})
    list(APPEND lge_libraries ${LUA_LIBRARIES})

    message(${lge_config_msg} " Lua     -   " ${LUA_VERSION_STRING})
endif ()
