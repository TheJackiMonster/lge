
find_package(OpenSSL QUIET)

if (OPENSSL_FOUND)
    list(APPEND lge_includes ${OPENSSL_INCLUDE_DIR})
    list(APPEND lge_libraries ${OPENSSL_LIBRARIES})

    message(${lge_config_msg} " OpenSSL -   " ${OPENSSL_VERSION})
endif ()
