
find_package(SDL QUIET)

if (SDL_FOUND)
    list(APPEND lge_includes ${SDL_INCLUDE_DIR})
    list(APPEND lge_libraries ${SDL_LIBRARY})

    message(${lge_config_msg} " SDL     -   " ${SDL_VERSION_STRING})
endif ()

