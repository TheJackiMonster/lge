
set(ON 1)
find_package(assimp QUIET)
unset(ON)

if (assimp_FOUND)
    list(APPEND lge_includes ${ASSIMP_INCLUDE_DIRS})
    list(APPEND lge_libraries ${ASSIMP_LIBRARIES})

    message(${lge_config_msg} " Assimp  -   " ${assimp_VERSION})
endif ()
