
find_package(GLEW QUIET)

if (GLEW_FOUND)
    list(APPEND lge_includes ${GLEW_INCLUDE_DIRS})
    list(APPEND lge_libraries ${GLEW_LIBRARIES})

    message(${lge_config_msg} " GLEW    -   ")
endif ()
