
find_package(Vulkan QUIET)

if (Vulkan_FOUND)
    list(APPEND lge_includes ${Vulkan_INCLUDE_DIR})
    list(APPEND lge_libraries ${Vulkan_LIBRARIES})

    message(${lge_config_msg} " Vulkan  -   ")
endif ()
