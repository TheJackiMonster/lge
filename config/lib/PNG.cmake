
find_package(PNG QUIET)

if (PNG_FOUND)
    list(APPEND lge_includes ${PNG_INCLUDE_DIRS})
    list(APPEND lge_libraries ${PNG_LIBRARIES})
    list(APPEND lge_definitions ${PNG_DEFINITIONS})

    list(APPEND lge_definitions LGE_USE_PNG)

    message(${lge_config_msg} " PNG     -   " ${PNG_VERSION_STRING})
endif ()
