
find_package(OpenACC QUIET)

if (OpenACC_${lge_lang}_FOUND)
    list(APPEND lge_flags ${OpenACC_${lge_lang}_FLAGS})

    message(${lge_config_msg} " OpenACC -   " ${OpenACC_${lge_lang}_VERSION})
endif ()

