
if (EXISTS "${lge_lib_path}/shaders")
    add_subdirectory(${lge_lib}/shaders)
endif ()

if (LGE_SHADERS_FOUND)
    list(APPEND lge_libraries ${LGE_SHADERS_LIBRARY})
    list(APPEND lge_includes ${LGE_SHADERS_INCLUDES})

    list(APPEND lge_definitions LGE_USE_SHADERS)

    message(${lge_config_msg} " SHADERS -   ")
endif ()
