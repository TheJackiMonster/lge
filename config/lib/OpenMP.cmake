
find_package(OpenMP QUIET)

if (OpenMP_${lge_lang}_FOUND)
    list(APPEND lge_flags ${OpenMP_${lge_lang}_FLAGS})
    list(APPEND lge_libraries ${OpenMP_${lge_lang}_LIBRARIES})

    message(${lge_config_msg} " OpenMP  -   " ${OpenMP_${lge_lang}_SPEC_DATE})
endif ()
