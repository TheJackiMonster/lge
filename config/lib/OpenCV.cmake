
find_package(OpenCV QUIET)

if (OpenCV_FOUND OR OPENCV_FOUND)
    list(APPEND lge_includes ${OpenCV_INCLUDE_DIRS})
    list(APPEND lge_libraries ${OpenCV_LIBRARIES})

    list(APPEND lge_definitions LGE_USE_OPENCV)

    message(${lge_config_msg} " OpenCV  -   " ${OpenCV_VERSION})
endif ()
