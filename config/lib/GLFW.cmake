
find_package(glfw3 QUIET)

if (glfw3_FOUND)
    list(APPEND lge_libraries glfw)

    message(${lge_config_msg} " GLFW    -   " ${glfw3_VERSION})
endif ()
