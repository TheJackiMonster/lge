
set(OpenGL_GL_PREFERENCE GLVND)

find_package(OpenGL QUIET)

if (OPENGL_FOUND)
    list(APPEND lge_includes ${OPENGL_INCLUDE_DIR})
    list(APPEND lge_libraries ${OPENGL_LIBRARIES})

    message(${lge_config_msg} " OpenGL  -   ")
endif ()

if (OpenGL_OpenGL_FOUND)
    list(APPEND lge_libraries ${OPENGL_opengl_LIBRARY})
endif ()

if (OPENGL_GLU_FOUND)
    list(APPEND lge_libraries ${OPENGL_glu_LIBRARY})
endif ()

if (OpenGL_GLX_FOUND)
    list(APPEND lge_includes ${OPENGL_GLX_INCLUDE_DIR})
    list(APPEND lge_libraries ${OPENGL_glx_LIBRARY})
endif ()

if (OpenGL_EGL_FOUND)
    list(APPEND lge_includes ${OPENGL_EGL_INCLUDE_DIRS})
    list(APPEND lge_libraries ${OPENGL_egl_LIBRARY})
endif ()

if (OPENGL_XMESA_FOUND)
    list(APPEND lge_includes ${OPENGL_xmesa_INCLUDE_DIR})
endif ()
