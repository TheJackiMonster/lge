
find_package(OpenCL QUIET)

if (OpenCL_FOUND)
    list(APPEND lge_definitions CL_HPP_TARGET_OPENCL_VERSION=${OpenCL_VERSION_MAJOR}00)

    list(APPEND lge_includes ${OpenCL_INCLUDE_DIRS})
    list(APPEND lge_libraries ${OpenCL_LIBRARIES})

    list(APPEND lge_definitions LGE_USE_OPENCL)

    message(${lge_config_msg} " OpenCL  -   " ${OpenCL_VERSION_STRING})
endif ()
