
find_package(OpenAL QUIET)

if (OPENAL_FOUND)
    list(APPEND lge_includes ${OPENAL_INCLUDE_DIR})
    list(APPEND lge_libraries ${OPENAL_LIBRARY})

    list(APPEND lge_definitions LGE_USE_OPENAL)

    message(${lge_config_msg} " OpenAL  -   ")
endif ()
