
find_package(GnuTLS QUIET)

if (GNUTLS_FOUND)
    list(APPEND lge_includes ${GNUTLS_INCLUDE_DIR})
    list(APPEND lge_libraries ${GNUTLS_LIBRARIES})
    list(APPEND lge_definitions ${GNUTLS_DEFINITIONS})

    message(${lge_config_msg} " GnuTLS  -   " ${GNUTLS_VERSION_STRING})
endif ()
