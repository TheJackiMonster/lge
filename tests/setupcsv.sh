#!/bin/sh
for f in test-*.txt
do
	g=$(echo $f | sed -r "s/test-(m[0-9]-[0-9]+).txt/data\/test-\1.csv/")
	
	if [ $f != $g ]; then ./log2csv.sh $f $g; fi
done

for mode in $(seq 0 3)
do
	python "merge-data.py" $(ls "data" | grep "test-m$mode") > "data/m$mode.csv"
done
