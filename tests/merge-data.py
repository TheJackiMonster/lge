
import sys
import pandas as pd

def search_for(rec, t):
    a = 0
    z = len(rec) - 1

    while (a <= z):
        q = (a + z) // 2
        qt = rec[q]['Time:']
        
        if (qt == t):
            return q
        elif (qt < t):
            a = q + 1
        else:
            z = q - 1

    return a

def lerp_at(rec, q, t):
    qt = rec[q]['Time:']
    rt = 0

    ft = rec[q]['Frametime:']
    e = rec[q]['Error:']
    dt = rec[q]['Deltatime:']
    en = rec[q]['Energy:']
    
    x = 0

    if (q > 0):
        rt = rec[q - 1]['Time:']

        ft0 = rec[q - 1]['Frametime:']
        e0 = rec[q - 1]['Error:']
        dt0 = rec[q - 1]['Deltatime:']
        en0 = rec[q - 1]['Energy:']
        
        x = 1

        if (qt > rt):
            x = (t - rt) / (qt - rt)
        
        ft = (ft - ft0) * x + ft0
        e = (e - e0) * x + e0
        dt = (dt - dt0) * x + dt0
        en = (en - en0) * x + en0
    else:
        x = 1

        if (qt > 0):
            x = t / qt

        ft = ft * x
        e = e * x
        dt = dt * x
        en = en * x

    return { 'Frametime:' : ft, 'Time:' : t, 'Error:' : e, 'Deltatime:' : dt, 'Variance:' : 0, 'Energy:' : en }

def add_missing(rec_x, rec_y):
    for y in rec_y:
        t = y['Time:']
        i = search_for(rec_x, t)
        
        if ((i < len(rec_x)) and (rec_x[i]['Time:'] != t)):
            rec_x.insert(i, lerp_at(rec_x, i, t))

def sum_both(rec_x, rec_y):
    for i in range(len(rec_x)):
        rec_x[i]['Frametime:'] += rec_y[i]['Frametime:']
        rec_x[i]['Error:'] += rec_y[i]['Error:']
        rec_x[i]['Deltatime:'] += rec_y[i]['Deltatime:']
        rec_x[i]['Energy:'] += rec_y[i]['Energy:']

def divide_by(rec, c):
    for e in rec:
        e['Frametime:'] /= c
        e['Error:'] /= c
        e['Deltatime:'] /= c
        e['Energy:'] /= c

def add_variance(rec_x, rec_y):
	for i in range(len(rec_x)):
		ex = rec_x[i]['Error:']
		ey = rec_y[i]['Error:']
		
		if (not 'Variance:' in rec_x[i]):
			rec_x[i]['Variance:'] = 0
		
		rec_x[i]['Variance:'] += (ey - ex) * (ey - ex)

def divide_by2(rec, c):
    for e in rec:
        e['Variance:'] /= c

def print_out_csv(rec):
    print("Frametime:,Time:,Error:,Deltatime:,Variance:,Energy:")

    for e in rec:
        print(str(e['Frametime:']) + "," + str(e['Time:']) + "," + str(e['Error:']) + "," + str(e['Deltatime:']) + "," + str(e['Variance:']) + "," + str(e['Energy:']))

if len(sys.argv) > 1:
    data = []

    for x in range(len(sys.argv) - 1):
        df = pd.read_csv("data/" + sys.argv[x + 1])
        rec = df.to_dict('records')
        data.append(rec)
    
    for x in range(1, len(data)):
        add_missing(data[0], data[x])

    ec = len(data[0])

    for x in range(1, len(data)):
        add_missing(data[x], data[0])
        ec = min(ec, len(data[x]))

    result = data[0][:ec]

    for x in range(1, len(data)):
        sum_both(result, data[x])

    divide_by(result, len(data))
    
    for x in range(0, len(data)):
        add_variance(result, data[x])
    
    divide_by2(result, len(data))
    
    print_out_csv(result)

