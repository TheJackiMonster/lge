#!/bin/sh
m0=$1
m1=$2
m2=$3
m3=$4

xx="$5"
yy="$6"
lw=1.25
xl="?"
yl="?"
fs=",12"
ll="$7"
rr="$8"

if [ $xx = "4" ] || [ $yy = "4" ]; then lw=0.75; fi

if [ $xx = '1' ]; then xl="Renderzeit (in s)"; fi
if [ $xx = '2' ]; then xl="Simulationszeit (in s)"; fi
if [ $xx = '3' ]; then xl="Verschiebung (in m)"; fi
if [ $xx = '4' ]; then xl="Rechenzeitschritt (in s)"; fi
if [ $xx = '5' ]; then xl="Varianz der Verschiebung (in m²)"; fi
if [ $xx = '6' ]; then xl="Gesamtenergie (in J)"; fi

if [ $yy = '1' ]; then yl="Renderzeit (in s)"; fi
if [ $yy = '2' ]; then yl="Simulationszeit (in s)"; fi
if [ $yy = '3' ]; then yl="Verschiebung (in m)"; fi
if [ $yy = '4' ]; then yl="Rechenzeitschritt (in s)"; fi
if [ $yy = '5' ]; then yl="Varianz der Verschiebung (in m²)"; fi
if [ $yy = '6' ]; then yl="Gesamtenergie (in J)"; fi

gnuplot -e "set terminal pdf; set output 'plot/test-$xx-$yy-$ll-$rr.pdf'; set datafile separator ','; set xlabel '$xl' font '$fs'; set ylabel '$yl' font '$fs'; set xrange [$ll:$rr]; plot 'data/$m0' using $xx:$yy title 'PRECISE' with line lw $lw, 'data/$m1' using $xx:$yy title 'DYNAMIC' with line lw $lw, 'data/$m2' using $xx:$yy title 'REALTIME' with line lw $lw, 'data/$m3' using $xx:$yy title 'A POSTERIORI' with line lw $lw"
