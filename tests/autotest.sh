#!/bin/sh
cd ../projects/sdf_rigid_bodies
for i in $(seq 0 29)
do
	for mode in $(seq 0 3)
	do
		./../../cmake-build-release/projects/sdf_rigid_bodies/sdf_rigid_bodies $mode > "../../tests/test-m$mode-$i.txt"
	done
done
