#!/bin/sh
echo "Frametime:,Time:,Error:,Deltatime:,Energy:" > $2
cat $1 | grep "Time:" | sed -r "s/Time: ([^;]+); Frametime: ([^;]+); Error: ([^;]+); Energy: ([^;]+); Deltatime: ([^;]+);/\2,\1,\3,\5,\4/" >> $2
